#pragma once

namespace MSpinTool {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
    private: System::Windows::Forms::StatusStrip^  statusStrip1;
    protected: 
    private: System::Windows::Forms::ToolStripStatusLabel^  toolStripStatusLabel1;
    private: System::Windows::Forms::MenuStrip^  menuStrip1;
    private: System::Windows::Forms::ToolStripMenuItem^  toolsToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  deviceConfigurationToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  deviceConfigurationToolStripMenuItem1;
    private: System::Windows::Forms::ToolStripMenuItem^  registerMapToolStripMenuItem;
    private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator1;
    private: System::Windows::Forms::ToolStripMenuItem^  optionsToolStripMenuItem;
    private: System::Windows::Forms::TabControl^  tabControl1;
    private: System::Windows::Forms::TabPage^  tabPage1;
    private: System::Windows::Forms::TabPage^  tabPage2;
    private: System::Windows::Forms::TabPage^  tabPage3;
    private: System::Windows::Forms::Button^  button1;
    private: System::Windows::Forms::Button^  button2;
    private: System::Windows::Forms::Button^  button3;
    private: System::Windows::Forms::Button^  button4;
    private: System::Windows::Forms::GroupBox^  groupBox1;

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
            this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
            this->toolStripStatusLabel1 = (gcnew System::Windows::Forms::ToolStripStatusLabel());
            this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
            this->toolsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->deviceConfigurationToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->deviceConfigurationToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->registerMapToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->toolStripSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
            this->optionsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
            this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
            this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
            this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
            this->tabPage3 = (gcnew System::Windows::Forms::TabPage());
            this->button1 = (gcnew System::Windows::Forms::Button());
            this->button2 = (gcnew System::Windows::Forms::Button());
            this->button3 = (gcnew System::Windows::Forms::Button());
            this->button4 = (gcnew System::Windows::Forms::Button());
            this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
            this->statusStrip1->SuspendLayout();
            this->menuStrip1->SuspendLayout();
            this->tabControl1->SuspendLayout();
            this->SuspendLayout();
            // 
            // statusStrip1
            // 
            this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->toolStripStatusLabel1});
            this->statusStrip1->Location = System::Drawing::Point(0, 533);
            this->statusStrip1->Name = L"statusStrip1";
            this->statusStrip1->Size = System::Drawing::Size(587, 22);
            this->statusStrip1->TabIndex = 0;
            this->statusStrip1->Text = L"statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this->toolStripStatusLabel1->Name = L"toolStripStatusLabel1";
            this->toolStripStatusLabel1->Size = System::Drawing::Size(118, 17);
            this->toolStripStatusLabel1->Text = L"toolStripStatusLabel1";
            // 
            // menuStrip1
            // 
            this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->toolsToolStripMenuItem});
            this->menuStrip1->Location = System::Drawing::Point(0, 0);
            this->menuStrip1->Name = L"menuStrip1";
            this->menuStrip1->Size = System::Drawing::Size(587, 24);
            this->menuStrip1->TabIndex = 1;
            this->menuStrip1->Text = L"menuStrip1";
            // 
            // toolsToolStripMenuItem
            // 
            this->toolsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(5) {this->deviceConfigurationToolStripMenuItem, 
                this->deviceConfigurationToolStripMenuItem1, this->registerMapToolStripMenuItem, this->toolStripSeparator1, this->optionsToolStripMenuItem});
            this->toolsToolStripMenuItem->Name = L"toolsToolStripMenuItem";
            this->toolsToolStripMenuItem->Size = System::Drawing::Size(48, 20);
            this->toolsToolStripMenuItem->Text = L"Tools";
            // 
            // deviceConfigurationToolStripMenuItem
            // 
            this->deviceConfigurationToolStripMenuItem->Name = L"deviceConfigurationToolStripMenuItem";
            this->deviceConfigurationToolStripMenuItem->Size = System::Drawing::Size(186, 22);
            this->deviceConfigurationToolStripMenuItem->Text = L"Connect to board";
            // 
            // deviceConfigurationToolStripMenuItem1
            // 
            this->deviceConfigurationToolStripMenuItem1->Name = L"deviceConfigurationToolStripMenuItem1";
            this->deviceConfigurationToolStripMenuItem1->Size = System::Drawing::Size(186, 22);
            this->deviceConfigurationToolStripMenuItem1->Text = L"Device Configuration";
            // 
            // registerMapToolStripMenuItem
            // 
            this->registerMapToolStripMenuItem->Name = L"registerMapToolStripMenuItem";
            this->registerMapToolStripMenuItem->Size = System::Drawing::Size(186, 22);
            this->registerMapToolStripMenuItem->Text = L"Register map";
            // 
            // toolStripSeparator1
            // 
            this->toolStripSeparator1->Name = L"toolStripSeparator1";
            this->toolStripSeparator1->Size = System::Drawing::Size(183, 6);
            // 
            // optionsToolStripMenuItem
            // 
            this->optionsToolStripMenuItem->Name = L"optionsToolStripMenuItem";
            this->optionsToolStripMenuItem->Size = System::Drawing::Size(186, 22);
            this->optionsToolStripMenuItem->Text = L"Options";
            // 
            // tabControl1
            // 
            this->tabControl1->Controls->Add(this->tabPage1);
            this->tabControl1->Controls->Add(this->tabPage2);
            this->tabControl1->Controls->Add(this->tabPage3);
            this->tabControl1->Location = System::Drawing::Point(12, 27);
            this->tabControl1->Name = L"tabControl1";
            this->tabControl1->SelectedIndex = 0;
            this->tabControl1->Size = System::Drawing::Size(322, 212);
            this->tabControl1->TabIndex = 0;
            // 
            // tabPage1
            // 
            this->tabPage1->Location = System::Drawing::Point(4, 22);
            this->tabPage1->Name = L"tabPage1";
            this->tabPage1->Padding = System::Windows::Forms::Padding(3);
            this->tabPage1->Size = System::Drawing::Size(314, 186);
            this->tabPage1->TabIndex = 0;
            this->tabPage1->Text = L"Positioning";
            this->tabPage1->UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this->tabPage2->Location = System::Drawing::Point(4, 22);
            this->tabPage2->Name = L"tabPage2";
            this->tabPage2->Padding = System::Windows::Forms::Padding(3);
            this->tabPage2->Size = System::Drawing::Size(325, 132);
            this->tabPage2->TabIndex = 1;
            this->tabPage2->Text = L"Speed";
            this->tabPage2->UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this->tabPage3->Location = System::Drawing::Point(4, 22);
            this->tabPage3->Name = L"tabPage3";
            this->tabPage3->Size = System::Drawing::Size(325, 132);
            this->tabPage3->TabIndex = 2;
            this->tabPage3->Text = L"Advanced";
            this->tabPage3->UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this->button1->Location = System::Drawing::Point(16, 242);
            this->button1->Name = L"button1";
            this->button1->Size = System::Drawing::Size(75, 45);
            this->button1->TabIndex = 2;
            this->button1->Text = L"HardStop";
            this->button1->UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this->button2->Location = System::Drawing::Point(97, 242);
            this->button2->Name = L"button2";
            this->button2->Size = System::Drawing::Size(75, 45);
            this->button2->TabIndex = 3;
            this->button2->Text = L"SoftStop";
            this->button2->UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this->button3->Location = System::Drawing::Point(178, 242);
            this->button3->Name = L"button3";
            this->button3->Size = System::Drawing::Size(75, 45);
            this->button3->TabIndex = 4;
            this->button3->Text = L"HardHiZ";
            this->button3->UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this->button4->Location = System::Drawing::Point(259, 241);
            this->button4->Name = L"button4";
            this->button4->Size = System::Drawing::Size(75, 45);
            this->button4->TabIndex = 5;
            this->button4->Text = L"SoftHiZ";
            this->button4->UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this->groupBox1->Location = System::Drawing::Point(12, 302);
            this->groupBox1->Name = L"groupBox1";
            this->groupBox1->Size = System::Drawing::Size(382, 213);
            this->groupBox1->TabIndex = 6;
            this->groupBox1->TabStop = false;
            this->groupBox1->Text = L"Status";
            // 
            // Form1
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(587, 555);
            this->Controls->Add(this->groupBox1);
            this->Controls->Add(this->button4);
            this->Controls->Add(this->button3);
            this->Controls->Add(this->button2);
            this->Controls->Add(this->button1);
            this->Controls->Add(this->tabControl1);
            this->Controls->Add(this->statusStrip1);
            this->Controls->Add(this->menuStrip1);
            this->MainMenuStrip = this->menuStrip1;
            this->Name = L"Form1";
            this->Text = L"MySPINTool";
            this->statusStrip1->ResumeLayout(false);
            this->statusStrip1->PerformLayout();
            this->menuStrip1->ResumeLayout(false);
            this->menuStrip1->PerformLayout();
            this->tabControl1->ResumeLayout(false);
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
	};
}

