#pragma once
#include "SteperConversion.h"
#include "BaseNewProtocolDevice.h"

enum CommadID
{
   CommadID_WRITE_SPEED_PROFILE_RUN	    = 0x1,
   CommadID_WRITE_STOP_MOTOR	        = 0x2,
   CommadID_WRITE_DRIVER_SETUP	        = 0x3,
   
   CommadID_READ_SPEED_PROFILE	        = 0x41,
   CommadID_READ_STATE_WORD	            = 0x42,
   CommadID_READ_DRIVER_SETUP		    = 0x43,
   
};

class DF1_Module:public BaseNewProtocolDevice
{
public:
    DF1_Module(void);
    ~DF1_Module(void);
	
    int WriteSpeedProfileRun(int* ControlWord,int* BeginSpeed,int* EndSpeed,int* TimeAcc,int* TimeStop,int* StepCount,int* StepMode);
    int ReadSpeedProfileRun (int* ControlWord,int* BeginSpeed,int* EndSpeed,int* TimeAcc,int* TimeStop,int* StepCount,int* StepMode);
   
    int SoftStopMotor(void);
    int ReadStateDrive     (int* StateWord , int* AbsolutePositionMotor , int* CurrentMotorSpeed);
    int ReadRegistersMotor (cSPIN_RegsStruct_TypeDef* Registers);
    int WriteRegistersMotor(cSPIN_RegsStruct_TypeDef* Registers);
};

