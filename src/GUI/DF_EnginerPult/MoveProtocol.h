#include  <stdio.h>?
#include <time.h>>
#include "CriticalSection.h"
class MoveProtocol
{
public:
	MoveProtocol();
	~MoveProtocol();
	bool Create(const char* Name);
	void PutInputParametrs(int* ControlWord, int* SpeedBegin, int* SpeedEnd, int* TimeAcc, int* TimeBraake, int* StepCount, int* StepMode);
	void PutInfo(int* StateWord, float* Sppeed, int*  Positiob);
	void Destroy(void);
private:
	FILE* File;
	time_t* Time;
	tm*     CurrentTime;
	CriticalSection Section;
};

