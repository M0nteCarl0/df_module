#include "MainView.h"
using namespace DF_EnginerPult;
void  MainView::HideStatusKabels(void)
{


}

void  MainView::ShowStatusKabels(void)
{

	
	
	   

}




void  MainView::InitLabels(void)
{

	System::Drawing::Color Color = System::Drawing::Color::Gray;
	MOduleStateWord_Label->Text = "";
	DriverStateWord_Label->Text = "";
	MotorSpeed_toolStripStatusLabel->Text = "";
	Hiz_Label->BackColor = Color;
	BUSY_Label->BackColor = Color;
	WRONG_CMD__Label->BackColor = Color;
	NOT_PERF_CMD_Label->BackColor = Color;
	SW_F_Label->BackColor = Color;
	NOT_PERF_CMD_Label->BackColor = Color;
	SW_EVN_Label->BackColor = Color;
	
	DriverStateWord_Label->Text = "";
	MOduleStateWord_Label->Text = "";

	
	DIR_Label->BackColor = Color;
	UVLO_Label->BackColor = Color;
	TH_WRN__Label->BackColor = Color;
	TH_SD__Label->BackColor = Color;
	OCD_Label->BackColor = Color;
	WS_Label->BackColor = Color;
	WDT_Label->BackColor = Color;
	SCK_Mode__Label->BackColor = Color;
	Sensor1_Label->BackColor = Color;
	Sensor2_Label->BackColor = Color;


}

        MainView::MainView(void)
		{
			InitializeComponent();
			RegisterView	= gcnew  DriverRegisterView();
			FT_Setup		= gcnew FTDIView();
			DF				= new shared_ptr<DF1_Module>( new DF1_Module());
			Help			= gcnew 	HelperForm();
			DF->get()->CreateProtocol();
			DriveProtocol = new 	MoveProtocol();
			ProgramConf = new   ProgramConfigFile();

			FTDI = new std::shared_ptr<MxComm>(new  MxComm());
			RegisterConfig = new  ConfigurationFile();
			InitLabels();
			ProgramConf->Reorginize("DF_Pult.ini");
			
			FTDI->get()->InitializeDeviceListener((HWND)this->Handle.ToPointer());
			nSpeed = 19200;
			nDataBits = 8;
			nParity = 1;
			nStopBits = 2;
			g_CoenficinetMinSpeed = pow(10.0, 6) / (pow(2.0, 24) * 0.25);
			g_CoenficinetSpeed = pow(10.0, 6) / (pow(2.0, 28) * 0.25);

			int SpeedEnd = ProgramConf->SpeedEnd / g_CoenficinetSpeed;
			int SpeedBegin = ProgramConf->SpeedBegin / g_CoenficinetMinSpeed;

			DecodeModuleParametr(false, &ProgramConf->Conntrol, &SpeedBegin, &SpeedEnd, &ProgramConf->TimeAcc, &ProgramConf->TimeBrake, &ProgramConf->StepNode, &ProgramConf->StepCount);
			TimeCounter = new HiRessTimeMeasure();
			FlagStartSrive = false;
			ProgramPath = new char[MAX_PATH];
			GetCurrentDirectory(MAX_PATH, ProgramPath);
			
			
		}


		MainView::~MainView()
		{


			if (TimeCounter)
			{
				delete TimeCounter;
			}


			if (RegisterConfig)
			{

				delete RegisterConfig;
			}

			if (components)
			{
				delete components;
			}
		}



#pragma region Windows Form Designer generated code
        void MainView::InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->MainstatusStrip = (gcnew System::Windows::Forms::StatusStrip());
			this->MotorSpeed_toolStripStatusLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->BrakefoLabel = (gcnew System::Windows::Forms::Label());
			this->AccInfoLabel = (gcnew System::Windows::Forms::Label());
			this->SpeedEndSB_label = (gcnew System::Windows::Forms::Label());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->SpeedBeginLSB_label = (gcnew System::Windows::Forms::Label());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->radioButton_Direction_Backward = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton_Direction_Forward = (gcnew System::Windows::Forms::RadioButton());
			this->MicrosStepcomboBox = (gcnew System::Windows::Forms::ComboBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->ModeSensor2 = (gcnew System::Windows::Forms::RadioButton());
			this->ModeSensor1 = (gcnew System::Windows::Forms::RadioButton());
			this->ModeInfinite = (gcnew System::Windows::Forms::RadioButton());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->Stepcount_label1 = (gcnew System::Windows::Forms::Label());
			this->MicrosStepCount_textBox = (gcnew System::Windows::Forms::TextBox());
			this->TimeBrake_label1 = (gcnew System::Windows::Forms::Label());
			this->TimeAcc_label = (gcnew System::Windows::Forms::Label());
			this->SpeedEnd_label = (gcnew System::Windows::Forms::Label());
			this->SpeedBegin_label = (gcnew System::Windows::Forms::Label());
			this->TimeBrake_textBox = (gcnew System::Windows::Forms::TextBox());
			this->TimeAcceleration_textBox = (gcnew System::Windows::Forms::TextBox());
			this->SpeedEnd_textBox = (gcnew System::Windows::Forms::TextBox());
			this->SpeedBegin_textBox = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->MianmenuStrip = (gcnew System::Windows::Forms::MenuStrip());
			this->fTDIToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->SetuoFTDIToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->��������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->������������������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->POllingtimer = (gcnew System::Windows::Forms::Timer(this->components));
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->WDT_Label = (gcnew System::Windows::Forms::Label());
			this->WS_Label = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->NOT_PERF_CMD_Label = (gcnew System::Windows::Forms::Label());
			this->label53 = (gcnew System::Windows::Forms::Label());
			this->DIR_Label = (gcnew System::Windows::Forms::Label());
			this->SCK_Mode__Label = (gcnew System::Windows::Forms::Label());
			this->label47 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->WRONG_CMD__Label = (gcnew System::Windows::Forms::Label());
			this->OCD_Label = (gcnew System::Windows::Forms::Label());
			this->label49 = (gcnew System::Windows::Forms::Label());
			this->Hiz_Label = (gcnew System::Windows::Forms::Label());
			this->label51 = (gcnew System::Windows::Forms::Label());
			this->label45 = (gcnew System::Windows::Forms::Label());
			this->BUSY_Label = (gcnew System::Windows::Forms::Label());
			this->label43 = (gcnew System::Windows::Forms::Label());
			this->SW_EVN_Label = (gcnew System::Windows::Forms::Label());
			this->TH_WRN__Label = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label41 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->TH_SD__Label = (gcnew System::Windows::Forms::Label());
			this->SW_F_Label = (gcnew System::Windows::Forms::Label());
			this->UVLO = (gcnew System::Windows::Forms::Label());
			this->label39 = (gcnew System::Windows::Forms::Label());
			this->UVLO_Label = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->Sensor2_Label = (gcnew System::Windows::Forms::Label());
			this->Sensor1_Label = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->groupBox6 = (gcnew System::Windows::Forms::GroupBox());
			this->MOduleStateWord_Label = (gcnew System::Windows::Forms::Label());
			this->DriverStateWord_Label = (gcnew System::Windows::Forms::Label());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->groupBox7 = (gcnew System::Windows::Forms::GroupBox());
			this->VersionHW_Label = (gcnew System::Windows::Forms::Label());
			this->VersionSW_Label = (gcnew System::Windows::Forms::Label());
			this->label55 = (gcnew System::Windows::Forms::Label());
			this->label54 = (gcnew System::Windows::Forms::Label());
			this->label37 = (gcnew System::Windows::Forms::Label());
			this->ModuleConectionStae_Label = (gcnew System::Windows::Forms::Label());
			this->FTDIConectionState_label = (gcnew System::Windows::Forms::Label());
			this->label35 = (gcnew System::Windows::Forms::Label());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->MainstatusStrip->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->panel2->SuspendLayout();
			this->panel1->SuspendLayout();
			this->MianmenuStrip->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox6->SuspendLayout();
			this->groupBox7->SuspendLayout();
			this->groupBox5->SuspendLayout();
			this->SuspendLayout();
			// 
			// MainstatusStrip
			// 
			this->MainstatusStrip->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->MotorSpeed_toolStripStatusLabel });
			this->MainstatusStrip->Location = System::Drawing::Point(0, 607);
			this->MainstatusStrip->Name = L"MainstatusStrip";
			this->MainstatusStrip->Size = System::Drawing::Size(540, 22);
			this->MainstatusStrip->TabIndex = 2;
			this->MainstatusStrip->Text = L"statusStrip1";
			// 
			// MotorSpeed_toolStripStatusLabel
			// 
			this->MotorSpeed_toolStripStatusLabel->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->MotorSpeed_toolStripStatusLabel->Name = L"MotorSpeed_toolStripStatusLabel";
			this->MotorSpeed_toolStripStatusLabel->Size = System::Drawing::Size(103, 17);
			this->MotorSpeed_toolStripStatusLabel->Text = L"�������� ������";
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->label8);
			this->groupBox3->Controls->Add(this->BrakefoLabel);
			this->groupBox3->Controls->Add(this->AccInfoLabel);
			this->groupBox3->Controls->Add(this->SpeedEndSB_label);
			this->groupBox3->Controls->Add(this->button3);
			this->groupBox3->Controls->Add(this->SpeedBeginLSB_label);
			this->groupBox3->Controls->Add(this->button2);
			this->groupBox3->Controls->Add(this->panel2);
			this->groupBox3->Controls->Add(this->MicrosStepcomboBox);
			this->groupBox3->Controls->Add(this->label6);
			this->groupBox3->Controls->Add(this->button1);
			this->groupBox3->Controls->Add(this->label19);
			this->groupBox3->Controls->Add(this->panel1);
			this->groupBox3->Controls->Add(this->label18);
			this->groupBox3->Controls->Add(this->label17);
			this->groupBox3->Controls->Add(this->label15);
			this->groupBox3->Controls->Add(this->label13);
			this->groupBox3->Controls->Add(this->Stepcount_label1);
			this->groupBox3->Controls->Add(this->MicrosStepCount_textBox);
			this->groupBox3->Controls->Add(this->TimeBrake_label1);
			this->groupBox3->Controls->Add(this->TimeAcc_label);
			this->groupBox3->Controls->Add(this->SpeedEnd_label);
			this->groupBox3->Controls->Add(this->SpeedBegin_label);
			this->groupBox3->Controls->Add(this->TimeBrake_textBox);
			this->groupBox3->Controls->Add(this->TimeAcceleration_textBox);
			this->groupBox3->Controls->Add(this->SpeedEnd_textBox);
			this->groupBox3->Controls->Add(this->SpeedBegin_textBox);
			this->groupBox3->Controls->Add(this->label4);
			this->groupBox3->Location = System::Drawing::Point(0, 39);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(356, 423);
			this->groupBox3->TabIndex = 4;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"��������� ��������";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(179, 190);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(29, 13);
			this->label8->TabIndex = 35;
			this->label8->Text = L"���";
			// 
			// BrakefoLabel
			// 
			this->BrakefoLabel->AutoSize = true;
			this->BrakefoLabel->Location = System::Drawing::Point(227, 149);
			this->BrakefoLabel->Name = L"BrakefoLabel";
			this->BrakefoLabel->Size = System::Drawing::Size(13, 13);
			this->BrakefoLabel->TabIndex = 34;
			this->BrakefoLabel->Text = L"0";
			// 
			// AccInfoLabel
			// 
			this->AccInfoLabel->AutoSize = true;
			this->AccInfoLabel->Location = System::Drawing::Point(227, 115);
			this->AccInfoLabel->Name = L"AccInfoLabel";
			this->AccInfoLabel->Size = System::Drawing::Size(13, 13);
			this->AccInfoLabel->TabIndex = 33;
			this->AccInfoLabel->Text = L"0";
			// 
			// SpeedEndSB_label
			// 
			this->SpeedEndSB_label->AutoSize = true;
			this->SpeedEndSB_label->Location = System::Drawing::Point(238, 76);
			this->SpeedEndSB_label->Name = L"SpeedEndSB_label";
			this->SpeedEndSB_label->Size = System::Drawing::Size(50, 13);
			this->SpeedEndSB_label->TabIndex = 32;
			this->SpeedEndSB_label->Text = L"�������";
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(176, 387);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(161, 23);
			this->button3->TabIndex = 8;
			this->button3->Text = L"������ ������� ������";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &MainView::button3_Click);
			// 
			// SpeedBeginLSB_label
			// 
			this->SpeedBeginLSB_label->AutoSize = true;
			this->SpeedBeginLSB_label->Location = System::Drawing::Point(238, 44);
			this->SpeedBeginLSB_label->Name = L"SpeedBeginLSB_label";
			this->SpeedBeginLSB_label->Size = System::Drawing::Size(50, 13);
			this->SpeedBeginLSB_label->TabIndex = 31;
			this->SpeedBeginLSB_label->Text = L"�������";
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(88, 387);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 6;
			this->button2->Text = L"����";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MainView::button2_Click);
			// 
			// panel2
			// 
			this->panel2->Controls->Add(this->radioButton_Direction_Backward);
			this->panel2->Controls->Add(this->radioButton_Direction_Forward);
			this->panel2->Location = System::Drawing::Point(174, 253);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(161, 63);
			this->panel2->TabIndex = 30;
			// 
			// radioButton_Direction_Backward
			// 
			this->radioButton_Direction_Backward->AutoSize = true;
			this->radioButton_Direction_Backward->Location = System::Drawing::Point(4, 30);
			this->radioButton_Direction_Backward->Name = L"radioButton_Direction_Backward";
			this->radioButton_Direction_Backward->Size = System::Drawing::Size(57, 17);
			this->radioButton_Direction_Backward->TabIndex = 1;
			this->radioButton_Direction_Backward->TabStop = true;
			this->radioButton_Direction_Backward->Text = L"�����";
			this->radioButton_Direction_Backward->UseVisualStyleBackColor = true;
			// 
			// radioButton_Direction_Forward
			// 
			this->radioButton_Direction_Forward->AutoSize = true;
			this->radioButton_Direction_Forward->Checked = true;
			this->radioButton_Direction_Forward->Location = System::Drawing::Point(4, 7);
			this->radioButton_Direction_Forward->Name = L"radioButton_Direction_Forward";
			this->radioButton_Direction_Forward->Size = System::Drawing::Size(62, 17);
			this->radioButton_Direction_Forward->TabIndex = 0;
			this->radioButton_Direction_Forward->TabStop = true;
			this->radioButton_Direction_Forward->Text = L"������";
			this->radioButton_Direction_Forward->UseVisualStyleBackColor = true;
			// 
			// MicrosStepcomboBox
			// 
			this->MicrosStepcomboBox->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->MicrosStepcomboBox->FormattingEnabled = true;
			this->MicrosStepcomboBox->Items->AddRange(gcnew cli::array< System::Object^  >(5) {
				L"������������", L"1/2 ", L"1/4 ", L"1/8",
					L"1/16"
			});
			this->MicrosStepcomboBox->Location = System::Drawing::Point(176, 346);
			this->MicrosStepcomboBox->Name = L"MicrosStepcomboBox";
			this->MicrosStepcomboBox->Size = System::Drawing::Size(161, 21);
			this->MicrosStepcomboBox->TabIndex = 10;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(207, 326);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(102, 13);
			this->label6->TabIndex = 11;
			this->label6->Text = L"����� ���������";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(6, 387);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 5;
			this->button1->Text = L"�����";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MainView::button1_Click);
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(212, 228);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(75, 13);
			this->label19->TabIndex = 29;
			this->label19->Text = L"�����������";
			// 
			// panel1
			// 
			this->panel1->Controls->Add(this->ModeSensor2);
			this->panel1->Controls->Add(this->ModeSensor1);
			this->panel1->Controls->Add(this->ModeInfinite);
			this->panel1->Location = System::Drawing::Point(6, 250);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(157, 117);
			this->panel1->TabIndex = 28;
			// 
			// ModeSensor2
			// 
			this->ModeSensor2->AutoSize = true;
			this->ModeSensor2->Location = System::Drawing::Point(4, 55);
			this->ModeSensor2->Name = L"ModeSensor2";
			this->ModeSensor2->Size = System::Drawing::Size(132, 17);
			this->ModeSensor2->TabIndex = 2;
			this->ModeSensor2->TabStop = true;
			this->ModeSensor2->Text = L"��������� �� ����.2";
			this->ModeSensor2->UseVisualStyleBackColor = true;
			// 
			// ModeSensor1
			// 
			this->ModeSensor1->AutoSize = true;
			this->ModeSensor1->Location = System::Drawing::Point(4, 30);
			this->ModeSensor1->Name = L"ModeSensor1";
			this->ModeSensor1->Size = System::Drawing::Size(132, 17);
			this->ModeSensor1->TabIndex = 1;
			this->ModeSensor1->TabStop = true;
			this->ModeSensor1->Text = L"��������� �� ����.1";
			this->ModeSensor1->UseVisualStyleBackColor = true;
			// 
			// ModeInfinite
			// 
			this->ModeInfinite->AutoSize = true;
			this->ModeInfinite->Checked = true;
			this->ModeInfinite->Location = System::Drawing::Point(4, 7);
			this->ModeInfinite->Name = L"ModeInfinite";
			this->ModeInfinite->Size = System::Drawing::Size(97, 17);
			this->ModeInfinite->TabIndex = 0;
			this->ModeInfinite->TabStop = true;
			this->ModeInfinite->Text = L"�����������";
			this->ModeInfinite->UseVisualStyleBackColor = true;
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(179, 149);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(21, 13);
			this->label18->TabIndex = 27;
			this->label18->Text = L"��";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(179, 115);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(21, 13);
			this->label17->TabIndex = 26;
			this->label17->Text = L"��";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(179, 76);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(38, 13);
			this->label15->TabIndex = 25;
			this->label15->Text = L"���/�";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(179, 44);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(38, 13);
			this->label13->TabIndex = 24;
			this->label13->Text = L"���/�";
			// 
			// Stepcount_label1
			// 
			this->Stepcount_label1->AutoSize = true;
			this->Stepcount_label1->Location = System::Drawing::Point(13, 190);
			this->Stepcount_label1->Name = L"Stepcount_label1";
			this->Stepcount_label1->Size = System::Drawing::Size(100, 13);
			this->Stepcount_label1->TabIndex = 21;
			this->Stepcount_label1->Text = L"���������� �����";
			// 
			// MicrosStepCount_textBox
			// 
			this->MicrosStepCount_textBox->Location = System::Drawing::Point(124, 187);
			this->MicrosStepCount_textBox->Name = L"MicrosStepCount_textBox";
			this->MicrosStepCount_textBox->Size = System::Drawing::Size(49, 20);
			this->MicrosStepCount_textBox->TabIndex = 20;
			// 
			// TimeBrake_label1
			// 
			this->TimeBrake_label1->AutoSize = true;
			this->TimeBrake_label1->Location = System::Drawing::Point(13, 156);
			this->TimeBrake_label1->Name = L"TimeBrake_label1";
			this->TimeBrake_label1->Size = System::Drawing::Size(106, 13);
			this->TimeBrake_label1->TabIndex = 19;
			this->TimeBrake_label1->Text = L"����� ����������";
			// 
			// TimeAcc_label
			// 
			this->TimeAcc_label->AutoSize = true;
			this->TimeAcc_label->Location = System::Drawing::Point(34, 119);
			this->TimeAcc_label->Name = L"TimeAcc_label";
			this->TimeAcc_label->Size = System::Drawing::Size(84, 13);
			this->TimeAcc_label->TabIndex = 18;
			this->TimeAcc_label->Text = L"����� �������";
			// 
			// SpeedEnd_label
			// 
			this->SpeedEnd_label->AutoSize = true;
			this->SpeedEnd_label->Location = System::Drawing::Point(7, 80);
			this->SpeedEnd_label->Name = L"SpeedEnd_label";
			this->SpeedEnd_label->Size = System::Drawing::Size(105, 13);
			this->SpeedEnd_label->TabIndex = 17;
			this->SpeedEnd_label->Text = L"�������� ��������";
			// 
			// SpeedBegin_label
			// 
			this->SpeedBegin_label->AutoSize = true;
			this->SpeedBegin_label->Location = System::Drawing::Point(7, 44);
			this->SpeedBegin_label->Name = L"SpeedBegin_label";
			this->SpeedBegin_label->Size = System::Drawing::Size(111, 13);
			this->SpeedBegin_label->TabIndex = 16;
			this->SpeedBegin_label->Text = L"�������� ���������";
			// 
			// TimeBrake_textBox
			// 
			this->TimeBrake_textBox->Location = System::Drawing::Point(124, 153);
			this->TimeBrake_textBox->Name = L"TimeBrake_textBox";
			this->TimeBrake_textBox->Size = System::Drawing::Size(49, 20);
			this->TimeBrake_textBox->TabIndex = 15;
			// 
			// TimeAcceleration_textBox
			// 
			this->TimeAcceleration_textBox->Location = System::Drawing::Point(124, 116);
			this->TimeAcceleration_textBox->Name = L"TimeAcceleration_textBox";
			this->TimeAcceleration_textBox->Size = System::Drawing::Size(49, 20);
			this->TimeAcceleration_textBox->TabIndex = 14;
			// 
			// SpeedEnd_textBox
			// 
			this->SpeedEnd_textBox->Location = System::Drawing::Point(124, 77);
			this->SpeedEnd_textBox->Name = L"SpeedEnd_textBox";
			this->SpeedEnd_textBox->Size = System::Drawing::Size(49, 20);
			this->SpeedEnd_textBox->TabIndex = 13;
			// 
			// SpeedBegin_textBox
			// 
			this->SpeedBegin_textBox->Location = System::Drawing::Point(124, 41);
			this->SpeedBegin_textBox->Name = L"SpeedBegin_textBox";
			this->SpeedBegin_textBox->Size = System::Drawing::Size(49, 20);
			this->SpeedBegin_textBox->TabIndex = 12;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(38, 228);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(82, 13);
			this->label4->TabIndex = 9;
			this->label4->Text = L"����� ������";
			// 
			// MianmenuStrip
			// 
			this->MianmenuStrip->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->fTDIToolStripMenuItem,
					this->������������������������ToolStripMenuItem, this->�������ToolStripMenuItem
			});
			this->MianmenuStrip->Location = System::Drawing::Point(0, 0);
			this->MianmenuStrip->Name = L"MianmenuStrip";
			this->MianmenuStrip->Size = System::Drawing::Size(540, 24);
			this->MianmenuStrip->TabIndex = 7;
			this->MianmenuStrip->Text = L"menuStrip1";
			// 
			// fTDIToolStripMenuItem
			// 
			this->fTDIToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {
				this->SetuoFTDIToolStripMenuItem,
					this->������������ToolStripMenuItem, this->����������ToolStripMenuItem, this->��������ToolStripMenuItem
			});
			this->fTDIToolStripMenuItem->Name = L"fTDIToolStripMenuItem";
			this->fTDIToolStripMenuItem->Size = System::Drawing::Size(43, 20);
			this->fTDIToolStripMenuItem->Text = L"FTDI";
			// 
			// SetuoFTDIToolStripMenuItem
			// 
			this->SetuoFTDIToolStripMenuItem->Name = L"SetuoFTDIToolStripMenuItem";
			this->SetuoFTDIToolStripMenuItem->Size = System::Drawing::Size(156, 22);
			this->SetuoFTDIToolStripMenuItem->Text = L"���������";
			this->SetuoFTDIToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::SetuoFTDIToolStripMenuItem_Click);
			// 
			// ������������ToolStripMenuItem
			// 
			this->������������ToolStripMenuItem->Name = L"������������ToolStripMenuItem";
			this->������������ToolStripMenuItem->Size = System::Drawing::Size(156, 22);
			this->������������ToolStripMenuItem->Text = L"������������";
			this->������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::������������ToolStripMenuItem_Click);
			// 
			// ����������ToolStripMenuItem
			// 
			this->����������ToolStripMenuItem->Name = L"����������ToolStripMenuItem";
			this->����������ToolStripMenuItem->Size = System::Drawing::Size(156, 22);
			this->����������ToolStripMenuItem->Text = L"����������";
			this->����������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::����������ToolStripMenuItem_Click);
			// 
			// ��������ToolStripMenuItem
			// 
			this->��������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->������������ToolStripMenuItem,
					this->����������������ToolStripMenuItem
			});
			this->��������ToolStripMenuItem->Name = L"��������ToolStripMenuItem";
			this->��������ToolStripMenuItem->Size = System::Drawing::Size(156, 22);
			this->��������ToolStripMenuItem->Text = L"��������";
			// 
			// ������������ToolStripMenuItem
			// 
			this->������������ToolStripMenuItem->Checked = true;
			this->������������ToolStripMenuItem->CheckOnClick = true;
			this->������������ToolStripMenuItem->CheckState = System::Windows::Forms::CheckState::Checked;
			this->������������ToolStripMenuItem->Name = L"������������ToolStripMenuItem";
			this->������������ToolStripMenuItem->Size = System::Drawing::Size(192, 22);
			this->������������ToolStripMenuItem->Text = L"������ ������";
			this->������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::������������ToolStripMenuItem_Click);
			// 
			// ����������������ToolStripMenuItem
			// 
			this->����������������ToolStripMenuItem->CheckOnClick = true;
			this->����������������ToolStripMenuItem->Name = L"����������������ToolStripMenuItem";
			this->����������������ToolStripMenuItem->Size = System::Drawing::Size(192, 22);
			this->����������������ToolStripMenuItem->Text = L"������ ���� ��������";
			this->����������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::����������������ToolStripMenuItem_Click);
			// 
			// ������������������������ToolStripMenuItem
			// 
			this->������������������������ToolStripMenuItem->Name = L"������������������������ToolStripMenuItem";
			this->������������������������ToolStripMenuItem->Size = System::Drawing::Size(182, 20);
			this->������������������������ToolStripMenuItem->Text = L"�������� �������� ���������";
			this->������������������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::�����������������������������ToolStripMenuItem_Click);
			// 
			// �������ToolStripMenuItem
			// 
			this->�������ToolStripMenuItem->Name = L"�������ToolStripMenuItem";
			this->�������ToolStripMenuItem->Size = System::Drawing::Size(65, 20);
			this->�������ToolStripMenuItem->Text = L"�������";
			this->�������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::�������ToolStripMenuItem_Click);
			// 
			// POllingtimer
			// 
			this->POllingtimer->Interval = 5;
			this->POllingtimer->Tick += gcnew System::EventHandler(this, &MainView::POllingtimer_Tick);
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->WDT_Label);
			this->groupBox4->Controls->Add(this->WS_Label);
			this->groupBox4->Controls->Add(this->label14);
			this->groupBox4->Controls->Add(this->label16);
			this->groupBox4->Location = System::Drawing::Point(7, 247);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(167, 89);
			this->groupBox4->TabIndex = 22;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"��������� ����������  �������";
			// 
			// WDT_Label
			// 
			this->WDT_Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->WDT_Label->Location = System::Drawing::Point(81, 34);
			this->WDT_Label->Name = L"WDT_Label";
			this->WDT_Label->Size = System::Drawing::Size(15, 15);
			this->WDT_Label->TabIndex = 6;
			// 
			// WS_Label
			// 
			this->WS_Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->WS_Label->Location = System::Drawing::Point(24, 36);
			this->WS_Label->Name = L"WS_Label";
			this->WS_Label->Size = System::Drawing::Size(15, 15);
			this->WS_Label->TabIndex = 5;
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(50, 62);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(82, 13);
			this->label14->TabIndex = 3;
			this->label14->Text = L"Watchdog timer";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(19, 59);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(25, 13);
			this->label16->TabIndex = 1;
			this->label16->Text = L"WS";
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->NOT_PERF_CMD_Label);
			this->groupBox1->Controls->Add(this->label53);
			this->groupBox1->Controls->Add(this->DIR_Label);
			this->groupBox1->Controls->Add(this->SCK_Mode__Label);
			this->groupBox1->Controls->Add(this->label47);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->WRONG_CMD__Label);
			this->groupBox1->Controls->Add(this->OCD_Label);
			this->groupBox1->Controls->Add(this->label49);
			this->groupBox1->Controls->Add(this->Hiz_Label);
			this->groupBox1->Controls->Add(this->label51);
			this->groupBox1->Controls->Add(this->label45);
			this->groupBox1->Controls->Add(this->BUSY_Label);
			this->groupBox1->Controls->Add(this->label43);
			this->groupBox1->Controls->Add(this->SW_EVN_Label);
			this->groupBox1->Controls->Add(this->TH_WRN__Label);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->label41);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->TH_SD__Label);
			this->groupBox1->Controls->Add(this->SW_F_Label);
			this->groupBox1->Controls->Add(this->UVLO);
			this->groupBox1->Controls->Add(this->label39);
			this->groupBox1->Controls->Add(this->UVLO_Label);
			this->groupBox1->Location = System::Drawing::Point(10, 468);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(511, 136);
			this->groupBox1->TabIndex = 1;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"���������  ��������";
			// 
			// NOT_PERF_CMD_Label
			// 
			this->NOT_PERF_CMD_Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->NOT_PERF_CMD_Label->Location = System::Drawing::Point(56, 28);
			this->NOT_PERF_CMD_Label->Name = L"NOT_PERF_CMD_Label";
			this->NOT_PERF_CMD_Label->Size = System::Drawing::Size(15, 15);
			this->NOT_PERF_CMD_Label->TabIndex = 42;
			// 
			// label53
			// 
			this->label53->Location = System::Drawing::Point(16, 50);
			this->label53->Name = L"label53";
			this->label53->Size = System::Drawing::Size(94, 13);
			this->label53->TabIndex = 41;
			this->label53->Text = L"NOT_PERF_CMD";
			this->label53->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// DIR_Label
			// 
			this->DIR_Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->DIR_Label->Location = System::Drawing::Point(205, 28);
			this->DIR_Label->Name = L"DIR_Label";
			this->DIR_Label->Size = System::Drawing::Size(15, 15);
			this->DIR_Label->TabIndex = 40;
			// 
			// SCK_Mode__Label
			// 
			this->SCK_Mode__Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->SCK_Mode__Label->Location = System::Drawing::Point(56, 83);
			this->SCK_Mode__Label->Name = L"SCK_Mode__Label";
			this->SCK_Mode__Label->Size = System::Drawing::Size(15, 15);
			this->SCK_Mode__Label->TabIndex = 36;
			// 
			// label47
			// 
			this->label47->Location = System::Drawing::Point(28, 106);
			this->label47->Name = L"label47";
			this->label47->Size = System::Drawing::Size(69, 13);
			this->label47->TabIndex = 35;
			this->label47->Text = L"SCK_Mode";
			this->label47->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// label1
			// 
			this->label1->Location = System::Drawing::Point(119, 106);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(44, 13);
			this->label1->TabIndex = 1;
			this->label1->Text = L"OCD";
			this->label1->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// WRONG_CMD__Label
			// 
			this->WRONG_CMD__Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->WRONG_CMD__Label->Location = System::Drawing::Point(407, 83);
			this->WRONG_CMD__Label->Name = L"WRONG_CMD__Label";
			this->WRONG_CMD__Label->Size = System::Drawing::Size(15, 15);
			this->WRONG_CMD__Label->TabIndex = 38;
			// 
			// OCD_Label
			// 
			this->OCD_Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->OCD_Label->Location = System::Drawing::Point(136, 83);
			this->OCD_Label->Name = L"OCD_Label";
			this->OCD_Label->Size = System::Drawing::Size(15, 15);
			this->OCD_Label->TabIndex = 5;
			// 
			// label49
			// 
			this->label49->Location = System::Drawing::Point(371, 106);
			this->label49->Name = L"label49";
			this->label49->Size = System::Drawing::Size(86, 13);
			this->label49->TabIndex = 37;
			this->label49->Text = L"WRONG_CMD";
			this->label49->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// Hiz_Label
			// 
			this->Hiz_Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->Hiz_Label->Location = System::Drawing::Point(407, 28);
			this->Hiz_Label->Name = L"Hiz_Label";
			this->Hiz_Label->Size = System::Drawing::Size(15, 15);
			this->Hiz_Label->TabIndex = 34;
			// 
			// label51
			// 
			this->label51->Location = System::Drawing::Point(180, 50);
			this->label51->Name = L"label51";
			this->label51->Size = System::Drawing::Size(61, 13);
			this->label51->TabIndex = 39;
			this->label51->Text = L"DIR";
			this->label51->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// label45
			// 
			this->label45->Location = System::Drawing::Point(395, 50);
			this->label45->Name = L"label45";
			this->label45->Size = System::Drawing::Size(36, 13);
			this->label45->TabIndex = 33;
			this->label45->Text = L"Hiz";
			this->label45->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// BUSY_Label
			// 
			this->BUSY_Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->BUSY_Label->Location = System::Drawing::Point(345, 28);
			this->BUSY_Label->Name = L"BUSY_Label";
			this->BUSY_Label->Size = System::Drawing::Size(15, 15);
			this->BUSY_Label->TabIndex = 32;
			// 
			// label43
			// 
			this->label43->Location = System::Drawing::Point(338, 50);
			this->label43->Name = L"label43";
			this->label43->Size = System::Drawing::Size(37, 13);
			this->label43->TabIndex = 31;
			this->label43->Text = L"BUSY";
			this->label43->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// SW_EVN_Label
			// 
			this->SW_EVN_Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->SW_EVN_Label->Location = System::Drawing::Point(138, 28);
			this->SW_EVN_Label->Name = L"SW_EVN_Label";
			this->SW_EVN_Label->Size = System::Drawing::Size(15, 15);
			this->SW_EVN_Label->TabIndex = 30;
			// 
			// TH_WRN__Label
			// 
			this->TH_WRN__Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->TH_WRN__Label->Location = System::Drawing::Point(273, 83);
			this->TH_WRN__Label->Name = L"TH_WRN__Label";
			this->TH_WRN__Label->Size = System::Drawing::Size(15, 15);
			this->TH_WRN__Label->TabIndex = 24;
			// 
			// label3
			// 
			this->label3->Location = System::Drawing::Point(169, 105);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(76, 13);
			this->label3->TabIndex = 4;
			this->label3->Text = L"TH_SD";
			this->label3->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// label41
			// 
			this->label41->Location = System::Drawing::Point(111, 50);
			this->label41->Name = L"label41";
			this->label41->Size = System::Drawing::Size(67, 13);
			this->label41->TabIndex = 29;
			this->label41->Text = L"SW_EVN";
			this->label41->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// label2
			// 
			this->label2->Location = System::Drawing::Point(239, 105);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(85, 13);
			this->label2->TabIndex = 3;
			this->label2->Text = L"TH_WRN";
			this->label2->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// TH_SD__Label
			// 
			this->TH_SD__Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->TH_SD__Label->Location = System::Drawing::Point(200, 83);
			this->TH_SD__Label->Name = L"TH_SD__Label";
			this->TH_SD__Label->Size = System::Drawing::Size(15, 15);
			this->TH_SD__Label->TabIndex = 26;
			// 
			// SW_F_Label
			// 
			this->SW_F_Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->SW_F_Label->Location = System::Drawing::Point(273, 28);
			this->SW_F_Label->Name = L"SW_F_Label";
			this->SW_F_Label->Size = System::Drawing::Size(15, 15);
			this->SW_F_Label->TabIndex = 28;
			// 
			// UVLO
			// 
			this->UVLO->Location = System::Drawing::Point(330, 105);
			this->UVLO->Name = L"UVLO";
			this->UVLO->Size = System::Drawing::Size(36, 13);
			this->UVLO->TabIndex = 2;
			this->UVLO->Text = L"UVLO";
			this->UVLO->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// label39
			// 
			this->label39->Location = System::Drawing::Point(239, 50);
			this->label39->Name = L"label39";
			this->label39->Size = System::Drawing::Size(82, 13);
			this->label39->TabIndex = 27;
			this->label39->Text = L"SW_F";
			this->label39->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// UVLO_Label
			// 
			this->UVLO_Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->UVLO_Label->Location = System::Drawing::Point(341, 83);
			this->UVLO_Label->Name = L"UVLO_Label";
			this->UVLO_Label->Size = System::Drawing::Size(15, 15);
			this->UVLO_Label->TabIndex = 25;
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->Sensor2_Label);
			this->groupBox2->Controls->Add(this->Sensor1_Label);
			this->groupBox2->Controls->Add(this->label5);
			this->groupBox2->Controls->Add(this->label7);
			this->groupBox2->Location = System::Drawing::Point(7, 342);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(160, 75);
			this->groupBox2->TabIndex = 3;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"�������� ��������";
			// 
			// Sensor2_Label
			// 
			this->Sensor2_Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->Sensor2_Label->Location = System::Drawing::Point(81, 34);
			this->Sensor2_Label->Name = L"Sensor2_Label";
			this->Sensor2_Label->Size = System::Drawing::Size(15, 15);
			this->Sensor2_Label->TabIndex = 5;
			// 
			// Sensor1_Label
			// 
			this->Sensor1_Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->Sensor1_Label->Location = System::Drawing::Point(19, 33);
			this->Sensor1_Label->Name = L"Sensor1_Label";
			this->Sensor1_Label->Size = System::Drawing::Size(15, 15);
			this->Sensor1_Label->TabIndex = 4;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(68, 51);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(41, 13);
			this->label5->TabIndex = 3;
			this->label5->Text = L"����,2";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(10, 51);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(44, 13);
			this->label7->TabIndex = 1;
			this->label7->Text = L"����. 1";
			// 
			// groupBox6
			// 
			this->groupBox6->Controls->Add(this->MOduleStateWord_Label);
			this->groupBox6->Controls->Add(this->DriverStateWord_Label);
			this->groupBox6->Controls->Add(this->label23);
			this->groupBox6->Controls->Add(this->label22);
			this->groupBox6->Location = System::Drawing::Point(0, 153);
			this->groupBox6->Name = L"groupBox6";
			this->groupBox6->Size = System::Drawing::Size(167, 78);
			this->groupBox6->TabIndex = 23;
			this->groupBox6->TabStop = false;
			this->groupBox6->Text = L"Hex ��������� �����";
			// 
			// MOduleStateWord_Label
			// 
			this->MOduleStateWord_Label->AutoSize = true;
			this->MOduleStateWord_Label->Location = System::Drawing::Point(122, 56);
			this->MOduleStateWord_Label->Name = L"MOduleStateWord_Label";
			this->MOduleStateWord_Label->Size = System::Drawing::Size(37, 13);
			this->MOduleStateWord_Label->TabIndex = 3;
			this->MOduleStateWord_Label->Text = L"L6472";
			// 
			// DriverStateWord_Label
			// 
			this->DriverStateWord_Label->AutoSize = true;
			this->DriverStateWord_Label->Location = System::Drawing::Point(122, 33);
			this->DriverStateWord_Label->Name = L"DriverStateWord_Label";
			this->DriverStateWord_Label->Size = System::Drawing::Size(37, 13);
			this->DriverStateWord_Label->TabIndex = 2;
			this->DriverStateWord_Label->Text = L"L6472";
			// 
			// label23
			// 
			this->label23->AutoSize = true;
			this->label23->Location = System::Drawing::Point(9, 56);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(45, 13);
			this->label23->TabIndex = 1;
			this->label23->Text = L"������";
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Location = System::Drawing::Point(9, 33);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(107, 13);
			this->label22->TabIndex = 0;
			this->label22->Text = L"������� ���������";
			// 
			// groupBox7
			// 
			this->groupBox7->Controls->Add(this->VersionHW_Label);
			this->groupBox7->Controls->Add(this->VersionSW_Label);
			this->groupBox7->Controls->Add(this->label55);
			this->groupBox7->Controls->Add(this->label54);
			this->groupBox7->Controls->Add(this->label37);
			this->groupBox7->Controls->Add(this->ModuleConectionStae_Label);
			this->groupBox7->Controls->Add(this->FTDIConectionState_label);
			this->groupBox7->Controls->Add(this->label35);
			this->groupBox7->Location = System::Drawing::Point(6, 19);
			this->groupBox7->Name = L"groupBox7";
			this->groupBox7->Size = System::Drawing::Size(161, 128);
			this->groupBox7->TabIndex = 33;
			this->groupBox7->TabStop = false;
			this->groupBox7->Text = L"��������� �����������";
			// 
			// VersionHW_Label
			// 
			this->VersionHW_Label->AutoSize = true;
			this->VersionHW_Label->Location = System::Drawing::Point(108, 102);
			this->VersionHW_Label->Name = L"VersionHW_Label";
			this->VersionHW_Label->Size = System::Drawing::Size(13, 13);
			this->VersionHW_Label->TabIndex = 12;
			this->VersionHW_Label->Text = L"0";
			// 
			// VersionSW_Label
			// 
			this->VersionSW_Label->AutoSize = true;
			this->VersionSW_Label->Location = System::Drawing::Point(108, 80);
			this->VersionSW_Label->Name = L"VersionSW_Label";
			this->VersionSW_Label->Size = System::Drawing::Size(13, 13);
			this->VersionSW_Label->TabIndex = 11;
			this->VersionSW_Label->Text = L"0";
			// 
			// label55
			// 
			this->label55->AutoSize = true;
			this->label55->Location = System::Drawing::Point(6, 102);
			this->label55->Name = L"label55";
			this->label55->Size = System::Drawing::Size(88, 13);
			this->label55->TabIndex = 10;
			this->label55->Text = L"������ ������:";
			// 
			// label54
			// 
			this->label54->AutoSize = true;
			this->label54->Location = System::Drawing::Point(8, 80);
			this->label54->Name = L"label54";
			this->label54->Size = System::Drawing::Size(66, 13);
			this->label54->TabIndex = 9;
			this->label54->Text = L"������ ��:";
			// 
			// label37
			// 
			this->label37->AutoSize = true;
			this->label37->Location = System::Drawing::Point(39, 58);
			this->label37->Name = L"label37";
			this->label37->Size = System::Drawing::Size(44, 13);
			this->label37->TabIndex = 8;
			this->label37->Text = L"������";
			// 
			// ModuleConectionStae_Label
			// 
			this->ModuleConectionStae_Label->BackColor = System::Drawing::Color::Gray;
			this->ModuleConectionStae_Label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->ModuleConectionStae_Label->Location = System::Drawing::Point(18, 56);
			this->ModuleConectionStae_Label->Name = L"ModuleConectionStae_Label";
			this->ModuleConectionStae_Label->Size = System::Drawing::Size(15, 15);
			this->ModuleConectionStae_Label->TabIndex = 7;
			// 
			// FTDIConectionState_label
			// 
			this->FTDIConectionState_label->BackColor = System::Drawing::Color::Gray;
			this->FTDIConectionState_label->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->FTDIConectionState_label->Location = System::Drawing::Point(18, 29);
			this->FTDIConectionState_label->Name = L"FTDIConectionState_label";
			this->FTDIConectionState_label->Size = System::Drawing::Size(15, 15);
			this->FTDIConectionState_label->TabIndex = 6;
			// 
			// label35
			// 
			this->label35->AutoSize = true;
			this->label35->Location = System::Drawing::Point(39, 31);
			this->label35->Name = L"label35";
			this->label35->Size = System::Drawing::Size(31, 13);
			this->label35->TabIndex = 5;
			this->label35->Text = L"FTDI";
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->groupBox7);
			this->groupBox5->Controls->Add(this->groupBox6);
			this->groupBox5->Controls->Add(this->groupBox2);
			this->groupBox5->Controls->Add(this->groupBox4);
			this->groupBox5->Location = System::Drawing::Point(355, 39);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Size = System::Drawing::Size(172, 432);
			this->groupBox5->TabIndex = 23;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = L"�����c";
			// 
			// MainView
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
			this->ClientSize = System::Drawing::Size(540, 629);
			this->Controls->Add(this->groupBox5);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->MainstatusStrip);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->MianmenuStrip);
			this->DoubleBuffered = true;
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->HelpButton = true;
			this->MainMenuStrip = this->MianmenuStrip;
			this->MaximizeBox = false;
			this->Name = L"MainView";
			this->Text = L"����� ���������� ������� ���������� ������������ 0.1";
			this->MainstatusStrip->ResumeLayout(false);
			this->MainstatusStrip->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->panel2->ResumeLayout(false);
			this->panel2->PerformLayout();
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			this->MianmenuStrip->ResumeLayout(false);
			this->MianmenuStrip->PerformLayout();
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			this->groupBox1->ResumeLayout(false);
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox6->ResumeLayout(false);
			this->groupBox6->PerformLayout();
			this->groupBox7->ResumeLayout(false);
			this->groupBox7->PerformLayout();
			this->groupBox5->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
/***********************************************************************************************************************************************************/
		 System::Void MainView::�����������������������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			

			if (!RegisterView->Visible )
			{
				
				if (RegisterView->IsDisposed)
				{
					RegisterView = gcnew DriverRegisterView();
					

				}

				RegisterView->Module = DF;
				RegisterView->Module->get()->Init(FTDI);
				RegisterView->SetTimeerHandle(POllingtimer);
				RegisterView->Show();

			}
		}



		 /***********************************************************************************************************************************************************/
void MainView::ShowFTDIDialog(void)
{
	if (!FT_Setup->Visible)
	{
		if (FT_Setup->IsDisposed)
		{
			FT_Setup = gcnew FTDIView();
			FT_Setup->SetParametrsFTDI(nSpeed, nDataBits, nStopBits);
			FT_Setup->SetMXComm(FTDI);
		}
		else
		{
			FT_Setup->SetParametrsFTDI(nSpeed, nDataBits, nStopBits);
			FT_Setup->SetMXComm(FTDI);

		}
		if (FT_Setup->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			FT_Setup->GetParametrsFTDI(nSpeed, nDataBits, nStopBits);
			SelectedDevice = FT_Setup->GetSelectedDeviceID();
		}

	}



}
/***********************************************************************************************************************************************************/
void DF_EnginerPult::MainView::DecodeModuleParametr(bool Err,int * VontrolWord, int * SpeedBegin, int * SpeedEnd, int * TimeAcc, int * TimeBrake, int * StepMode, int* StepCount)
{


	if (!Err)
	{

	SpeedBeginLSB_label->Text = String::Format("{0:0} [�������]", *SpeedBegin);
	SpeedEndSB_label->Text = String::Format("{0:0} [�������]", *SpeedEnd);
	*SpeedBegin = *SpeedBegin  *  g_CoenficinetMinSpeed;
	*SpeedEnd = (*SpeedEnd)  * g_CoenficinetSpeed;
	SpeedBegin_textBox->Text = (*SpeedBegin + 1).ToString();
	SpeedEnd_textBox->Text = (*SpeedEnd + 1).ToString();
	TimeAcceleration_textBox->Text = (*TimeAcc).ToString();
	TimeBrake_textBox->Text = (*TimeBrake).ToString();
	MicrosStepCount_textBox->Text = (*StepCount).ToString();
	MicrosStepcomboBox->SelectedIndex = *StepMode;

	if (*VontrolWord & 0x1 == 1)
	{

		radioButton_Direction_Backward->Checked = true;
		radioButton_Direction_Forward->Checked = false;

	}
	else
	{
		radioButton_Direction_Backward->Checked = false;
		radioButton_Direction_Forward->Checked = true;
	}

	int Mode = (*VontrolWord >> 1) & 0x3;


	switch (Mode)
	{

	case 0:
	{
		ModeInfinite->Checked = true;
		break;
	}


	case 1:
	{
		ModeSensor1->Checked = true;
		break;
	}

	case 2:
	{
		ModeSensor2->Checked = true;
		break;
	}
	}
}
		 else
		 {

			 SpeedBeginLSB_label->Text = String::Empty;
			 SpeedEndSB_label->Text = String::Empty;
			 SpeedBegin_textBox->Text = String::Empty;
			 SpeedEnd_textBox->Text = String::Empty;
			 TimeAcceleration_textBox->Text = String::Empty;
			 TimeBrake_textBox->Text = String::Empty;
			 MicrosStepCount_textBox->Text = String::Empty;
			 MotorSpeed_toolStripStatusLabel->Text = String::Empty;
			
		 }

}

/***********************************************************************************************************************************************************/
		 System::Void MainView::SetuoFTDIToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

			 ShowFTDIDialog();
			
		 }
/***********************************************************************************************************************************************************/
		  System::Void MainView::������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

			 FTDIConectionState_label->BackColor = System::Drawing::Color::Red;
			 ModuleConectionStae_Label->BackColor = System::Drawing::Color::Red;
			 POllingtimer->Enabled = false;

			 FTDI->get()->Close();

			 if (FTDI->get()->ConfigureByID(SelectedDevice, nSpeed, nDataBits, nParity, nStopBits))
			 {
				 BYTE SW[4];
				 BYTE HW[4];
				 BYTE Name[4];
				 FTDI->get()->SetTimeout(100);
				 DF->get()->Init(FTDI);
				 FTDI->get()->PurgeRX();
				 FTDIConectionState_label->BackColor = System::Drawing::Color::Green;
				 if (DF->get()->ReadVersion(Name, HW, SW) == 0)
				 {
					 if (!strcmp((const char *)Name, "DF"))
					 {
						 VersionSW_Label->Text = SW[0] + "." + SW[1];
						 VersionHW_Label->Text = HW[0] + "." + HW[1];

						 ModuleConectionStae_Label->BackColor = System::Drawing::Color::Green;
						 POllingtimer->Enabled = true;
					 }

				 }
			 }

		 }
/***********************************************************************************************************/
    System::Void MainView::����������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	FTDI->get()->Close();
	FTDIConectionState_label->BackColor = System::Drawing::Color::Red;
	ModuleConectionStae_Label->BackColor = System::Drawing::Color::Red;
	InitLabels();
	POllingtimer->Enabled = false;
}
/***********************************************************************************************************/
	void MainView::DecodeStatusWord(int* Word)
	{

		
		int State = *Word;
		if (State & 1 << 0)
		{
			Hiz_Label->BackColor = System::Drawing::Color::Orange;
		}
		else
		{
			Hiz_Label->BackColor = System::Drawing::Color::Green;
		}

		if (State & 1 << 1)
		{
			BUSY_Label->BackColor = System::Drawing::Color::Orange;
		}
		else
		{
			BUSY_Label->BackColor = System::Drawing::Color::Green;
		}


		if (State & 1 << 8)
		{

			WRONG_CMD__Label->BackColor = System::Drawing::Color::Orange;
		}
		else
		{
			WRONG_CMD__Label->BackColor = System::Drawing::Color::Green;
		}


		if (State & 1 << 7)
		{
			NOT_PERF_CMD_Label->BackColor = System::Drawing::Color::Orange;
		}
		else
		{
			NOT_PERF_CMD_Label->BackColor = System::Drawing::Color::Green;
		}

		if (State & 1 << 2)
		{
			SW_F_Label->BackColor = System::Drawing::Color::Orange;
		}
		else
		{
			SW_F_Label->BackColor = System::Drawing::Color::Green;
		}

		if (State & 1 << 3)
		{
			SW_EVN_Label->BackColor = System::Drawing::Color::Orange;
		}
		else
		{
			SW_EVN_Label->BackColor = System::Drawing::Color::Green;
		}

		if (State & 1 << 4)
		{
			//MotorDirection_toolStripStatusLabel->Text = "������";
			DIR_Label->BackColor = System::Drawing::Color::Green;
		}
		else
		{
			//MotorDirection_toolStripStatusLabel->Text = "�����";
			DIR_Label->BackColor = System::Drawing::Color::Orange;
			
		}

		if (State & 1 << 9)
		{
			UVLO_Label->BackColor = System::Drawing::Color::Green;
		}
		else
		{
			UVLO_Label->BackColor = System::Drawing::Color::Orange;
		}


		if (State & 1 << 10)
		{
			TH_WRN__Label->BackColor = System::Drawing::Color::Green;
		}
		else
		{
			TH_WRN__Label->BackColor = System::Drawing::Color::Orange;
		}

		if (State & 1 << 11)
		{
			TH_SD__Label->BackColor = System::Drawing::Color::Green;
		}
		else
		{
			TH_SD__Label->BackColor = System::Drawing::Color::Orange;
		}

		if (State & 1 << 12)
		{
			OCD_Label->BackColor  = System::Drawing::Color::Green;
		}
		else
		{
			OCD_Label->BackColor = System::Drawing::Color::Orange;
		}

		if (State & 1 << 19)
		{
			WS_Label->BackColor  = System::Drawing::Color::Orange;
		}
		else
		{
			WS_Label->BackColor = System::Drawing::Color::Green;
		}

		if (State & 1 << 20)
		{
			WDT_Label->BackColor  = System::Drawing::Color::Red;
		}
		else
		{
			WDT_Label->BackColor = System::Drawing::Color::Green;
		}

		if (State & 1 << 15)
		{
			SCK_Mode__Label->BackColor = System::Drawing::Color::Orange;
		}
		else
		{
			SCK_Mode__Label->BackColor = System::Drawing::Color::Green;
		}

		if (State & 1 << 16)
		{
			Sensor1_Label->BackColor = System::Drawing::Color::Green;
		}
				
		else
		{
			Sensor1_Label->BackColor = System::Drawing::Color::Gray;
		}

		if (State & 1 << 17)
		{
			Sensor2_Label->BackColor = System::Drawing::Color::Green;
			
		}
		else
		{
			Sensor2_Label->BackColor = System::Drawing::Color::Gray;
		}


	}

/*************************************************************************************************************/
     System::Void MainView::button1_Click(System::Object^  sender, System::EventArgs^  e) {

		 if (DF->get()->m_Comm->get()->m_FTDI != NULL || DF->get()->m_Comm->get()->m_FTDI != INVALID_HANDLE_VALUE)
		 {

			 bool FlagExe = true;
			 int ControlWord = 0;
			 int BeginSpeed;
			 int EndSpeed;
			 int TimeStart;
			 int TimeStop;
			 int StepCount;
			 int StepMode;

			 double Acceleration = 0;
			 double Deceleration = 0;
			 double StepCountAcc    = 0;
			 double StepCountBrake  = 0;
			 int MicrosStepsScaleFactor = 0;
			 StepMode = MicrosStepcomboBox->SelectedIndex;
			 int StepMultiFactor = 0;

			 switch (StepMode)
			 {

			 case 0:
			 {
				 StepMultiFactor = 1;
				 break;
			 }


			 case 1:
			 {

				 StepMultiFactor = 2;
				 break;
			 }


			 case 2:
			 {

				 StepMultiFactor = 4;
				 break;
			 }



			 case 3:
			 {

				 StepMultiFactor = 8;
				 break;
			 }


			 case 4:
			 {

				 StepMultiFactor = 16;
				 break;
			 }


			 default:
				 break;
			 }

			 if (radioButton_Direction_Backward->Checked)
			 {
				 ControlWord = 0;
			 }

			 if (radioButton_Direction_Backward->Checked)
			 {
				 ControlWord = 1;
			 }

			 if (ModeSensor1->Checked)
			 {
				 ControlWord |= 0x2;
			 }

			 if (ModeSensor2->Checked)
			 {
				 ControlWord |= 0x4;
			 }

			 if (ModeInfinite->Checked)
			 {
				 ControlWord |= 0x0;
			 }


			 if (int::TryParse(SpeedBegin_textBox->Text, BeginSpeed))

			 {
				 if (BeginSpeed < 0 || BeginSpeed > 976)
				 {

					 SpeedBegin_label->ForeColor = System::Drawing::Color::Red;
					 FlagExe = false;
				 }
				 else
				 {
					 SpeedBegin_label->ForeColor = System::Drawing::Color::Black;

				 }

			 }
			 else
			 {
				 SpeedBegin_label->ForeColor = System::Drawing::Color::Red;
				 FlagExe = false;
			 }


			 if (int::TryParse(SpeedEnd_textBox->Text, EndSpeed))
			 {

				 if (EndSpeed > 15625 || EndSpeed < 0)
				 {
					 SpeedEnd_label->ForeColor = System::Drawing::Color::Red;
					 FlagExe = false;
				 }
				 else
				 {
					 SpeedEnd_label->ForeColor = System::Drawing::Color::Black;

				 }

			 }
			 else
			 {
				 SpeedEnd_label->ForeColor = System::Drawing::Color::Red;
				 FlagExe = false;

			 }


			 if (int::TryParse(MicrosStepCount_textBox->Text, StepCount))
			 {

				 if (StepCount < 0 || StepCount > 2097152)
				 {
					 Stepcount_label1->ForeColor = System::Drawing::Color::Red;
					 FlagExe = false;
				 }
				 else
				 {
					 Stepcount_label1->ForeColor = System::Drawing::Color::Black;
				 }

			 }
			 else
			 {
				 Stepcount_label1->ForeColor = System::Drawing::Color::Red;
				 FlagExe = false;

			 }


			 if (int::TryParse(TimeAcceleration_textBox->Text, TimeStart))
			 {

				 if (TimeStart < 0 || TimeStart > 16383)
				 {

					 TimeAcc_label->ForeColor = System::Drawing::Color::Red;
					 FlagExe = false;
				 }
				 else
				 {
					 TimeAcc_label->ForeColor = System::Drawing::Color::Black;
				 }

			 }
			 else
			 {
				 TimeAcc_label->ForeColor = System::Drawing::Color::Red;
				 FlagExe = false;

			 }

			 if (int::TryParse(TimeBrake_textBox->Text, TimeStop))
			 {
				 if (TimeStop < 0 || TimeStop > 16383)
				 {

					 TimeBrake_label1->ForeColor = System::Drawing::Color::Red;
					 FlagExe = false;
				 }
				 else
				 {

					 TimeBrake_label1->ForeColor = System::Drawing::Color::Black;

				 }

			 }
			 else
			 {
				 TimeBrake_label1->ForeColor = System::Drawing::Color::Red;
				 FlagExe = false;
			 }


			 if (!FlagExe)
			 {
				 return;
			 }

			 SetCurrentDirectory(ProgramPath);
			 
			 double TimeMoveAcc = 0;
			 double TimeBrakeDecc = 0;
			 if (TimeStart > 0)
			 {
				 TimeMoveAcc = TimeStart * 0.001;
			 }
			 else
			 {
				 TimeMoveAcc = 1;
			 }
			 



			 if (TimeStop > 0)
			 {
				 TimeBrakeDecc = TimeStop *  0.001;
			 }
			 else
			 {
				 TimeBrakeDecc = 1;
			 }

			 

			 if (TimeStart > 0)
			 {
				 Acceleration = (EndSpeed - BeginSpeed) / TimeMoveAcc;

			 }
			 else
			 {
				 Acceleration = 59590;
			 }
			
			 if (TimeStop > 0)
			 {

				 Deceleration = (EndSpeed - BeginSpeed) / TimeBrakeDecc;

			 }
			 else
			 {
				 Deceleration = 59590 ;
			 }
			

			 if (TimeStart > 0)
			 {
				 StepCountAcc = (((TimeMoveAcc* TimeMoveAcc) *Acceleration) / 2) + (BeginSpeed*TimeMoveAcc);
			 }
			 else
			 {
				 StepCountAcc = 0;
			 }
			

			 if (TimeStop > 0)
			 {

				 StepCountBrake = (((TimeBrakeDecc* TimeBrakeDecc)  * Deceleration) / 2) + (BeginSpeed*TimeBrakeDecc);
			 }
			 else
			 {
				 StepCountBrake = 0;
			 }

			 BeginSpeed = BeginSpeed / g_CoenficinetMinSpeed;
			 EndSpeed = EndSpeed / g_CoenficinetSpeed;
			 SpeedBeginLSB_label->Text = String::Format("{0:0} [�������]", BeginSpeed & 0xFFF);
			 SpeedEndSB_label->Text = String::Format("{0:0} [�������]", EndSpeed & 0xFFFFF);

			 AccInfoLabel->Text = String::Format("{0:0}[�/c2] ,{1:0}[���]", Acceleration, StepCountAcc * StepMultiFactor);
			 BrakefoLabel->Text = String::Format("{0:0}[�/�2] ,{1:0}[���]", Deceleration, StepCountBrake * StepMultiFactor);

			 if (Acceleration > 59590 || Acceleration < 0)
			 {
				 AccInfoLabel->ForeColor = System::Drawing::Color::Red;
			 }
			 else
			 {

				 AccInfoLabel->ForeColor = System::Drawing::Color::Black;
			 }


			 if (Deceleration >  59590 || Deceleration < 0)
			 {
				 BrakefoLabel->ForeColor = System::Drawing::Color::Red;
			 }
			 else
			 {
				 BrakefoLabel->ForeColor = System::Drawing::Color::Black;
			 }

			
			 if (DriveProtocol->Create("Motion_Protocol.log"))

			 {
				 DriveProtocol->PutInputParametrs(&ControlWord, &BeginSpeed, &EndSpeed, &TimeStart, &TimeStop, &StepCount, &StepMode);
			 }

			 DF->get()->WriteSpeedProfileRun(&ControlWord, &BeginSpeed, &EndSpeed, &TimeStart, &TimeStop, &StepCount, &StepMode);
			 if (FlagStartSrive == false)
			 {
				 TimeCounter->Start();
				 FlagStartSrive = true;
			 }

		 }
	}

/*************************************************************************************************************/
   System::Void  MainView::POllingtimer_Tick(System::Object^  sender, System::EventArgs^  e) {


	   size_t DeviceCount = 0;
	   if (DF->get()->m_Comm->get()->m_FTDI != NULL || DF->get()->m_Comm->get()->m_FTDI != INVALID_HANDLE_VALUE)
	   {

		   int SpeedMotor = 0;
		   int MotorPositition = 0;
		   int StateWord = 0;

		   BYTE SW[4];
		   BYTE HW[4];
		   BYTE Name[4];

		   if (DF->get()->ReadVersion(Name, HW, SW) == 0)
		   {
			   if (!strcmp((const char *)Name, "DF"))
			   {
				   VersionSW_Label->Text = SW[0] + "." + SW[1];
				   VersionHW_Label->Text = HW[0] + "." + HW[1];
				   ModuleConectionStae_Label->BackColor = System::Drawing::Color::Green;
				   
			   }
			   else
			   {
				   VersionSW_Label->Text = 0 + "." + 0;
				   VersionHW_Label->Text = 0 + "." + 0;
				   ModuleConectionStae_Label->BackColor = System::Drawing::Color::Red;
				   ShowStatusKabels();
			   }

			   if (DF->get()->ReadStateDrive(&StateWord, &MotorPositition, &SpeedMotor) == 0)
			   {

				   float Speed = SpeedMotor * g_CoenficinetSpeed;
				   MotorSpeed_toolStripStatusLabel->Text = String::Format("{0:0.0} [�/�]", Speed);
				   int DriverState = StateWord & 0xFFFF;
				   int SenseState = (StateWord >> 16) & 0x1F;
				   DriverStateWord_Label->Text = String::Format("0x{0:X}", DriverState);
				   MOduleStateWord_Label->Text = String::Format("0x{0:X}", SenseState);
				   DecodeStatusWord(&StateWord);
				   DriveProtocol->PutInfo(&StateWord, &Speed, &MotorPositition);
				   
				  static double TimeMotion;

				   if (FlagStartSrive)
				   {
					   int MotionState = (DriverState >> 5) & 0x3;
					   TimeCounter->End();
					   TimeMotion = TimeCounter->GetEllapsedSeconds();
					   if (MotionState == 0)
					   {
						   FlagStartSrive = false;
						   DriveProtocol->Destroy();
					   }


				   }
				 
				   MotorSpeed_toolStripStatusLabel->ForeColor = System::Drawing::Color::Black;
				   MotorSpeed_toolStripStatusLabel->Text = String::Format("{0:0.0} [�/�]  {1:0.0}[���]  {2:0.000}[�] ", Speed, MotorPositition, TimeMotion);

			   }
		   }
		   else
		   {

			   InitLabels();
			   HideStatusKabels();
			   VersionSW_Label->Text = 0 + "." + 0;
			   VersionHW_Label->Text = 0 + "." + 0;
			   ModuleConectionStae_Label->BackColor = System::Drawing::Color::Red;
			   MotorSpeed_toolStripStatusLabel->ForeColor = System::Drawing::Color::Red;
			   MotorSpeed_toolStripStatusLabel->Text = "������� ������ ������!";
		   }

	   }
	  
}
 /*************************************************************************************************************/
 System::Void MainView::button2_Click(System::Object^  sender, System::EventArgs^  e) {
   DF->get()->SoftStopMotor();
}
 /*************************************************************************************************************/
 System::Void  MainView::button3_Click(System::Object^  sender, System::EventArgs^  e) {

		 int ControlWord = 0;
		 int BeginSpeed;
		 int EndSpeed;
		 int TimeStart;
		 int TimeStop;
		 int StepCount;
		 int StepMode;
		 bool Err = 0;
		 if (DF->get()->ReadSpeedProfileRun(&ControlWord, &BeginSpeed, &EndSpeed, &TimeStart, &TimeStop, &StepCount, &StepMode) != 0)
		 {
			 Err = 1;
		 }

		 DecodeModuleParametr(Err, &ControlWord, &BeginSpeed, &EndSpeed, &TimeStart, &TimeStop, &StepMode, &StepCount);
 }


 void MainView::DeivceNotificator(int Messsage)
 {

	 switch (Messsage)
	 {
		case DBT_DEVICEREMOVECOMPLETE:

		{
			InitLabels();
			FTDIConectionState_label->BackColor = System::Drawing::Color::Red;
			FTDI->get()->Close();
			
			 break;

		}

		case DBT_DEVICEARRIVAL:

		{
			 FTDIConectionState_label->BackColor = System::Drawing::Color::Green;
			 FTDI->get()->AutoConnectForFreeModule(nSpeed, nDataBits, nParity, nStopBits);
			 DF->get()->Init(FTDI);
			 break;

		}
		default:
			 break;
		}
 }


 System::Void DF_EnginerPult::MainView::WndProc(Message % m)
 {

	 switch (m.Msg)
	 {

		case WM_DEVICECHANGE:
		{
			 DeivceNotificator(m.WParam.ToInt32());
			 break;
		}

	 }

	 Form::WndProc(m);
 }
 /*************************************************************************************************************/