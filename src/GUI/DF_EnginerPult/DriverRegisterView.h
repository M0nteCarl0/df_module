#pragma once
#include "CheccForm.h"
#include "ConfigurationFile.h"
#include "DF1_Module.h"
#include "SteperConversion.h"
#include <msclr\marshal_cppstd.h>
#include <string.h>
namespace DF_EnginerPult {

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace msclr;

/// <summary>
/// ������ ��� DriverRegisterView
/// </summary>
public
ref class DriverRegisterView : public System::Windows::Forms::Form {
public:
  SPIN_RegsStruct_TypeDef *RegisterMap;
  CheccForm ^ ValiadteForm;
  std::shared_ptr<DF1_Module> *Module;
  std::shared_ptr<ConfigurationFile> *Config;

private:
  System::Windows::Forms::Label ^ ABS_POS_label;

private:
  System::Windows::Forms::Label ^ EL_POS__label;

private:
  System::Windows::Forms::Label ^ MARK_label;

private:
  System::Windows::Forms::Label ^ ACC_label;

private:
  System::Windows::Forms::Label ^ DEC_label;

private:
  System::Windows::Forms::Label ^ MAX_SPEED_label;

private:
  System::Windows::Forms::Label ^ MIN_SPEED_label;

private:
  System::Windows::Forms::Label ^ FSSPEED_label;

private:
  System::Windows::Forms::Label ^ TVAL_HOLD_label;

private:
  System::Windows::Forms::Label ^ TVAL_RUN_label;

private:
  System::Windows::Forms::Label ^ TVAL_ACC_label;

private:
  System::Windows::Forms::Label ^ TVAL_DEC__label;

private:
  System::Windows::Forms::Label ^ TFAST_label;

private:
  System::Windows::Forms::Label ^ TON_MIN_label;

private:
  System::Windows::Forms::Label ^ TOFF_MIN_label;

private:
  System::Windows::Forms::Label ^ OCD_TH_label;

private:
  System::Windows::Forms::Label ^ STEP_MODE_label;

private:
  System::Windows::Forms::Label ^ ALARM_END_label4;

private:
  System::Windows::Forms::Label ^ GATECFG1_label4;

private:
  System::Windows::Forms::Label ^ GAYRCFG2_label;

private:
  System::Windows::Forms::Label ^ CONFIG_label;

public:
public:
public:
private:
  System::Windows::Forms::Panel ^ panel1;

public:
private:
  System::Windows::Forms::Panel ^ panel2;

private:
  System::Windows::Forms::StatusStrip ^ statusStrip1;

private:
  System::Windows::Forms::TableLayoutPanel ^ tableLayoutPanel2;

private:
  System::Windows::Forms::TableLayoutPanel ^ tableLayoutPanel1;

private:
  System::Windows::Forms::ToolStripStatusLabel ^ toolStripStatusLabel_Info;

private:
  System::Windows::Forms::Label ^ label4;

private:
  System::Windows::Forms::Label ^ InversInputState_Label;

private:
  System::Windows::Forms::Timer ^ timer_Control;

public:
  void SetTimeerHandle(System::Windows::Forms::Timer ^ timer) {

    timer_Control = timer;
  }

  DriverRegisterView(void) {
    InitializeComponent();
    RegisterMap = new SPIN_RegsStruct_TypeDef();
    Module = new std::shared_ptr<DF1_Module>(new DF1_Module());
    Config = new std::shared_ptr<ConfigurationFile>(new ConfigurationFile());
    openFileDialog1->Filter = "ini ����� (*.ini)|*.ini";
    saveFileDialog1->Filter = "ini ����� (*.ini)|*.ini";
    saveFileDialog1->FileName = "";
    toolStripStatusLabel_Info->Text = "";
    ValiadteForm = gcnew CheccForm();
  }

  void Init(std::shared_ptr<DF1_Module> *_Module) {
    Module = _Module;
    if (Module->get()->ReadRegistersMotor(RegisterMap) == 0) {
      UpdateContentForm(RegisterMap);
    }
  }

protected:
  /// <summary>
  /// ���������� ��� ������������ �������.
  /// </summary>
  ~DriverRegisterView() {

    if (RegisterMap) {

      delete RegisterMap;
    }

    if (components) {
      delete components;
    }
  }

private:
  System::Windows::Forms::Button ^ button1;

protected:
private:
  System::Windows::Forms::Button ^ button2;

private:
  System::Windows::Forms::Button ^ button3;

private:
  System::Windows::Forms::Button ^ button4;

private:
  System::Windows::Forms::Label ^ label1;

private:
  System::Windows::Forms::Label ^ label2;

private:
  System::Windows::Forms::Label ^ label3;

private:
  System::Windows::Forms::Label ^ label5;

private:
  System::Windows::Forms::Label ^ label6;

private:
  System::Windows::Forms::Label ^ label7;

private:
  System::Windows::Forms::Label ^ label8;

private:
  System::Windows::Forms::Label ^ label9;

private:
  System::Windows::Forms::Label ^ label10;

private:
  System::Windows::Forms::Label ^ label11;

private:
  System::Windows::Forms::Label ^ label12;

private:
  System::Windows::Forms::Label ^ label13;

private:
  System::Windows::Forms::Label ^ label14;

private:
  System::Windows::Forms::Label ^ label15;

private:
  System::Windows::Forms::Label ^ label16;

private:
  System::Windows::Forms::Label ^ label17;

private:
  System::Windows::Forms::Label ^ label18;

private:
  System::Windows::Forms::Label ^ label19;

private:
  System::Windows::Forms::Label ^ label20;

private:
  System::Windows::Forms::Label ^ label21;

private:
  System::Windows::Forms::Label ^ label22;

private:
  System::Windows::Forms::OpenFileDialog ^ openFileDialog1;

private:
  System::Windows::Forms::SaveFileDialog ^ saveFileDialog1;

private:
  System::ComponentModel::IContainer ^ components;

  /// <summary>
  /// ��������� ���������� ������������.
  /// </summary>

protected:
  void InitializeComponent(void) {
    this->components = (gcnew System::ComponentModel::Container());
    this->button1 = (gcnew System::Windows::Forms::Button());
    this->button2 = (gcnew System::Windows::Forms::Button());
    this->button3 = (gcnew System::Windows::Forms::Button());
    this->button4 = (gcnew System::Windows::Forms::Button());
    this->label1 = (gcnew System::Windows::Forms::Label());
    this->label2 = (gcnew System::Windows::Forms::Label());
    this->label3 = (gcnew System::Windows::Forms::Label());
    this->label5 = (gcnew System::Windows::Forms::Label());
    this->label6 = (gcnew System::Windows::Forms::Label());
    this->label7 = (gcnew System::Windows::Forms::Label());
    this->label8 = (gcnew System::Windows::Forms::Label());
    this->label9 = (gcnew System::Windows::Forms::Label());
    this->label10 = (gcnew System::Windows::Forms::Label());
    this->label11 = (gcnew System::Windows::Forms::Label());
    this->label12 = (gcnew System::Windows::Forms::Label());
    this->label13 = (gcnew System::Windows::Forms::Label());
    this->label14 = (gcnew System::Windows::Forms::Label());
    this->label15 = (gcnew System::Windows::Forms::Label());
    this->label16 = (gcnew System::Windows::Forms::Label());
    this->label17 = (gcnew System::Windows::Forms::Label());
    this->label18 = (gcnew System::Windows::Forms::Label());
    this->label19 = (gcnew System::Windows::Forms::Label());
    this->label20 = (gcnew System::Windows::Forms::Label());
    this->label21 = (gcnew System::Windows::Forms::Label());
    this->label22 = (gcnew System::Windows::Forms::Label());
    this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
    this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
    this->ABS_POS_label = (gcnew System::Windows::Forms::Label());
    this->EL_POS__label = (gcnew System::Windows::Forms::Label());
    this->MARK_label = (gcnew System::Windows::Forms::Label());
    this->ACC_label = (gcnew System::Windows::Forms::Label());
    this->DEC_label = (gcnew System::Windows::Forms::Label());
    this->MAX_SPEED_label = (gcnew System::Windows::Forms::Label());
    this->MIN_SPEED_label = (gcnew System::Windows::Forms::Label());
    this->FSSPEED_label = (gcnew System::Windows::Forms::Label());
    this->TVAL_HOLD_label = (gcnew System::Windows::Forms::Label());
    this->TVAL_RUN_label = (gcnew System::Windows::Forms::Label());
    this->TVAL_ACC_label = (gcnew System::Windows::Forms::Label());
    this->TVAL_DEC__label = (gcnew System::Windows::Forms::Label());
    this->TFAST_label = (gcnew System::Windows::Forms::Label());
    this->TON_MIN_label = (gcnew System::Windows::Forms::Label());
    this->TOFF_MIN_label = (gcnew System::Windows::Forms::Label());
    this->OCD_TH_label = (gcnew System::Windows::Forms::Label());
    this->STEP_MODE_label = (gcnew System::Windows::Forms::Label());
    this->ALARM_END_label4 = (gcnew System::Windows::Forms::Label());
    this->GATECFG1_label4 = (gcnew System::Windows::Forms::Label());
    this->GAYRCFG2_label = (gcnew System::Windows::Forms::Label());
    this->CONFIG_label = (gcnew System::Windows::Forms::Label());
    this->panel1 = (gcnew System::Windows::Forms::Panel());
    this->tableLayoutPanel2 =
        (gcnew System::Windows::Forms::TableLayoutPanel());
    this->label4 = (gcnew System::Windows::Forms::Label());
    this->InversInputState_Label = (gcnew System::Windows::Forms::Label());
    this->panel2 = (gcnew System::Windows::Forms::Panel());
    this->tableLayoutPanel1 =
        (gcnew System::Windows::Forms::TableLayoutPanel());
    this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
    this->toolStripStatusLabel_Info =
        (gcnew System::Windows::Forms::ToolStripStatusLabel());
    this->timer_Control =
        (gcnew System::Windows::Forms::Timer(this->components));
    this->panel1->SuspendLayout();
    this->tableLayoutPanel2->SuspendLayout();
    this->panel2->SuspendLayout();
    this->tableLayoutPanel1->SuspendLayout();
    this->statusStrip1->SuspendLayout();
    this->SuspendLayout();
    //
    // button1
    //
    this->button1->Location = System::Drawing::Point(460, 269);
    this->button1->Name = L"button1";
    this->button1->Size = System::Drawing::Size(75, 23);
    this->button1->TabIndex = 0;
    this->button1->Text = L"�������� ";
    this->button1->UseVisualStyleBackColor = true;
    this->button1->Click +=
        gcnew System::EventHandler(this, &DriverRegisterView::button1_Click);
    //
    // button2
    //
    this->button2->Location = System::Drawing::Point(7, 269);
    this->button2->Name = L"button2";
    this->button2->Size = System::Drawing::Size(75, 23);
    this->button2->TabIndex = 1;
    this->button2->Text = L"���������";
    this->button2->UseVisualStyleBackColor = true;
    this->button2->Click +=
        gcnew System::EventHandler(this, &DriverRegisterView::button2_Click);
    //
    // button3
    //
    this->button3->Location = System::Drawing::Point(89, 269);
    this->button3->Name = L"button3";
    this->button3->Size = System::Drawing::Size(180, 23);
    this->button3->TabIndex = 2;
    this->button3->Text = L"������� ���� ������������";
    this->button3->UseVisualStyleBackColor = true;
    this->button3->Click +=
        gcnew System::EventHandler(this, &DriverRegisterView::button3_Click);
    //
    // button4
    //
    this->button4->Location = System::Drawing::Point(275, 269);
    this->button4->Name = L"button4";
    this->button4->Size = System::Drawing::Size(180, 23);
    this->button4->TabIndex = 3;
    this->button4->Text = L"��������� �������� �  ���� ������������";
    this->button4->UseVisualStyleBackColor = true;
    this->button4->Click +=
        gcnew System::EventHandler(this, &DriverRegisterView::button4_Click);
    //
    // label1
    //
    this->label1->AutoSize = true;
    this->label1->Location = System::Drawing::Point(3, 0);
    this->label1->Name = L"label1";
    this->label1->Size = System::Drawing::Size(68, 13);
    this->label1->TabIndex = 4;
    this->label1->Text = L"ABS Position";
    //
    // label2
    //
    this->label2->AutoSize = true;
    this->label2->Location = System::Drawing::Point(3, 20);
    this->label2->Name = L"label2";
    this->label2->Size = System::Drawing::Size(60, 13);
    this->label2->TabIndex = 5;
    this->label2->Text = L"EL Position";
    //
    // label3
    //
    this->label3->AutoSize = true;
    this->label3->Location = System::Drawing::Point(3, 40);
    this->label3->Name = L"label3";
    this->label3->Size = System::Drawing::Size(38, 13);
    this->label3->TabIndex = 6;
    this->label3->Text = L"MARK";
    //
    // label5
    //
    this->label5->AutoSize = true;
    this->label5->Location = System::Drawing::Point(3, 60);
    this->label5->Name = L"label5";
    this->label5->Size = System::Drawing::Size(28, 13);
    this->label5->TabIndex = 8;
    this->label5->Text = L"ACC";
    //
    // label6
    //
    this->label6->AutoSize = true;
    this->label6->Location = System::Drawing::Point(3, 80);
    this->label6->Name = L"label6";
    this->label6->Size = System::Drawing::Size(29, 13);
    this->label6->TabIndex = 9;
    this->label6->Text = L"DEC";
    //
    // label7
    //
    this->label7->AutoSize = true;
    this->label7->Location = System::Drawing::Point(3, 100);
    this->label7->Name = L"label7";
    this->label7->Size = System::Drawing::Size(72, 13);
    this->label7->TabIndex = 10;
    this->label7->Text = L"MAX_SPEED";
    //
    // label8
    //
    this->label8->AutoSize = true;
    this->label8->Location = System::Drawing::Point(3, 120);
    this->label8->Name = L"label8";
    this->label8->Size = System::Drawing::Size(69, 13);
    this->label8->TabIndex = 11;
    this->label8->Text = L"MIN_SPEED";
    //
    // label9
    //
    this->label9->AutoSize = true;
    this->label9->Location = System::Drawing::Point(3, 140);
    this->label9->Name = L"label9";
    this->label9->Size = System::Drawing::Size(62, 13);
    this->label9->TabIndex = 12;
    this->label9->Text = L"FS_SPEED";
    //
    // label10
    //
    this->label10->AutoSize = true;
    this->label10->Location = System::Drawing::Point(3, 160);
    this->label10->Name = L"label10";
    this->label10->Size = System::Drawing::Size(70, 13);
    this->label10->TabIndex = 13;
    this->label10->Text = L"TVAL_HOLD";
    //
    // label11
    //
    this->label11->AutoSize = true;
    this->label11->Location = System::Drawing::Point(3, 180);
    this->label11->Name = L"label11";
    this->label11->Size = System::Drawing::Size(64, 13);
    this->label11->TabIndex = 14;
    this->label11->Text = L"TVAL_RUN";
    //
    // label12
    //
    this->label12->AutoSize = true;
    this->label12->Location = System::Drawing::Point(3, 200);
    this->label12->Name = L"label12";
    this->label12->Size = System::Drawing::Size(61, 13);
    this->label12->TabIndex = 15;
    this->label12->Text = L"TVAL_ACC";
    //
    // label13
    //
    this->label13->AutoSize = true;
    this->label13->Location = System::Drawing::Point(3, 0);
    this->label13->Name = L"label13";
    this->label13->Size = System::Drawing::Size(62, 13);
    this->label13->TabIndex = 16;
    this->label13->Text = L"TVAL_DEC";
    //
    // label14
    //
    this->label14->AutoSize = true;
    this->label14->Location = System::Drawing::Point(3, 20);
    this->label14->Name = L"label14";
    this->label14->Size = System::Drawing::Size(47, 13);
    this->label14->TabIndex = 17;
    this->label14->Text = L"T_FAST";
    //
    // label15
    //
    this->label15->AutoSize = true;
    this->label15->Location = System::Drawing::Point(3, 40);
    this->label15->Name = L"label15";
    this->label15->Size = System::Drawing::Size(56, 13);
    this->label15->TabIndex = 18;
    this->label15->Text = L"TON_MIN";
    //
    // label16
    //
    this->label16->AutoSize = true;
    this->label16->Location = System::Drawing::Point(3, 60);
    this->label16->Name = L"label16";
    this->label16->Size = System::Drawing::Size(60, 13);
    this->label16->TabIndex = 19;
    this->label16->Text = L"TOFF_MIN";
    //
    // label17
    //
    this->label17->AutoSize = true;
    this->label17->Location = System::Drawing::Point(3, 80);
    this->label17->Name = L"label17";
    this->label17->Size = System::Drawing::Size(51, 13);
    this->label17->TabIndex = 20;
    this->label17->Text = L"OCD_TH";
    //
    // label18
    //
    this->label18->AutoSize = true;
    this->label18->Location = System::Drawing::Point(3, 100);
    this->label18->Name = L"label18";
    this->label18->Size = System::Drawing::Size(73, 13);
    this->label18->TabIndex = 21;
    this->label18->Text = L"STEP_MODE";
    //
    // label19
    //
    this->label19->AutoSize = true;
    this->label19->Location = System::Drawing::Point(3, 120);
    this->label19->Name = L"label19";
    this->label19->Size = System::Drawing::Size(65, 13);
    this->label19->TabIndex = 22;
    this->label19->Text = L"ALARM_EN";
    //
    // label20
    //
    this->label20->AutoSize = true;
    this->label20->Location = System::Drawing::Point(3, 140);
    this->label20->Name = L"label20";
    this->label20->Size = System::Drawing::Size(63, 13);
    this->label20->TabIndex = 23;
    this->label20->Text = L"GATECFG1";
    //
    // label21
    //
    this->label21->AutoSize = true;
    this->label21->Location = System::Drawing::Point(3, 160);
    this->label21->Name = L"label21";
    this->label21->Size = System::Drawing::Size(63, 13);
    this->label21->TabIndex = 24;
    this->label21->Text = L"GATECFG2";
    //
    // label22
    //
    this->label22->AutoSize = true;
    this->label22->Location = System::Drawing::Point(3, 180);
    this->label22->Name = L"label22";
    this->label22->Size = System::Drawing::Size(47, 13);
    this->label22->TabIndex = 25;
    this->label22->Text = L"CONFIG";
    //
    // openFileDialog1
    //
    this->openFileDialog1->FileName = L"openFileDialog1";
    //
    // ABS_POS_label
    //
    this->ABS_POS_label->Location = System::Drawing::Point(132, 0);
    this->ABS_POS_label->Name = L"ABS_POS_label";
    this->ABS_POS_label->Size = System::Drawing::Size(31, 13);
    this->ABS_POS_label->TabIndex = 26;
    //
    // EL_POS__label
    //
    this->EL_POS__label->Location = System::Drawing::Point(132, 20);
    this->EL_POS__label->Name = L"EL_POS__label";
    this->EL_POS__label->Size = System::Drawing::Size(41, 13);
    this->EL_POS__label->TabIndex = 27;
    //
    // MARK_label
    //
    this->MARK_label->Location = System::Drawing::Point(132, 40);
    this->MARK_label->Name = L"MARK_label";
    this->MARK_label->Size = System::Drawing::Size(41, 13);
    this->MARK_label->TabIndex = 28;
    //
    // ACC_label
    //
    this->ACC_label->Location = System::Drawing::Point(132, 60);
    this->ACC_label->Name = L"ACC_label";
    this->ACC_label->Size = System::Drawing::Size(41, 13);
    this->ACC_label->TabIndex = 30;
    //
    // DEC_label
    //
    this->DEC_label->Location = System::Drawing::Point(132, 80);
    this->DEC_label->Name = L"DEC_label";
    this->DEC_label->Size = System::Drawing::Size(41, 13);
    this->DEC_label->TabIndex = 31;
    //
    // MAX_SPEED_label
    //
    this->MAX_SPEED_label->Location = System::Drawing::Point(132, 100);
    this->MAX_SPEED_label->Name = L"MAX_SPEED_label";
    this->MAX_SPEED_label->Size = System::Drawing::Size(41, 13);
    this->MAX_SPEED_label->TabIndex = 32;
    //
    // MIN_SPEED_label
    //
    this->MIN_SPEED_label->Location = System::Drawing::Point(132, 120);
    this->MIN_SPEED_label->Name = L"MIN_SPEED_label";
    this->MIN_SPEED_label->Size = System::Drawing::Size(41, 13);
    this->MIN_SPEED_label->TabIndex = 33;
    //
    // FSSPEED_label
    //
    this->FSSPEED_label->Location = System::Drawing::Point(132, 140);
    this->FSSPEED_label->Name = L"FSSPEED_label";
    this->FSSPEED_label->Size = System::Drawing::Size(41, 13);
    this->FSSPEED_label->TabIndex = 34;
    //
    // TVAL_HOLD_label
    //
    this->TVAL_HOLD_label->Location = System::Drawing::Point(132, 160);
    this->TVAL_HOLD_label->Name = L"TVAL_HOLD_label";
    this->TVAL_HOLD_label->Size = System::Drawing::Size(41, 13);
    this->TVAL_HOLD_label->TabIndex = 35;
    //
    // TVAL_RUN_label
    //
    this->TVAL_RUN_label->Location = System::Drawing::Point(132, 180);
    this->TVAL_RUN_label->Name = L"TVAL_RUN_label";
    this->TVAL_RUN_label->Size = System::Drawing::Size(41, 13);
    this->TVAL_RUN_label->TabIndex = 36;
    //
    // TVAL_ACC_label
    //
    this->TVAL_ACC_label->Location = System::Drawing::Point(132, 200);
    this->TVAL_ACC_label->Name = L"TVAL_ACC_label";
    this->TVAL_ACC_label->Size = System::Drawing::Size(41, 13);
    this->TVAL_ACC_label->TabIndex = 37;
    //
    // TVAL_DEC__label
    //
    this->TVAL_DEC__label->Location = System::Drawing::Point(133, 0);
    this->TVAL_DEC__label->Name = L"TVAL_DEC__label";
    this->TVAL_DEC__label->Size = System::Drawing::Size(41, 13);
    this->TVAL_DEC__label->TabIndex = 38;
    //
    // TFAST_label
    //
    this->TFAST_label->Location = System::Drawing::Point(133, 20);
    this->TFAST_label->Name = L"TFAST_label";
    this->TFAST_label->Size = System::Drawing::Size(41, 13);
    this->TFAST_label->TabIndex = 39;
    //
    // TON_MIN_label
    //
    this->TON_MIN_label->Location = System::Drawing::Point(133, 40);
    this->TON_MIN_label->Name = L"TON_MIN_label";
    this->TON_MIN_label->Size = System::Drawing::Size(41, 13);
    this->TON_MIN_label->TabIndex = 40;
    //
    // TOFF_MIN_label
    //
    this->TOFF_MIN_label->Location = System::Drawing::Point(133, 60);
    this->TOFF_MIN_label->Name = L"TOFF_MIN_label";
    this->TOFF_MIN_label->Size = System::Drawing::Size(41, 13);
    this->TOFF_MIN_label->TabIndex = 41;
    //
    // OCD_TH_label
    //
    this->OCD_TH_label->AutoSize = true;
    this->OCD_TH_label->Location = System::Drawing::Point(133, 80);
    this->OCD_TH_label->Name = L"OCD_TH_label";
    this->OCD_TH_label->Size = System::Drawing::Size(0, 13);
    this->OCD_TH_label->TabIndex = 42;
    //
    // STEP_MODE_label
    //
    this->STEP_MODE_label->Location = System::Drawing::Point(133, 100);
    this->STEP_MODE_label->Name = L"STEP_MODE_label";
    this->STEP_MODE_label->Size = System::Drawing::Size(41, 13);
    this->STEP_MODE_label->TabIndex = 43;
    //
    // ALARM_END_label4
    //
    this->ALARM_END_label4->Location = System::Drawing::Point(133, 120);
    this->ALARM_END_label4->Name = L"ALARM_END_label4";
    this->ALARM_END_label4->Size = System::Drawing::Size(41, 13);
    this->ALARM_END_label4->TabIndex = 44;
    //
    // GATECFG1_label4
    //
    this->GATECFG1_label4->Location = System::Drawing::Point(133, 140);
    this->GATECFG1_label4->Name = L"GATECFG1_label4";
    this->GATECFG1_label4->Size = System::Drawing::Size(41, 13);
    this->GATECFG1_label4->TabIndex = 45;
    //
    // GAYRCFG2_label
    //
    this->GAYRCFG2_label->Location = System::Drawing::Point(133, 160);
    this->GAYRCFG2_label->Name = L"GAYRCFG2_label";
    this->GAYRCFG2_label->Size = System::Drawing::Size(41, 13);
    this->GAYRCFG2_label->TabIndex = 46;
    //
    // CONFIG_label
    //
    this->CONFIG_label->Location = System::Drawing::Point(133, 180);
    this->CONFIG_label->Name = L"CONFIG_label";
    this->CONFIG_label->Size = System::Drawing::Size(45, 13);
    this->CONFIG_label->TabIndex = 47;
    //
    // panel1
    //
    this->panel1->BorderStyle =
        System::Windows::Forms::BorderStyle::FixedSingle;
    this->panel1->Controls->Add(this->tableLayoutPanel2);
    this->panel1->Location = System::Drawing::Point(262, 12);
    this->panel1->Name = L"panel1";
    this->panel1->Size = System::Drawing::Size(273, 251);
    this->panel1->TabIndex = 48;
    //
    // tableLayoutPanel2
    //
    this->tableLayoutPanel2->ColumnCount = 2;
    this->tableLayoutPanel2->ColumnStyles->Add(
        (gcnew System::Windows::Forms::ColumnStyle(
            System::Windows::Forms::SizeType::Percent, 50)));
    this->tableLayoutPanel2->ColumnStyles->Add(
        (gcnew System::Windows::Forms::ColumnStyle(
            System::Windows::Forms::SizeType::Percent, 50)));
    this->tableLayoutPanel2->Controls->Add(this->label4, 0, 10);
    this->tableLayoutPanel2->Controls->Add(this->CONFIG_label, 1, 9);
    this->tableLayoutPanel2->Controls->Add(this->label13, 0, 0);
    this->tableLayoutPanel2->Controls->Add(this->label22, 0, 9);
    this->tableLayoutPanel2->Controls->Add(this->GAYRCFG2_label, 1, 8);
    this->tableLayoutPanel2->Controls->Add(this->label14, 0, 1);
    this->tableLayoutPanel2->Controls->Add(this->label21, 0, 8);
    this->tableLayoutPanel2->Controls->Add(this->GATECFG1_label4, 1, 7);
    this->tableLayoutPanel2->Controls->Add(this->label15, 0, 2);
    this->tableLayoutPanel2->Controls->Add(this->label20, 0, 7);
    this->tableLayoutPanel2->Controls->Add(this->label16, 0, 3);
    this->tableLayoutPanel2->Controls->Add(this->label19, 0, 6);
    this->tableLayoutPanel2->Controls->Add(this->STEP_MODE_label, 1, 5);
    this->tableLayoutPanel2->Controls->Add(this->label17, 0, 4);
    this->tableLayoutPanel2->Controls->Add(this->label18, 0, 5);
    this->tableLayoutPanel2->Controls->Add(this->TVAL_DEC__label, 1, 0);
    this->tableLayoutPanel2->Controls->Add(this->OCD_TH_label, 1, 4);
    this->tableLayoutPanel2->Controls->Add(this->TFAST_label, 1, 1);
    this->tableLayoutPanel2->Controls->Add(this->TON_MIN_label, 1, 2);
    this->tableLayoutPanel2->Controls->Add(this->TOFF_MIN_label, 1, 3);
    this->tableLayoutPanel2->Controls->Add(this->ALARM_END_label4, 1, 6);
    this->tableLayoutPanel2->Controls->Add(this->InversInputState_Label, 1, 10);
    this->tableLayoutPanel2->Location = System::Drawing::Point(13, 17);
    this->tableLayoutPanel2->Name = L"tableLayoutPanel2";
    this->tableLayoutPanel2->RowCount = 11;
    this->tableLayoutPanel2->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel2->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel2->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel2->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel2->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel2->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel2->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel2->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel2->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel2->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel2->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel2->Size = System::Drawing::Size(260, 222);
    this->tableLayoutPanel2->TabIndex = 51;
    //
    // label4
    //
    this->label4->AutoSize = true;
    this->label4->Location = System::Drawing::Point(3, 200);
    this->label4->Name = L"label4";
    this->label4->Size = System::Drawing::Size(79, 13);
    this->label4->TabIndex = 49;
    this->label4->Text = L"InvertedSensor";
    //
    // InversInputState_Label
    //
    this->InversInputState_Label->Location = System::Drawing::Point(133, 200);
    this->InversInputState_Label->Name = L"InversInputState_Label";
    this->InversInputState_Label->Size = System::Drawing::Size(45, 13);
    this->InversInputState_Label->TabIndex = 48;
    //
    // panel2
    //
    this->panel2->BorderStyle =
        System::Windows::Forms::BorderStyle::FixedSingle;
    this->panel2->Controls->Add(this->tableLayoutPanel1);
    this->panel2->Location = System::Drawing::Point(7, 12);
    this->panel2->Name = L"panel2";
    this->panel2->Size = System::Drawing::Size(262, 251);
    this->panel2->TabIndex = 49;
    //
    // tableLayoutPanel1
    //
    this->tableLayoutPanel1->ColumnCount = 2;
    this->tableLayoutPanel1->ColumnStyles->Add(
        (gcnew System::Windows::Forms::ColumnStyle(
            System::Windows::Forms::SizeType::Percent, 50)));
    this->tableLayoutPanel1->ColumnStyles->Add(
        (gcnew System::Windows::Forms::ColumnStyle(
            System::Windows::Forms::SizeType::Percent, 50)));
    this->tableLayoutPanel1->Controls->Add(this->TVAL_ACC_label, 1, 10);
    this->tableLayoutPanel1->Controls->Add(this->label2, 0, 1);
    this->tableLayoutPanel1->Controls->Add(this->label12, 0, 10);
    this->tableLayoutPanel1->Controls->Add(this->TVAL_RUN_label, 1, 9);
    this->tableLayoutPanel1->Controls->Add(this->label3, 0, 2);
    this->tableLayoutPanel1->Controls->Add(this->label11, 0, 9);
    this->tableLayoutPanel1->Controls->Add(this->TVAL_HOLD_label, 1, 8);
    this->tableLayoutPanel1->Controls->Add(this->label5, 0, 3);
    this->tableLayoutPanel1->Controls->Add(this->label10, 0, 8);
    this->tableLayoutPanel1->Controls->Add(this->FSSPEED_label, 1, 7);
    this->tableLayoutPanel1->Controls->Add(this->label6, 0, 4);
    this->tableLayoutPanel1->Controls->Add(this->label9, 0, 7);
    this->tableLayoutPanel1->Controls->Add(this->MIN_SPEED_label, 1, 6);
    this->tableLayoutPanel1->Controls->Add(this->label1, 0, 0);
    this->tableLayoutPanel1->Controls->Add(this->label8, 0, 6);
    this->tableLayoutPanel1->Controls->Add(this->ABS_POS_label, 1, 0);
    this->tableLayoutPanel1->Controls->Add(this->MAX_SPEED_label, 1, 5);
    this->tableLayoutPanel1->Controls->Add(this->EL_POS__label, 1, 1);
    this->tableLayoutPanel1->Controls->Add(this->MARK_label, 1, 2);
    this->tableLayoutPanel1->Controls->Add(this->ACC_label, 1, 3);
    this->tableLayoutPanel1->Controls->Add(this->DEC_label, 1, 4);
    this->tableLayoutPanel1->Controls->Add(this->label7, 0, 5);
    this->tableLayoutPanel1->Location = System::Drawing::Point(0, 17);
    this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
    this->tableLayoutPanel1->RowCount = 11;
    this->tableLayoutPanel1->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel1->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel1->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel1->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel1->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel1->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel1->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel1->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel1->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel1->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel1->RowStyles->Add(
        (gcnew System::Windows::Forms::RowStyle(
            System::Windows::Forms::SizeType::Absolute, 20)));
    this->tableLayoutPanel1->Size = System::Drawing::Size(259, 222);
    this->tableLayoutPanel1->TabIndex = 51;
    //
    // statusStrip1
    //
    this->statusStrip1->Items->AddRange(
        gcnew cli::array<System::Windows::Forms::ToolStripItem ^>(1){
            this->toolStripStatusLabel_Info});
    this->statusStrip1->Location = System::Drawing::Point(0, 300);
    this->statusStrip1->Name = L"statusStrip1";
    this->statusStrip1->Size = System::Drawing::Size(549, 22);
    this->statusStrip1->TabIndex = 50;
    this->statusStrip1->Text = L"statusStrip1";
    //
    // toolStripStatusLabel_Info
    //
    this->toolStripStatusLabel_Info->Name = L"toolStripStatusLabel_Info";
    this->toolStripStatusLabel_Info->Size = System::Drawing::Size(118, 17);
    this->toolStripStatusLabel_Info->Text = L"toolStripStatusLabel1";
    //
    // DriverRegisterView
    //
    this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
    this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
    this->ClientSize = System::Drawing::Size(549, 322);
    this->Controls->Add(this->statusStrip1);
    this->Controls->Add(this->panel2);
    this->Controls->Add(this->panel1);
    this->Controls->Add(this->button4);
    this->Controls->Add(this->button3);
    this->Controls->Add(this->button2);
    this->Controls->Add(this->button1);
    this->FormBorderStyle =
        System::Windows::Forms::FormBorderStyle::FixedSingle;
    this->MaximizeBox = false;
    this->MinimizeBox = false;
    this->Name = L"DriverRegisterView";
    this->Text = L"�������� �������� ��������� L6472";
    this->panel1->ResumeLayout(false);
    this->tableLayoutPanel2->ResumeLayout(false);
    this->tableLayoutPanel2->PerformLayout();
    this->panel2->ResumeLayout(false);
    this->tableLayoutPanel1->ResumeLayout(false);
    this->tableLayoutPanel1->PerformLayout();
    this->statusStrip1->ResumeLayout(false);
    this->statusStrip1->PerformLayout();
    this->ResumeLayout(false);
    this->PerformLayout();
  }

private:
  System::Void button2_Click(System::Object ^ sender, System::EventArgs ^ e) {

    if (Module->get()->ReadRegistersMotor(RegisterMap) == 0) {
      UpdateContentForm(RegisterMap);
      toolStripStatusLabel_Info->ForeColor = System::Drawing::Color::Black;
      toolStripStatusLabel_Info->Text =
          "������  ������������ ������� ���������!";
    } else {
      toolStripStatusLabel_Info->Text = "������ ������  ������������!";
      toolStripStatusLabel_Info->ForeColor = System::Drawing::Color::Red;
    }
  }

  /*****************************************************************************************/
  void UpdateContentForm(SPIN_RegsStruct_TypeDef *_RegisterMap) {

    ABS_POS_label->Text = _RegisterMap->ABS_POS.ToString();
    EL_POS__label->Text = _RegisterMap->EL_POS.ToString();
    MARK_label->Text = _RegisterMap->MARK.ToString();
    ACC_label->Text = _RegisterMap->ACC.ToString();
    DEC_label->Text = _RegisterMap->DEC.ToString();
    MAX_SPEED_label->Text = _RegisterMap->MAX_SPEED.ToString();
    MIN_SPEED_label->Text = _RegisterMap->MIN_SPEED.ToString();
    FSSPEED_label->Text = _RegisterMap->FS_SPD.ToString();
    TVAL_HOLD_label->Text = _RegisterMap->TVAL_HOLD.ToString();
    TVAL_ACC_label->Text = _RegisterMap->TVAL_ACC.ToString();
    TVAL_DEC__label->Text = _RegisterMap->TVAL_DEC.ToString();
    TVAL_RUN_label->Text = _RegisterMap->TVAL_RUN.ToString();
    TFAST_label->Text = String::Format("0x{0:x}", _RegisterMap->T_FAST);
    TON_MIN_label->Text = _RegisterMap->TON_MIN.ToString();
    TOFF_MIN_label->Text = _RegisterMap->TOFF_MIN.ToString();
    OCD_TH_label->Text = _RegisterMap->OCD_TH.ToString();
    STEP_MODE_label->Text = _RegisterMap->STEP_MODE.ToString();
    ALARM_END_label4->Text = _RegisterMap->ALARM_EN.ToString();
    GATECFG1_label4->Text = _RegisterMap->GATECFG1.ToString();
    GAYRCFG2_label->Text = _RegisterMap->GATECFG2.ToString();
    CONFIG_label->Text = String::Format("0x{0:x}", _RegisterMap->CONFIG);
    InversInputState_Label->Text = _RegisterMap->InverseSensorInput.ToString();
  }
  /*****************************************************************************************/

private:
  System::Void button1_Click(System::Object ^ sender, System::EventArgs ^ e) {

    System::Windows::Forms::DialogResult Result;
    if (ValiadteForm->IsDisposed) {
      ValiadteForm = gcnew CheccForm();
    }

    if (!ValiadteForm->Visible) {
      ValiadteForm->updateNumbers();
      Result = ValiadteForm->ShowDialog();
      if (Result == System::Windows::Forms::DialogResult::OK) {
        goto VAL;
      } else {
        toolStripStatusLabel_Info->ForeColor = System::Drawing::Color::Red;
        toolStripStatusLabel_Info->Text =
            "�������� ������������� ��� ������ �� ������� ������!";

        return;
      }
    };

  VAL:

    if (timer_Control->Enabled) {
      timer_Control->Enabled = false;
    }
    SPIN_RegsStruct_TypeDef RegisterMapR;
    SPIN_RegsStruct_TypeDef RegisterMapW;
    memset(&RegisterMapR, 0, sizeof(SPIN_RegsStruct_TypeDef));
    memset(&RegisterMapW, 0, sizeof(SPIN_RegsStruct_TypeDef));

    if (Module->get()->WriteRegistersMotor(RegisterMap) == 0) {
      Sleep(1000);
      if (Module->get()->ReadRegistersMotor(&RegisterMapR) == 0) {
        UpdateContentForm(&RegisterMapR);
        if (CompareSPIN_RegsStruct(RegisterMap, &RegisterMapR)) {
          toolStripStatusLabel_Info->ForeColor = System::Drawing::Color::Black;
          toolStripStatusLabel_Info->Text =
              "������ ����� ������������ ������� ��������!";

        } else {
          toolStripStatusLabel_Info->ForeColor = System::Drawing::Color::Red;
          toolStripStatusLabel_Info->Text = "������ ������ ����������!";
        }
      }

    } else {
      toolStripStatusLabel_Info->ForeColor = System::Drawing::Color::Red;
      toolStripStatusLabel_Info->Text = "������ ������ ����������!";
    }

    if (!timer_Control->Enabled) {
      timer_Control->Enabled = true;
    }
  }
  /*****************************************************************************************/
private:
  System::Void button4_Click(System::Object ^ sender, System::EventArgs ^ e) {

    ;
    System::Windows::Forms::DialogResult Result = saveFileDialog1->ShowDialog();
    if (Result == System::Windows::Forms::DialogResult::OK)

    {
      std::string FileName =
          interop::marshal_as<std::string>(saveFileDialog1->FileName);
      if (Module->get()->ReadRegistersMotor(RegisterMap) == 0) {

        UpdateContentForm(RegisterMap);
        Config->get()->Save(FileName.c_str(), RegisterMap);
        toolStripStatusLabel_Info->ForeColor = System::Drawing::Color::Black;
        toolStripStatusLabel_Info->Text = "���� ������� ��������!";
      } else {
        toolStripStatusLabel_Info->ForeColor = System::Drawing::Color::Red;
        toolStripStatusLabel_Info->Text = "������ ��� ������ ������ �� ������!";
      }

    } else {
      toolStripStatusLabel_Info->Text = "������ �������� �������������!";
      toolStripStatusLabel_Info->ForeColor = System::Drawing::Color::Red;
    }
  }
  /*****************************************************************************************/
private:
  System::Void button3_Click(System::Object ^ sender, System::EventArgs ^ e) {

    ;
    System::Windows::Forms::DialogResult Result = openFileDialog1->ShowDialog();
    if (Result == System::Windows::Forms::DialogResult::OK)

    {
      std::string FileName =
          interop::marshal_as<std::string>(openFileDialog1->FileName);

      if (Config->get()->Load(FileName.c_str())) {
        UpdateContentForm(&Config->get()->Registers);
        *RegisterMap = Config->get()->Registers;
        toolStripStatusLabel_Info->ForeColor = System::Drawing::Color::Black;
        toolStripStatusLabel_Info->Text = "���� ������� ������!";
      } else {
        toolStripStatusLabel_Info->Text = "������ �������� �����!";
        toolStripStatusLabel_Info->ForeColor = System::Drawing::Color::Red;
      }

    } else {
      toolStripStatusLabel_Info->Text = "������ �������� �������������!";
      toolStripStatusLabel_Info->ForeColor = System::Drawing::Color::Red;
    }
  }
};
} // namespace DF_EnginerPult
