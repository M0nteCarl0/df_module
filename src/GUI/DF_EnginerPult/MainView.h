#pragma once
#include "HiRessTimeMeasure.h"
#include "ConfigurationFile.h"
#include  "SteperDefinition.h"
#include  "DF1_Module.h"
#include  "DriverRegisterView.h"
#include  "FTDIView.h"""

#include "ConfigurationFile.h"
#include "ProgramConfigFile.h"
#include "MoveProtocol.h"
#include "HelperForm.h"
namespace DF_EnginerPult {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Form1
	/// </summary>
	public ref class MainView : public System::Windows::Forms::Form
	{
public:
		MainView(void);
protected:
		~MainView();
private: 
 void InitializeComponent(void);
 System::Void �����������������������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
 System::Void SetuoFTDIToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
 System::Void ������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
 System::Void ����������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);


 System::Windows::Forms::StatusStrip^			MainstatusStrip;
	private: System::Windows::Forms::ToolStripStatusLabel^  MotorSpeed_toolStripStatusLabel;





 System::Windows::Forms::GroupBox^				groupBox3;
 System::Windows::Forms::Button^				button1;
 System::Windows::Forms::Button^				button2;
 System::Windows::Forms::Label^				    label6;
 System::Windows::Forms::ComboBox^			    MicrosStepcomboBox;
 System::Windows::Forms::Label^					label4;

	private: System::Windows::Forms::Label^  Stepcount_label1;


 System::Windows::Forms::TextBox^				MicrosStepCount_textBox;
	private: System::Windows::Forms::Label^  TimeBrake_label1;

	private: System::Windows::Forms::Label^  TimeAcc_label;

	private: System::Windows::Forms::Label^  SpeedEnd_label;

	private: System::Windows::Forms::Label^  SpeedBegin_label;


 System::Windows::Forms::TextBox^				TimeBrake_textBox;
 System::Windows::Forms::TextBox^				TimeAcceleration_textBox;
 System::Windows::Forms::TextBox^				SpeedEnd_textBox;
 System::Windows::Forms::TextBox^				SpeedBegin_textBox;
 System::Windows::Forms::MenuStrip^				MianmenuStrip;
 System::Windows::Forms::ToolStripMenuItem^		������������������������ToolStripMenuItem;



 System::Windows::Forms::Button^				button3;
 System::Windows::Forms::Label^					label18;
 System::Windows::Forms::Label^					label17;
 System::Windows::Forms::Label^					label15;
 System::Windows::Forms::Label^					label13;
 System::Windows::Forms::Panel^					panel2;
 System::Windows::Forms::RadioButton^		    radioButton_Direction_Backward;
 System::Windows::Forms::RadioButton^		    radioButton_Direction_Forward;
 System::Windows::Forms::Label^				    label19;
 System::Windows::Forms::Panel^				    panel1;
 System::Windows::Forms::RadioButton^			ModeSensor2;
 System::Windows::Forms::RadioButton^			ModeSensor1;
 System::Windows::Forms::RadioButton^			ModeInfinite;
	private: System::Windows::Forms::Label^  SpeedEndSB_label;

	private: System::Windows::Forms::Label^  SpeedBeginLSB_label;

 System::Windows::Forms::ToolStripMenuItem^		fTDIToolStripMenuItem;
 System::Windows::Forms::ToolStripMenuItem^		SetuoFTDIToolStripMenuItem;
 System::Windows::Forms::ToolStripMenuItem^		������������ToolStripMenuItem;
 System::Windows::Forms::ToolStripMenuItem^		����������ToolStripMenuItem;
 System::Windows::Forms::Timer^					POllingtimer;
 System::Windows::Forms::GroupBox^				groupBox4;
 System::Windows::Forms::Label^				    WDT_Label;
 System::Windows::Forms::Label^				    WS_Label;
 System::Windows::Forms::Label^					label14;
 System::Windows::Forms::Label^					label16;
 System::Windows::Forms::GroupBox^				groupBox1;
 System::Windows::Forms::Label^				    TH_SD__Label;
 System::Windows::Forms::Label^				    UVLO_Label;
 System::Windows::Forms::Label^					TH_WRN__Label;
 System::Windows::Forms::Label^					OCD_Label;
 System::Windows::Forms::Label^					label3;
 System::Windows::Forms::Label^					label2;
 System::Windows::Forms::Label^					UVLO;
 System::Windows::Forms::Label^					label1;
 System::Windows::Forms::GroupBox^				groupBox2;
 System::Windows::Forms::Label^					Sensor2_Label;
 System::Windows::Forms::Label^					Sensor1_Label;
 System::Windows::Forms::Label^					label5;
 System::Windows::Forms::Label^					label7;
 System::Windows::Forms::GroupBox^				groupBox6;
 System::Windows::Forms::Label^					MOduleStateWord_Label;
 System::Windows::Forms::Label^					DriverStateWord_Label;
 System::Windows::Forms::Label^					label23;
 System::Windows::Forms::Label^					label22;
 System::Windows::Forms::GroupBox^				groupBox7;
 System::Windows::Forms::Label^					label37;
 System::Windows::Forms::Label^					ModuleConectionStae_Label;
 System::Windows::Forms::Label^					FTDIConectionState_label;
 System::Windows::Forms::Label^					label35;
 System::Windows::Forms::GroupBox^				groupBox5;
 System::Windows::Forms::Label^					Hiz_Label;
 System::Windows::Forms::Label^					label45;
 System::Windows::Forms::Label^					BUSY_Label;
 System::Windows::Forms::Label^					label43;
 System::Windows::Forms::Label^					SW_EVN_Label;
 System::Windows::Forms::Label^					label41;
 System::Windows::Forms::Label^					SW_F_Label;
 System::Windows::Forms::Label^					label39;
 System::Windows::Forms::Label^					WRONG_CMD__Label;
 System::Windows::Forms::Label^					label49;
 System::Windows::Forms::Label^					SCK_Mode__Label;
 System::Windows::Forms::Label^					label47;
 System::Windows::Forms::Label^					NOT_PERF_CMD_Label;
 System::Windows::Forms::Label^					label53;
 System::Windows::Forms::Label^					DIR_Label;
 System::Windows::Forms::Label^					label51;
 System::Windows::Forms::Label^					VersionHW_Label;
 System::Windows::Forms::Label^					VersionSW_Label;
 System::Windows::Forms::Label^					label55;
 System::Windows::Forms::Label^					label54;
 System::ComponentModel::IContainer^			components;

 shared_ptr<MxComm>* FTDI;
 std::shared_ptr<DF1_Module>* DF;
 DriverRegisterView^ RegisterView;
 HelperForm^ Help;
 FTDIView^ FT_Setup;
 SPIN_RegsStruct_TypeDef* RegisterMap;
 ConfigurationFile* RegisterConfig;
 HiRessTimeMeasure* TimeCounter;
 UINT				nSpeed;
 UINT				nDataBits;
 UINT				nParity;
 UINT				nStopBits;
 DWORD				SelectedDevice;
 char*               ProgramPath;
 MoveProtocol*      DriveProtocol;
 ProgramConfigFile* ProgramConf;
 double g_CoenficinetMinSpeed;
private: System::Windows::Forms::ToolStripMenuItem^  ��������ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  ������������ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  ����������������ToolStripMenuItem;
private: System::Windows::Forms::Label^  BrakefoLabel;
private: System::Windows::Forms::Label^  AccInfoLabel;
private: System::Windows::Forms::Label^  label8;
private: System::Windows::Forms::ToolStripMenuItem^  �������ToolStripMenuItem;
		 double g_CoenficinetSpeed;
void  MainView::InitLabels(void);
void DecodeStatusWord(int* Word);
bool FlagStartSrive;
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void POllingtimer_Tick(System::Object^  sender, System::EventArgs^  e);
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e);
protected: 	virtual System::Void WndProc(Message% m) override;
void DeivceNotificator(int Messsage);
void ShowFTDIDialog(void);
void DecodeModuleParametr(bool Err,int* VontrolWord, int* SpeedBegin, int* SpeedEnd, int*TimeAcc, int* TimeBrake, int* StepMode,int* StepCount);
void  ShowStatusKabels(void);
void  HideStatusKabels(void);
private: System::Void ������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	DF->get()->Level = LOGING_LEVEL_ERROR;
	����������������ToolStripMenuItem->Checked = false;
	
}
private: System::Void ����������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	DF->get()->Level = LOGING_LEVEL_INFO;
	������������ToolStripMenuItem->Checked = false;
}
	private: System::Void �������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

		if (!Help->Visible)
			if (Help->IsDisposed)
			{
				Help = gcnew HelperForm();
			}
			Help->Show();
		}
};
}

