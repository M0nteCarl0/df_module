#include "HiRessTimeMeasure.h"
static unsigned long long TimerHandlers = 0;
/******************************************************************************/
HiRessTimeMeasure::HiRessTimeMeasure(void):
	 _Sec(0),
	 _Milisec(0)
{
	TimerHandlers++;
}
/******************************************************************************/

HiRessTimeMeasure::~HiRessTimeMeasure(void)
{
	if(TimerHandlers != 0)
	{
	TimerHandlers--;
	}
}
/******************************************************************************/

void HiRessTimeMeasure:: Start(void)
{   QueryPerformanceFrequency(&_Freq);
	QueryPerformanceCounter(&_Begin);

}
/******************************************************************************/
void HiRessTimeMeasure::End(void)
{
	QueryPerformanceCounter(&_End);
	_Total.QuadPart = _End.QuadPart -  _Begin.QuadPart;
}
/******************************************************************************/
double HiRessTimeMeasure::GetEllapsedSeconds(void)
{
	_Sec =(DOUBLE) _Total.QuadPart/_Freq.QuadPart;
	return _Sec;
}
/******************************************************************************/
	double HiRessTimeMeasure::GetEllapsedMiliseconds(void)
{
	_Milisec = (DOUBLE)(_Total.QuadPart*1000)/_Freq.QuadPart;
	return _Milisec;
}
/******************************************************************************/
double HiRessTimeMeasure::GetEllapsedMicroseconds(void)
{
	_Microsec =(DOUBLE) (_Total.QuadPart*1000000)/_Freq.QuadPart;
	return _Microsec;
}
/******************************************************************************/
unsigned long long HiRessTimeMeasure::GetTotalCountHandles(void)
{
	return TimerHandlers;
}
/******************************************************************************/
