#include "ProgramConfigFile.h"


ProgramConfigFile::ProgramConfigFile(void)
{
	SetDelimetrToken("=");

	Conntrol = 1;
	SpeedBegin = 100;
	SpeedEnd = 2000;
	TimeAcc = 100;
	TimeBrake = 100;
	StepCount = 6000;
	StepNode = 0x1;


}


ProgramConfigFile::~ProgramConfigFile(void)
{
}

void ProgramConfigFile::Reorginize(const char * FileName)
{
	bool Flag = Open(FileName);
	if (Flag)
	{
		GetIntParam("Control", Conntrol);
		GetIntParam("SpeedBegin",SpeedBegin);
		GetIntParam("SpeedEnd",SpeedEnd);
		GetIntParam("TimeAcc", TimeAcc);
		GetIntParam("TimeBrake", TimeBrake);
		GetIntParam("StepCount", StepCount);
		GetIntParam("StepMode", StepNode);
		
	}
	else
	{

		CreateDefaultParamsConfig();

	}
}
/************************************************************/
void ProgramConfigFile::CreateDefaultParamsConfig(void)
{
	FILE* Config = fopen("DF_Pult.ini", "w+");
	if (Config)
	{

		fprintf(Config, "Control=%i\nSpeedBegin=%i\nSpeedEnd=%i\nTimeAcc=%i\nTimeBrake=%i\nStepCount=%i\nStepMode=%i\n",
			Conntrol,
			SpeedBegin,
			SpeedEnd,
			TimeAcc,
			TimeBrake,
			StepCount,
			StepNode);		
	}


	fflush(Config);
	fclose(Config);

}
