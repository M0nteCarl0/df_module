#pragma once
#include <Windows.h>
#include "MXCOMM.H"
#include <memory>
namespace DF_EnginerPult 
{
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class FTDIView : public System::Windows::Forms::Form
	{

	private:
#pragma region Windows Form Designer generated code
	void InitializeComponent(void);
#pragma endregion
	void ShowDeviceInfoDetail(int ID_Device, FT_DEVICE_LIST_INFO_NODE* _DeviceLIst );
	void EnumFTDIDevices(void);
	System::Void Ok_Click(System::Object^  sender, System::EventArgs^  e);
	System::Void Cancel_Click(System::Object^  sender, System::EventArgs^  e);
	System::Void listBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

	System::ComponentModel::Container  ^components;
	System::Windows::Forms::Label^     label1;
	System::Windows::Forms::Label^     label2;
	System::Windows::Forms::Label^     label3;
	System::Windows::Forms::TextBox^   Baudrate;
	System::Windows::Forms::TextBox^   DataBit;
	System::Windows::Forms::TextBox^   StopBit;
	System::Windows::Forms::Button^    Ok;
	System::Windows::Forms::Button^    Cancel;
	System::Windows::Forms::ListBox^   listBox1;
	System::Windows::Forms::Label^     label4;
	System::Windows::Forms::GroupBox^  groupBox1;
	System::Windows::Forms::Label^     FTD_Desvription;
	System::Windows::Forms::Label^     label10;
	System::Windows::Forms::Label^     FTDI_LocalID;
	System::Windows::Forms::Label^     label8;
	System::Windows::Forms::Label^     FTDi_ID;
	System::Windows::Forms::Label^     label5;
	System::Windows::Forms::CheckBox^  Bussy;

public: 

	FTDIView(void);
	~FTDIView();
	void SetParametrsFTDI(UINT Speed,UINT  DataBits,UINT  StopBitS);  
	void GetParametrsFTDI( UINT% Speed,UINT%  DataBits,UINT%  StopBitS);
	void SetMXComm(std::shared_ptr<MxComm>* MXCOMM);
	DWORD GetSelectedDeviceID(void);
	int									SelectedDeviceID;
	UINT								nSpeed;   
	UINT								nDataBits; 
	UINT								nParity;   
	UINT								nStopBits;
	FT_DEVICE_LIST_INFO_NODE*			DeviceLIst;
	std::shared_ptr<MxComm>*			_MXCOMM;
	
	};
}
