#include "DF1_Module.h"

/*****************************************************************************************************************/
DF1_Module::DF1_Module(void) {
  SetModuleAdress(0x9);
  // InitComm(19200);
}

/*****************************************************************************************************************/
DF1_Module::~DF1_Module(void) {}
/*****************************************************************************************************************/
int DF1_Module::WriteSpeedProfileRun(int *ControlWord, int *BeginSpeed,
                                     int *EndSpeed, int *TimeAcc, int *TimeStop,
                                     int *StepCount, int *StepMode) {

  m_DataS[2] = CommadID_WRITE_SPEED_PROFILE_RUN;
  int NS = 20;
  int NR = 20;

  m_DataS[5] = *ControlWord & 0x7f;

  m_DataS[6] = *BeginSpeed & 0x7f;
  m_DataS[7] = (*BeginSpeed >> 7) & 0x7f;

  m_DataS[8] = (*EndSpeed) & 0x7f;
  m_DataS[9] = (*EndSpeed >> 7) & 0x7f;
  m_DataS[10] = (*EndSpeed >> 14) & 0x7f;

  m_DataS[11] = (*TimeAcc) & 0x7f;
  m_DataS[12] = (*TimeAcc >> 7) & 0x7f;

  m_DataS[13] = (*TimeStop) & 0x7f;
  m_DataS[14] = (*TimeStop >> 7) & 0x7f;

  m_DataS[15] = (*StepCount) & 0x7f;
  m_DataS[16] = (*StepCount >> 7) & 0x7f;
  m_DataS[17] = (*StepCount >> 14) & 0x7f;

  m_DataS[18] = *StepMode & 0x7f;

  int REt = ReadCommand(m_DataS, NS, m_DataR, NR);
  *ControlWord = m_DataR[5];
  return REt;
}
/*****************************************************************************************************************/
int DF1_Module::ReadSpeedProfileRun(int *ControlWord, int *BeginSpeed,
                                    int *EndSpeed, int *TimeAcc, int *TimeStop,
                                    int *StepCount, int *StepMode) {
  m_DataS[2] = CommadID_READ_SPEED_PROFILE;
  int NS = 5;
  int NR = 20;
  int REt = ReadCommand(m_DataS, NS, m_DataR, NR);
  *ControlWord = m_DataR[5];

  *BeginSpeed = m_DataR[6] | m_DataR[7] << 7;
  *EndSpeed = m_DataR[8] | m_DataR[9] << 7 | m_DataR[10] << 14;

  *TimeAcc = m_DataR[11] | m_DataR[12] << 7;
  *TimeStop = m_DataR[13] | m_DataR[14] << 7;

  *StepCount = m_DataR[15] | m_DataR[16] << 7 | m_DataR[17] << 14;
  *StepMode = m_DataR[18];
  return REt;
}
/*****************************************************************************************************************/
int DF1_Module::SoftStopMotor(void) {

  m_DataS[2] = CommadID_WRITE_STOP_MOTOR;
  int NS = 5;
  int NR = 5;
  int REt = ReadCommand(m_DataS, NS, m_DataR, NR);

  return REt;
}
/*****************************************************************************************************************/
int DF1_Module::ReadStateDrive(int *StateWord, int *AbsolutePositionMotor,
                               int *CurrentMotorSpeed) {
  m_DataS[2] = CommadID_READ_STATE_WORD;
  int NS = 5;
  int NR = 16;
  int REt = ReadCommand(m_DataS, NS, m_DataR, NR);

  *StateWord = m_DataR[5] | m_DataR[6] << 7 | m_DataR[7] << 14;
  *AbsolutePositionMotor = m_DataR[8] | m_DataR[9] << 7 | m_DataR[10] << 14;
  if (m_DataR[11]) {
    *AbsolutePositionMotor *= -1;
  };

  *CurrentMotorSpeed = m_DataR[12] | m_DataR[13] << 7 | m_DataR[14] << 14;

  return REt;
}
/*****************************************************************************************************************/
int DF1_Module::ReadRegistersMotor(cSPIN_RegsStruct_TypeDef *Registers) {
  // Section.Enter();
  m_DataS[2] = CommadID_READ_DRIVER_SETUP;
  int NS = 5;
  int NR = 67;
  int REt = ReadCommand(m_DataS, NS, m_DataR, NR);
  ConvcSPIN_ByteToRegsStruct_TypeDef(Registers, (uint8_t *)&m_DataR[5]);
  ///	Section.Leave();
  return REt;
}
/*****************************************************************************************************************/
int DF1_Module::WriteRegistersMotor(cSPIN_RegsStruct_TypeDef *Registers) {

  //	Section.Enter();
  m_DataS[2] = CommadID_WRITE_DRIVER_SETUP;
  int NS = 67;
  int NR = 0;
  ConvSPIN_RegsStruct_TypeDefToByte(Registers, (uint8_t *)&m_DataS[5]);
  int REt = WriteCommand(m_DataS, NS);
  // Section.Leave();
  return REt;
}
/*****************************************************************************************************************/
