#include "MoveProtocol.h"
#include <string>>
/*************************************************/
MoveProtocol::MoveProtocol()
{
	
	File = NULL;
	Time = NULL;
	CurrentTime = NULL;
	Section = CriticalSection("FileMoni");
	
}
/*************************************************/
MoveProtocol::~MoveProtocol()
{
	if (File)
	{
		Destroy();
	}
}
/*************************************************/
bool MoveProtocol::Create(const char * Name)
{
	bool Flag = true;

	if (File!=NULL)
	{
		Destroy();
	}


	File = fopen(Name, "w+");

	if (File == NULL )
	{
		Flag = false;
	}

	return Flag;
};
/*************************************************/
void MoveProtocol::PutInputParametrs(int * ControlWord, int * SpeedBegin, int * SpeedEnd, int * TimeAcc, int * TimeBraake, int * StepCount,int* StepMode)
{

	if (File)
	{

		double SpeedBeginL = *SpeedBegin * 0.23842;
		double SpeedEndL = *SpeedEnd * 0.014901;
		fprintf(File, "User defined data  \n");
		fprintf(File, "Control Word:0x%x\n",*ControlWord);
		fprintf(File, "SpeedBegin:%.1f [%i]\n", SpeedBeginL,*SpeedBegin);
		fprintf(File, "SpeedEnd:%.1f[%i]\n", SpeedEndL,*SpeedEnd);
		fprintf(File, "Time Acc:%i\n", *TimeAcc);
		fprintf(File, "Time Brake:%i\n", *TimeBraake);
		fprintf(File, "StepCount:%i\n", *StepCount);
		fprintf(File, "StepMode:0x%x\n", *StepMode);
		fprintf(File, "\n");
		
	}

};
/*************************************************/
void MoveProtocol::PutInfo(int * StateWord, float * Sppeed, int * Positiob)
{
	if (File)
	{
		Section.Enter();
		SYSTEMTIME Time;
		GetLocalTime(&Time);
		int SpeedLSB = *Sppeed / 0.014901;
		fprintf(File, "[%i.%i.%i  %i:%i:%i:%i]  GlobalState:0x%x DriverState:0x%x  ModuleState:0x%x  Speed:%.2f[%i] Position:%i\n", Time.wYear, Time.wMonth, Time.wDay, Time.wHour, Time.wMinute, Time.wSecond,
			Time.wMilliseconds,*StateWord, *StateWord & 0xFFFF, (*StateWord >> 16) & 0x1F, *Sppeed, SpeedLSB, *Positiob);
		fflush(File);
		Section.Leave();

	}
}
/*************************************************/
void MoveProtocol::Destroy(void)
{
	fflush(File);
	fclose(File);
	File = NULL;
}
/*************************************************/
