#pragma once
#include "MXCOMM.H"
#include <memory>
#include <stdio.h>
#include "CriticalSection.h"
#define  NCOM   128
#define  mID    0x96

enum LOGING_LEVEL
{
	LOGING_LEVEL_INFO,
	LOGING_LEVEL_ERROR,

};
class BaseNewProtocolDevice
{
public:
	LOGING_LEVEL Level;
    BaseNewProtocolDevice(void);
    BaseNewProtocolDevice(int _ModuleAdress);
    ~BaseNewProtocolDevice(void);
	void CreateProtocol(void);
void	WriteToProtocol(BYTE *DataS, int NS, BYTE *DataR, int NumRec,int Erc);
    void     InitDefaultCRCCheck(void);
    int InitComm(
                                    UINT  _nSpeed    = 115200,
                                    UINT  _nDataBits = 8, 
                                    UINT  _nParity   = 1, 
                                    UINT  _nStopBits = 2,
                                    int   _Timeout   = 120);


	void     Init(std::shared_ptr<MxComm>*   Source);
    void     SetModuleAdress(int _ModuleAdress);
    int      ReadVersion      (BYTE *Name, BYTE *Hard, BYTE *Soft);
    BYTE     MakeCRC   (BYTE *Data, int N);
    void     AddCRC    (BYTE *Data, int N);
    bool     CheckCRC  (BYTE *Data, int N);
    int      CheckErrorWR (BYTE *DataS, int NS, BYTE *DataR, int NR);
    int      WriteCommand (BYTE *DataS, int NS);
    int      ReadCommand  (BYTE *DataS, int NS, BYTE *DataR, int NR);
    
    bool     m_FlgDebugS;
    bool     m_FlgDebugR;
    bool     m_FlgCheckCRCwrite;
    bool     m_FlgCheckCRCread;
    std::shared_ptr<MxComm>*   m_Comm;
    BYTE     m_DataS [NCOM];
    BYTE     m_DataR [NCOM];
    int      m_ModuleAdress;
    bool     m_FTDIisConected;
	FILE* Protocol;
	CriticalSection Section;
	
};

