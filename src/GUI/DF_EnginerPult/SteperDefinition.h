#include <stdint.h>
#pragma once
#ifndef __SDEF__
#define __SDEF__

typedef struct SPIN_RegsStruct_TypeDef {
  uint32_t ABS_POS;
  uint16_t EL_POS;
  uint32_t MARK;
  uint32_t SPEED;
  uint16_t ACC;
  uint16_t DEC;
  uint16_t MAX_SPEED;
  uint16_t MIN_SPEED;
  uint16_t FS_SPD;
  uint8_t TVAL_HOLD;
  uint8_t TVAL_RUN;
  uint8_t TVAL_ACC;
  uint8_t TVAL_DEC;
  uint16_t RESERVED_3;
  uint8_t T_FAST;
  uint8_t TON_MIN;
  uint8_t TOFF_MIN;
  uint8_t RESERVED_2;
  uint8_t ADC_OUT;
  uint8_t OCD_TH;
  uint8_t RESERVED_1;
  uint8_t STEP_MODE;
  uint8_t ALARM_EN;
  uint16_t GATECFG1;
  uint8_t GATECFG2;
  uint16_t CONFIG;
  uint16_t STATUS;
  uint8_t InverseSensorInput;
  uint16_t CheckWord;
} cSPIN_RegsStruct_TypeDef;

bool CompareSPIN_RegsStruct(SPIN_RegsStruct_TypeDef *Val,
                            SPIN_RegsStruct_TypeDef *Val2);
#endif
