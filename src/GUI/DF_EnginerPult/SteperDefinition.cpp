#include "SteperDefinition.h"
bool CompareSPIN_RegsStruct(SPIN_RegsStruct_TypeDef* Val, SPIN_RegsStruct_TypeDef* Val2)
{
	bool flag = true;
	if (Val->ACC!= Val2->ACC || (Val->DEC != Val2->DEC) || (Val->MAX_SPEED != Val2->MAX_SPEED)  
		|| (Val->MIN_SPEED != Val2->MIN_SPEED ) || (Val->FS_SPD != Val2->FS_SPD) || (Val->TVAL_ACC != Val2->TVAL_ACC) || (Val->TVAL_DEC != Val2->TVAL_DEC)
		|| (Val->TVAL_HOLD != Val2->TVAL_HOLD) || (Val->TVAL_RUN != Val2->TVAL_RUN) || (Val->T_FAST != Val2->T_FAST) ||  (Val->TON_MIN != Val2->TON_MIN) 
		|| (Val->TOFF_MIN != Val2->TOFF_MIN) || (Val->OCD_TH != Val2->OCD_TH) || (Val->STEP_MODE != Val2->STEP_MODE)|| (Val->ALARM_EN != Val2->ALARM_EN) || 
		(Val->CONFIG != Val2->CONFIG)
		)
	{
		flag = false;
	}


	return flag;
}