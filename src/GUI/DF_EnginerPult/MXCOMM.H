//----------------------------------------------------------------------------
//    Create by Novikov
//    data 29.06.97
//    last data 25.07.97
//----------------------------------------------------------------------------
#ifndef __MXCOMM_H
#define __MXCOMM_H

#include <windows.h>
#include <WinUser.h>
//#include "stdafx.h"
#include "FTD2XX.H"	// Added by ClassView
#include <Dbt.h>
#define COMM_BUFF 64
//----------------------------------------------------------------------------
class MxComm{
   public:

      char	      m_Buff [COMM_BUFF];

      FT_HANDLE   m_FTDI;

      MxComm  ();	      // GENERIC_READ | GENERIC_WRITE
      ~MxComm (){};


	  FT_DEVICE_LIST_INFO_NODE* EnumerateFTDIDevices(size_t* CountConectedDevices);
	  bool AutoConnectForFreeModule(const UINT nSpeed, const UINT nDataBits, const UINT nParity, const UINT nStopBits);
	  bool Configure(const UINT nSpeed, const UINT nDataBits, const UINT nParity, const UINT nStopBits);
	  bool ConfigureByID(const UINT ID, const UINT nSpeed, const UINT nDataBits, const UINT nParity, const UINT nStopBits);
	

      bool Close     ();

      bool Read      (char *Buffer, DWORD BytesToRead, DWORD *BytesReturned);
      bool Write     (char *Buffer, DWORD BytesToWrite, DWORD *BytesReturned);

      void Delay     (int Del);
      void PurgeTX   ();
      void PurgeRX   ();
      void SetTimeout(DWORD Constant);
      bool CommMounted(void);
      bool WRcommand (BYTE *DataSend, int NS, BYTE *DataRec, int NR, DWORD *NumRec, bool FlgDebugS, bool FlgDebugR);
	  void InitializeDeviceListener(HWND WindowsHandle);
	  DEV_BROADCAST_DEVICEINTERFACE  Broadcaster;
	  HDEVNOTIFY NotifityHandler;









};
//----------------------------------------------------------------------------

#endif
