// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "ByteConversion.h"
void Conversion::WordToByte(uint16_t &Source,uint8_t &Dest1, uint8_t &Dest2)
{
	Dest1 = Source & 0xFF;
	Dest2 = Source >> 8;	
};
/***************************************************************************/
void Conversion::ByteToWord(uint16_t &Source,uint8_t &Dest1, uint8_t &Dest2)
{
	Source = Dest1 | (Dest2 << 8);
};
/***************************************************************************/

void Conversion::DWordToByte(uint32_t &Source,uint8_t &Dest1, uint8_t &Dest2,uint8_t& Dest3,uint8_t& Dest4)
{
	uint16_t Word1 = Source & 0xFFFF;
	uint16_t Word2 = Source >>16;
   Conversion::WordToByte(Word1,Dest1,Dest2);
   Conversion::WordToByte(Word2,Dest3,Dest4);
};
/***************************************************************************/
void  Conversion::ByteToDWord(uint32_t &Source,uint8_t &Dest1, uint8_t &Dest2,uint8_t& Dest3,uint8_t& Dest4)
{
	uint16_t Word1 = 0;
	uint16_t Word2 = 0;
   Conversion::ByteToWord(Word1,Dest1,Dest2);
   Conversion::ByteToWord(Word2,Dest3,Dest4);
	Source = Word1 | (Word2 << 16);
};
/***************************************************************************/

void  Conversion::QWordToByte(uint64_t &Source,uint8_t &Dest1, uint8_t &Dest2,uint8_t& Dest3,uint8_t& Dest4, uint8_t &Dest5, uint8_t &Dest6,uint8_t& Dest7,uint8_t& Dest8)
{
	uint32_t Dword1 = Source  &  0xffffffff ;
	uint32_t Dword2 = Source >> 32;
   Conversion::DWordToByte(Dword1,Dest1,Dest2,Dest3,Dest4);
   Conversion::DWordToByte(Dword2,Dest5,Dest6,Dest7,Dest8);

};
/***************************************************************************/
void Conversion::ByteToQWord(uint64_t &Source,uint8_t &Dest1, uint8_t &Dest2,uint8_t& Dest3,uint8_t& Dest4, uint8_t &Dest5, uint8_t &Dest6,uint8_t& Dest7,uint8_t& Dest8)
{
	uint32_t Dword1 = 0;
	uint32_t Dword2 = 0;
   Conversion::ByteToDWord(Dword1,Dest1,Dest2,Dest3,Dest4);
   Conversion::ByteToDWord(Dword2,Dest5,Dest6,Dest7,Dest8);
	Source = Dword1 | (Dword2 << 32);
};
/***************************************************************************/
void Conversion::ByteTo7Bit(uint8_t &Source,uint8_t &Dest1,uint8_t &Dest2)
{
	Dest1 = Source & 0x7F;
	Dest2 = (Source >> 7);

};
/***************************************************************************/
void Conversion::Bit7ToByte(uint8_t &Source,uint8_t &Dest1,uint8_t &Dest2)
{
	Source = Dest1 & 0x7F |(Dest2 << 7);
};
/***************************************************************************/
void  Conversion::WordTo7Bit(uint16_t &Source,uint8_t &Dest1,uint8_t &Dest2,uint8_t &Dest3,uint8_t &Dest4)
{
	uint8_t D3[2];
   Conversion::WordToByte(Source,D3[0],D3[1]);
   Conversion::ByteTo7Bit(D3[0],Dest1,Dest2);
   Conversion::ByteTo7Bit(D3[1],Dest3,Dest4);
};
/***************************************************************************/
void Conversion::Bit7ToWord(uint16_t &Source,uint8_t &Dest1,uint8_t &Dest2,uint8_t &Dest3,uint8_t &Dest4)
{
	uint8_t D3[2];
   Conversion::Bit7ToByte(D3[0],Dest1,Dest2);
   Conversion::Bit7ToByte(D3[1],Dest1,Dest2);
   Conversion::ByteToWord(Source,D3[0],D3[1]);

};
/***************************************************************************/
void Conversion:: DWordTo7Bit(uint32_t &Source,uint8_t &Dest1, uint8_t &Dest2,uint8_t& Dest3,uint8_t& Dest4,uint8_t &Dest5, uint8_t &Dest6,uint8_t& Dest7,uint8_t& Dest8)
{

	uint8_t D3[4];
	
   Conversion::DWordToByte(Source,D3[0],D3[1],D3[2],D3[3]);
   Conversion::ByteTo7Bit(D3[0],Dest1,Dest2);
   Conversion::ByteTo7Bit(D3[1],Dest3,Dest4);
   Conversion::ByteTo7Bit(D3[2],Dest5,Dest6);
   Conversion::ByteTo7Bit(D3[3],Dest7,Dest8);

}
/***************************************************************************/
void Conversion:: Bit7ToDWord(uint32_t &Source,uint8_t &Dest1, uint8_t &Dest2,uint8_t& Dest3,uint8_t& Dest4,uint8_t &Dest5, uint8_t &Dest6,uint8_t& Dest7,uint8_t& Dest8)
{
	uint8_t D3[4];
   Conversion::Bit7ToByte(D3[0],Dest1,Dest2);
   Conversion::Bit7ToByte(D3[1],Dest3,Dest4);
   Conversion::Bit7ToByte(D3[2],Dest5,Dest6);
   Conversion::Bit7ToByte(D3[3],Dest6,Dest7);
   Conversion::ByteToDWord(Source,D3[0],D3[1],D3[2],D3[3]);
};
/***************************************************************************/
  void Conversion:: FAST_WordToByte(uint16_t &Source,uint8_t &Dest1, uint8_t &Dest2)
  {
    uint8_t Data[2];

    FAST_GenericTo7Bit( (uint64_t&)Source,Data,2);
    Dest1 = Data[0];
    Dest2 = Data[1];
  }
  
  
  /***************************************************************************/
   void Conversion:: FAST_GenericTo7Bit(uint64_t &Source,uint8_t* Buff,uint8_t N)
   {
     
     for(uint8_t i = 0;i<N;i++)
    {
      
      if(i==0)
      {
        Buff[i] = Source & 0x7f ;
      }
      else
      {  
        Buff[i] =  Source >>7*i;
      }
      
    }
     
     
   }
  /***************************************************************************/
  void  Conversion::FAST_ByteToWord(uint16_t &Dest,uint8_t &Source1, uint8_t &Source2)
  {
    
     uint8_t Data[2];
     Data[0] = Source1;
     Data[1] = Source2;
     FAST_7BitToZGeneric((uint64_t&)Dest,Data,2);
  }
  /***************************************************************************/
  void Conversion::FAST_7BitToZGeneric(uint64_t &Dest,uint8_t* Source,uint8_t N)
  {
    
     for(uint8_t i = 0;i<N;i++)
    {
      
      if(i==0)
      {
        Dest = Source[i] & 0x7f ;
      }
      else
      {  
        Dest|=  Source[i] << 7*i;
      }
      
    }
    
  }
 /***************************************************************************/
  