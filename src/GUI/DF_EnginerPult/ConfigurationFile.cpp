#include "ConfigurationFile.h"
/**********************************************************/
ConfigurationFile::ConfigurationFile(void) {
  memset(&Registers, 0, sizeof(Registers));
  SetDelimetrToken("=");
}
/**********************************************************/
ConfigurationFile::~ConfigurationFile(void) {}
/**********************************************************/
bool ConfigurationFile::Load(const char *FileName) {

  bool Flag = Open(FileName);
  if (Flag) {

    int ABS_POS;
    int EL_POS;
    int MARK;
    int ACC;
    int DEC;
    int MAX_SPEED;
    int MIN_SPEED;
    int FS_SPD;
    int TVAL_HOLD;
    int TVAL_RUN;
    int TVAL_ACC;
    int TVAL_DEC;
    int T_FAST;
    int TON_MIN;
    int TOFF_MIN;
    int OCD_TH;
    int STEP_MODE;
    int ALARM_EN;
    int GATECFG1;
    int GATECFG2;
    int CONFIG;
    int InvertedSensorInput;

    GetIntParam("ABS_POS", ABS_POS);
    GetIntParam("EL_POS", EL_POS);
    GetIntParam("MARK", MARK);
    GetIntParam("ACC", ACC);
    GetIntParam("DEC", DEC);
    GetIntParam("MAX_SPEED", MAX_SPEED);
    GetIntParam("MIN_SPEED", MIN_SPEED);
    GetIntParam("FS_SPD", FS_SPD);
    GetIntParam("TVAL_HOLD", TVAL_HOLD);
    GetIntParam("TVAL_RUN", TVAL_RUN);
    GetIntParam("TVAL_ACC", TVAL_ACC);
    GetIntParam("TVAL_DEC", TVAL_DEC);
    GetIntParamHex("T_FAST", T_FAST);
    GetIntParam("TON_MIN", TON_MIN);
    GetIntParam("TOFF_MIN", TOFF_MIN);
    GetIntParam("OCD_TH", OCD_TH);
    GetIntParam("STEP_MODE", STEP_MODE);
    GetIntParam("ALARM_EN", ALARM_EN);
    GetIntParam("GATECFG1", GATECFG1);
    GetIntParam("GATECFG2", GATECFG2);
    GetIntParamHex("CONFIG", CONFIG);
    GetIntParamHex("InvertedSensor", InvertedSensorInput);

    Registers.ABS_POS = (uint32_t)ABS_POS;
    Registers.EL_POS = (uint16_t)EL_POS;
    Registers.MARK = (uint32_t)MARK;
    Registers.ACC = (uint16_t)ACC;
    Registers.DEC = (uint16_t)DEC;
    Registers.MAX_SPEED = (uint16_t)MAX_SPEED;
    Registers.MIN_SPEED = (uint16_t)MIN_SPEED;
    Registers.FS_SPD = (uint16_t)FS_SPD;
    Registers.TVAL_HOLD = (uint8_t)TVAL_HOLD;
    Registers.TVAL_RUN = (uint8_t)TVAL_RUN;
    Registers.TVAL_ACC = (uint8_t)TVAL_ACC;
    Registers.TVAL_DEC = (uint8_t)TVAL_DEC;
    Registers.T_FAST = (uint8_t)T_FAST;
    Registers.TON_MIN = (uint8_t)TON_MIN;
    Registers.TOFF_MIN = (uint8_t)TOFF_MIN;
    Registers.OCD_TH = (uint8_t)OCD_TH;
    Registers.STEP_MODE = (uint8_t)STEP_MODE;
    Registers.ALARM_EN = (uint8_t)ALARM_EN;
    Registers.GATECFG1 = (uint16_t)GATECFG1;
    Registers.GATECFG2 = (uint8_t)GATECFG2;
    Registers.CONFIG = (uint16_t)CONFIG;
    Registers.InverseSensorInput = (uint8_t)InvertedSensorInput;
  }

  return Flag;
}
/**********************************************************/

bool ConfigurationFile::Save(const char *FileName,
                             cSPIN_RegsStruct_TypeDef *RegistersW) {
  bool Flag = false;
  FILE *Configuration = fopen(FileName, "w+");

  if (Configuration) {
    Flag = true;

    fprintf(Configuration,
            "ABS_POS=%i\nEL_POS=%i\nMARK=%i\nACC=%i\nDEC=%i\nMAX_SPEED=%i\nMIN_"
            "SPEED=%i\nFS_SPD=%i\nTVAL_HOLD=%i\nTVAL_RUN=%i\nTVAL_ACC=%i\nTVAL_"
            "DEC=%i\nT_FAST=0x%x\nTON_MIN=%i\nTOFF_MIN=%i\nOCD_TH=%i\nSTEP_"
            "MODE=%i\nALARM_EN=%i\nGATECFG1=%i\nGATECFG2=%i\nCONFIG=0x%"
            "x\nInvertedSensor=%i\n",
            RegistersW->ABS_POS, RegistersW->EL_POS, RegistersW->MARK,
            RegistersW->ACC, RegistersW->DEC, RegistersW->MAX_SPEED,
            RegistersW->MIN_SPEED, RegistersW->FS_SPD, RegistersW->TVAL_HOLD,
            RegistersW->TVAL_RUN, RegistersW->TVAL_ACC, RegistersW->TVAL_DEC,
            RegistersW->T_FAST, RegistersW->TON_MIN, RegistersW->TOFF_MIN,
            RegistersW->OCD_TH, RegistersW->STEP_MODE, RegistersW->ALARM_EN,
            RegistersW->GATECFG1, RegistersW->GATECFG2, RegistersW->CONFIG,
            RegistersW->InverseSensorInput);
    fflush(Configuration);
    fclose(Configuration);
  }

  return Flag;
}
/**********************************************************/
