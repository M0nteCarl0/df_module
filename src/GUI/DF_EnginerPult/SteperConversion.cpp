// This is an independent project of an individual developer. Dear PVS-Studio,
// please check it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// http://www.viva64.com

#include "SteperConversion.h"

void ConvSPIN_RegsStruct_TypeDefToByte(cSPIN_RegsStruct_TypeDef *Source,
                                       uint8_t *Dest) {

  DwordToByte(&Source->ABS_POS, &Dest[0], &Dest[1], &Dest[2]);
  DwordToByte((uint32_t *)&Source->EL_POS, &Dest[3], &Dest[4], &Dest[5]);
  DwordToByte(&Source->MARK, &Dest[6], &Dest[7], &Dest[8]);
  DwordToByte(&Source->SPEED, &Dest[9], &Dest[10], &Dest[11]);

  DwordToByte((uint32_t *)&Source->ACC, &Dest[12], &Dest[13], &Dest[14]);
  DwordToByte((uint32_t *)&Source->DEC, &Dest[15], &Dest[16], &Dest[17]);
  DwordToByte((uint32_t *)&Source->MAX_SPEED, &Dest[18], &Dest[19], &Dest[20]);
  DwordToByte((uint32_t *)&Source->MIN_SPEED, &Dest[21], &Dest[22], &Dest[23]);
  DwordToByte((uint32_t *)&Source->FS_SPD, &Dest[24], &Dest[25], &Dest[26]);

  ByteTo7Bit(&Source->TVAL_HOLD, &Dest[27], &Dest[28]);
  ByteTo7Bit(&Source->TVAL_RUN, &Dest[29], &Dest[30]);
  ByteTo7Bit(&Source->TVAL_ACC, &Dest[31], &Dest[32]);
  ByteTo7Bit(&Source->TVAL_DEC, &Dest[33], &Dest[34]);

  ByteTo7Bit(&Source->T_FAST, &Dest[35], &Dest[36]);
  ByteTo7Bit(&Source->TON_MIN, &Dest[37], &Dest[38]);
  ByteTo7Bit(&Source->TOFF_MIN, &Dest[39], &Dest[40]);
  ByteTo7Bit(&Source->ADC_OUT, &Dest[41], &Dest[42]);
  ByteTo7Bit(&Source->OCD_TH, &Dest[43], &Dest[44]);
  ByteTo7Bit(&Source->STEP_MODE, &Dest[45], &Dest[46]);
  ByteTo7Bit(&Source->ALARM_EN, &Dest[47], &Dest[48]);
  DwordToByte((uint32_t *)&Source->GATECFG1, &Dest[49], &Dest[50], &Dest[51]);
  ByteTo7Bit(&Source->GATECFG2, &Dest[52], &Dest[53]);
  DwordToByte((uint32_t *)&Source->CONFIG, &Dest[54], &Dest[55], &Dest[56]);
  DwordToByte((uint32_t *)&Source->STATUS, &Dest[57], &Dest[58], &Dest[59]);
  Dest[60] = Source->InverseSensorInput;
}
/***********************************************************************************************/
void ConvcSPIN_ByteToRegsStruct_TypeDef(cSPIN_RegsStruct_TypeDef *Source,
                                        uint8_t *Dest) {

  ByteToDword(&Source->ABS_POS, &Dest[0], &Dest[1], &Dest[2]);
  ByteToDword((uint32_t *)&Source->EL_POS, &Dest[3], &Dest[4], &Dest[5]);
  ByteToDword(&Source->MARK, &Dest[6], &Dest[7], &Dest[8]);
  ByteToDword(&Source->SPEED, &Dest[9], &Dest[10], &Dest[11]);
  ByteToDword((uint32_t *)&Source->ACC, &Dest[12], &Dest[13], &Dest[14]);
  ByteToDword((uint32_t *)&Source->DEC, &Dest[15], &Dest[16], &Dest[17]);
  ByteToDword((uint32_t *)&Source->MAX_SPEED, &Dest[18], &Dest[19], &Dest[20]);
  ByteToDword((uint32_t *)&Source->MIN_SPEED, &Dest[21], &Dest[22], &Dest[23]);
  ByteToDword((uint32_t *)&Source->FS_SPD, &Dest[24], &Dest[25], &Dest[26]);

  Bit7ToByte(&Source->TVAL_HOLD, &Dest[27], &Dest[28]);
  Bit7ToByte(&Source->TVAL_RUN, &Dest[29], &Dest[30]);
  Bit7ToByte(&Source->TVAL_ACC, &Dest[31], &Dest[32]);
  Bit7ToByte(&Source->TVAL_DEC, &Dest[33], &Dest[34]);

  Bit7ToByte(&Source->T_FAST, &Dest[35], &Dest[36]);
  Bit7ToByte(&Source->TON_MIN, &Dest[37], &Dest[38]);
  Bit7ToByte(&Source->TOFF_MIN, &Dest[39], &Dest[40]);
  Bit7ToByte(&Source->ADC_OUT, &Dest[41], &Dest[42]);
  Bit7ToByte(&Source->OCD_TH, &Dest[43], &Dest[44]);
  Bit7ToByte(&Source->STEP_MODE, &Dest[45], &Dest[46]);
  Bit7ToByte(&Source->ALARM_EN, &Dest[47], &Dest[48]);
  ByteToDword((uint32_t *)&Source->GATECFG1, &Dest[49], &Dest[50], &Dest[51]);
  Bit7ToByte(&Source->GATECFG2, &Dest[52], &Dest[53]);
  ByteToDword((uint32_t *)&Source->CONFIG, &Dest[54], &Dest[55], &Dest[56]);
  ByteToDword((uint32_t *)&Source->STATUS, &Dest[57], &Dest[58], &Dest[59]);
  Source->InverseSensorInput = Dest[60];
}
/************************************************************************/
void ByteTo7Bit(uint8_t *Source, uint8_t *Dest1, uint8_t *Dest2) {
  *Dest1 = *Source & 0x7F;
  *Dest2 = (*Source >> 7);
};
/************************************************************************/
void Bit7ToByte(uint8_t *Source, uint8_t *Dest1, uint8_t *Dest2) {
  *Source = *Dest1 & 0x7F | (*Dest2 << 7);
};
/******************************************************************************/
void ByteToDword(uint32_t *Destination, uint8_t *Source1, uint8_t *Source2,
                 uint8_t *Source3) {
  *Destination = ((uint32_t)*Source1 & 0x7F) |
                 (((uint32_t)*Source2 & 0x7F) << 7) |
                 (((uint32_t)*Source3 & 0x7F) << 14);
};

/******************************************************************************/
void DwordToByte(uint32_t *Source, uint8_t *B1, uint8_t *B2, uint8_t *B3) {

  *B1 = *Source & 0x7f;
  *B2 = *Source >> 7 & 0x7f;
  *B3 = *Source >> 14 & 0x7f;
}
/************************************************************************/
void TraceInfo_cSPIN_RegsStruct(cSPIN_RegsStruct_TypeDef _DevRegs) {

  printf("ABS_POS= %i\nEL_POS= %i\nMARK= %i\nSPEED= %i\nACC= %i\nDEC= "
         "%i\nMAX_SPEED= %i\nMIN_SPEED= %i\nFS_SPD= %i\nTVAL_HOLD= "
         "%i\nTVAL_RUN= %i\nTVAL_ACC= %i\nTVAL_DEC= %i\nT_FAST= 0x%x\nTON_MIN= "
         "%i\nTOFF_MIN = %i\nADC_OUT= %i\nOCD_TH= %i\nSTEP_MODE= %i\nALARM_EN= "
         "%i\nGATECFG1= %i\nGATECFG2= %i\nCONFIG= 0x%x\nSTATUS= %i\n",
         _DevRegs.ABS_POS, _DevRegs.EL_POS, _DevRegs.MARK, _DevRegs.SPEED,
         _DevRegs.ACC, _DevRegs.DEC, _DevRegs.MAX_SPEED, _DevRegs.MIN_SPEED,
         _DevRegs.FS_SPD, _DevRegs.TVAL_HOLD, _DevRegs.TVAL_RUN,
         _DevRegs.TVAL_ACC, _DevRegs.TVAL_DEC, _DevRegs.T_FAST,
         _DevRegs.TON_MIN, _DevRegs.TOFF_MIN, _DevRegs.ADC_OUT, _DevRegs.OCD_TH,
         _DevRegs.STEP_MODE, _DevRegs.ALARM_EN, _DevRegs.GATECFG1,
         _DevRegs.GATECFG2, _DevRegs.CONFIG, _DevRegs.STATUS);
}
/************************************************************************/
