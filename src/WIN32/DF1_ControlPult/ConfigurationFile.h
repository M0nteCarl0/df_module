#pragma once
#include  "SteperDefinition.h"
#include  "ConfigParcer.h"
class ConfigurationFile:public ConfigParcer
{
public:
    ConfigurationFile(void);
    ~ConfigurationFile(void);
    bool Load(const char* FileName);
    bool Save(const char* FileName);
    cSPIN_RegsStruct_TypeDef Registers; 
 
};

