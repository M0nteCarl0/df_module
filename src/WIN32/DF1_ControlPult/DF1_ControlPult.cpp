// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "stdafx.h"
#include "DF1_Module.h"
#include "ConfigurationFile.h"
#include "HiRessTimeMeasure.h"

BYTE Name[4];
BYTE HW[2];
BYTE SW[2];

DF1_Module DF;
ConfigurationFile Config;
HiRessTimeMeasure Time;


void DecodeStatusWordInFile(int* Word)
{

 SYSTEMTIME TimeS;       
 GetLocalTime(&TimeS);
char _Name[255];
memset(_Name,0,255);
sprintf(_Name,"StateWordDecod_%i_%i_%i_%i_%i_%i_%i.log",TimeS.wYear, TimeS.wMonth, TimeS.wDay, TimeS.wHour, TimeS.wMinute, TimeS.wSecond, TimeS.wMilliseconds);
FILE* Protocol = fopen(_Name,"w+");


 fprintf(Protocol,"RAW Value: %x\n",*Word);
 
 int State = *Word;
 int MotorMotionState  = (State >> 5) & 0x3;

 switch(MotorMotionState)
 {

    case 0:
    {
       fprintf(Protocol,"Motor stoped\n");
       break;
    }

      case 1:
    {
       fprintf(Protocol,"Motor acceletion\n");
       break;
    }


     case 2:
    {
       fprintf(Protocol,"Motor deceleration\n");
       break;
    }

    case 3:
    {
       fprintf(Protocol,"Motor constant speed motion\n");
       break;
    }


 }


 if( State &  1 << 0)
 {
     fprintf(Protocol,"Motor in Hiz State\n");

 }
 else
 {

   fprintf(Protocol,"Motor in Normal State\n");

 }

 if( State &  1 << 1)
 {
    fprintf(Protocol,"Motor is Busy\n");
 }
 else
 {
     fprintf(Protocol,"Motor is not Busy\n");
 }

 if( State &  1 << 8)
 {
    fprintf(Protocol,"WRONG CMD Bit High\n");
 }
 else
 {
    fprintf(Protocol,"WRONG CMD Bit Low \n");
 }


 if( State &  1 << 7)
 {
    fprintf(Protocol,"NOT PERF CMD Bit High\n");
 }
 else
 {
    fprintf(Protocol,"NOT PERF CMD Bit Low \n");
 }


 if( State &  1 << 2)
 {
    fprintf(Protocol,"Motor is SWF State Active\n");
 }
 else
 {
    fprintf(Protocol,"Motor is SWF State Active Reset\n");
 }

 if( State &  1 << 3)
 {
    fprintf(Protocol,"Motor is SW_EVN State Active\n");
 }
 else
 {
    fprintf(Protocol,"Motor is SW_EVN Active Reset\n");
 }

 if( State &  1 << 4)
 {
    fprintf(Protocol,"Direction Move is Forward\n");
 }
 else
 {
    fprintf(Protocol,"Direction Move is Backward\n");
 }

 if( State &  1 << 9)
 {
    fprintf(Protocol,"UVLO State Normal\n");
 }
 else
 {
    fprintf(Protocol,"UVLO Fault detected\n");
 }

 if( State &  1 <<10)
 {
    fprintf(Protocol,"TH_WRN State Normal\n");
 }
 else
 {
    fprintf(Protocol,"TH_WRN  Fault detected\n");
 }

 if( State &  1 <<11)
 {
    fprintf(Protocol,"TH_SD State Normal\n");
 }
 else
 {
    fprintf(Protocol,"TH_SD  Fault detected\n");
 }

 if( State &  1 <<12)
 {
    fprintf(Protocol,"OCD State Normal\n");
 }
 else
 {
    fprintf(Protocol,"OCD Fault detected\n");
 }

 if( State &  1 <<19)
 {
    fprintf(Protocol,"WS bit Trigered: Comand execution Error\n");
 }
 else
 {
    fprintf(Protocol,"Comand sucesfuly started\n");
 }



 if( State &  1 <<20)
 {
    fprintf(Protocol,"System recovery after fatal error\n");
 }
 else
 {
     fprintf(Protocol,"System normal booted\n");
 }



  if( State &  1 <<16)
 {
    fprintf(Protocol,"XP4 Trigered Motor on Sensor 1 \n");
 }
 
 if( State &  1 <<17)
 {
    fprintf(Protocol,"XP5 Trigered Motor on Sensor 2 \n");
 }

 fclose(Protocol);
}



void DecodeControlWord(int* ControlWord)
{

    int LControl = *ControlWord;
    int Direction;
    int ModeMotor = LControl >> 3 & 0x3;
    printf("\n");
    printf("  - - - Control Word Section Begin  - - - \n");
    printf("RAW Control Word:%x\n",LControl);
    if(LControl & 0x1)
    {

     printf("Backward Direction Motion\n");

    }
    else
    {

      printf("Forward Direction Motion\n");

    }
    switch(ModeMotor)
    {
        case 0:
        {
            printf("Mode Free Run\n");
            break;

        }

         case 1:
        {
            printf("Drive to Sensor 1 and Stop\n");
            break;

        }

          case 2:
        {
            printf("Drive to Sensor 2 and Stop\n");
            break;

        }

    
    }

    printf("  - - - Control Word Section End  - - - \n");
    printf("\n");

}



void DecodeStatusWord(int* Word)
{

 printf("\n");
 printf("  - - - State Word Section Begin  - - - \n");

 printf("RAW Value: %x\n",*Word);
 
 int State = *Word;
 int MotorMotionState  = (State >> 5) & 0x3;

 switch(MotorMotionState)
 {

    case 0:
    {
       printf("Motor stoped\n");
       break;
    }

      case 1:
    {
       printf("Motor acceletion\n");
       break;
    }


     case 2:
    {
       printf("Motor deceleration\n");
       break;
    }

    case 3:
    {
       printf("Motor constant speed motion\n");
       break;
    }


 }


 if( State &  1 << 0)
 {
     printf("Motor in Hiz State\n");

 }
 else
 {

   printf("Motor in Normal State\n");

 }

 if( State &  1 << 1)
 {
    printf("Motor is Busy\n");
 }
 else
 {
     printf("Motor is not Busy\n");
 }



 if( State &  1 << 8)
 {
    printf("WRONG CMD Bit High\n");
 }
 else
 {
    printf("WRONG CMD Bit Low \n");
 }


 if( State &  1 << 7)
 {
    printf("NOT PERF CMD Bit High\n");
 }
 else
 {
    printf("NOT PERF CMD Bit Low \n");
 }


 if( State &  1 << 2)
 {
    printf("Motor is SWF State Active\n");
 }
 else
 {
    printf("Motor is SWF State Active Reset\n");
 }

 if( State &  1 << 3)
 {
    printf("Motor is SW_EVN State Active\n");
 }
 else
 {
    printf("Motor is SW_EVN Active Reset\n");
 }

 if( State &  1 << 4)
 {
    printf("Direction Move is Forward\n");
 }
 else
 {
    printf("Direction Move is Backward\n");
 }

 if( State &  1 << 9)
 {
    printf("UVLO State Normal\n");
 }
 else
 {
    printf("UVLO Fault detected\n");
 }

 if( State &  1 <<10)
 {
    printf("TH_WRN State Normal\n");
 }
 else
 {
    printf("TH_WRN  Fault detected\n");
 }

 if( State &  1 <<11)
 {
    printf("TH_SD State Normal\n");
 }
 else
 {
    printf("TH_SD  Fault detected\n");
 }

 if( State &  1 <<12)
 {
    printf("OCD State Normal\n");
 }
 else
 {
    printf("OCD Fault detected\n");
 }

 if( State &  1 <<19)
 {
    printf("WS bit Trigered: Comand execution Error\n");
 }
 else
 {
    printf("Comand sucesfuly started\n");
 }



 if( State &  1 <<20)
 {
    printf("System recovery after fatal error\n");
 }
 else
 {
     printf("System normal booted\n");
 }



  if( State &  1 <<16)
 {
    printf("XP4 Trigered Motor on Sensor 1 \n");
 }
 
 if( State &  1 <<17)
 {
    printf("XP5 Trigered Motor on Sensor 2 \n");
 }



 printf("  - - - State Word Section End  - - - \n");
 printf("\n");
}





/****************************************************************************************/
bool PreaparationToRunModule(void)
{

bool Flag = true;

if(DF.m_FTDIisConected)
    {
        DF.ReadVersion(Name,HW,SW);
        printf("Name Module = %s\nSW:%i.%i\nHW:%i.%i\n",Name,SW[0],SW[1],HW[0],HW[1]);
        if(Config.Load("L6472.ini"))
        {
            cSPIN_RegsStruct_TypeDef RR;
           
            memset(&RR,0,sizeof(cSPIN_RegsStruct_TypeDef));
            printf("Read Regs Moduele\n");
            printf(" - - - - - -- - - -- - - - - - -- - - - - -\n");
            DF.ReadRegistersMotor(&RR);
            TraceInfo_cSPIN_RegsStruct(RR);
            printf(" - - - - - -- - - -- - - - - - -- - - - - -\n");
            if(RR.CONFIG != Config.Registers.CONFIG)
            {
                printf("Write Regs Moduele\n");
                printf(" - - - - - -- - - -- - - - - - -- - - - - -\n");
                DF.WriteRegistersMotor(&Config.Registers);
                TraceInfo_cSPIN_RegsStruct(Config.Registers);
                printf(" - - - - - -- - - -- - - - - - -- - - - - -\n");
                Sleep(5000);
                Flag = false;
            }
        }
        else
        {
          printf("Config not found!\n");

        }

    }

    return Flag;
}

/****************************************************************************************/
void SaveRegistersToFile(const char* FileName,cSPIN_RegsStruct_TypeDef  _DevRegs)
{

 FILE* Protocol = fopen(FileName,"w+");
 fprintf(Protocol, "ABS_POS= %i\nEL_POS= %i\nMARK= %i\nSPEED= %i\nACC= %i\nDEC= %i\nMAX_SPEED= %i\nMIN_SPEED= %i\nFS_SPD= %i\nTVAL_HOLD= %i\nTVAL_RUN= %i\nTVAL_ACC= %i\nTVAL_DEC= %i\nT_FAST= 0x%x\nTON_MIN= %i\nTOFF_MIN = %i\nADC_OUT= %i\nOCD_TH= %i\nSTEP_MODE= %i\nALARM_EN= %i\nGATECFG1= %i\nGATECFG2= %i\nCONFIG= 0x%x\nSTATUS= 0x%x\n",
  _DevRegs.ABS_POS,
  _DevRegs.EL_POS,
  _DevRegs. MARK,
  _DevRegs. SPEED,
  _DevRegs. ACC,
  _DevRegs.  DEC,
  _DevRegs.  MAX_SPEED,
  _DevRegs.  MIN_SPEED,
  _DevRegs.  FS_SPD,
  _DevRegs.  TVAL_HOLD,
  _DevRegs.  TVAL_RUN,
  _DevRegs.  TVAL_ACC,
  _DevRegs.  TVAL_DEC,
  _DevRegs.  T_FAST,
  _DevRegs.  TON_MIN,
  _DevRegs.  TOFF_MIN,
  _DevRegs.  ADC_OUT,
  _DevRegs.  OCD_TH,
  _DevRegs.  STEP_MODE,
  _DevRegs.  ALARM_EN,
  _DevRegs.  GATECFG1,
  _DevRegs.  GATECFG2,
  _DevRegs.  CONFIG,
  _DevRegs.  STATUS
   
   
);
   fflush(Protocol);
   fclose(Protocol);

}



bool WriteSpeedProfileEx(int* ControlWord,int* BeginSpeed,int* EndSpeed,int* TimeAcc,int* TimeStop,int* StepCount,int* StepMode)
{

 bool Flag = true;
 if(DF.m_FTDIisConected)
    {
        int StateWord       = 0;
        int MotorPosition   = 0;
        int SpeedMotor      = 0;

        int ControlWordR;
        int BeginSpeedR;
        int EndSpeedR;
        int TimeAccR;
        int TimeStopR;
        int StepCountR;
        int StepModeR;
        int ControlW = *ControlWord;
        float Acc = 0;
        float Dec = 0;
        cSPIN_RegsStruct_TypeDef RR;

        double g_CoenficinetMinSpeed = pow(10.0,6)/(pow(2.0,24) * 0.25);
        double g_CoenficinetSpeed    = pow(10.0,6)/(pow(2.0,28) * 0.25);

        int StepsBegin = *BeginSpeed/g_CoenficinetMinSpeed;
        int StepsEnd   = *EndSpeed/g_CoenficinetSpeed;

        if(TimeAcc > 0 && TimeStop > 0)
        {
             Acc = (float)(*EndSpeed - *BeginSpeed) / (float)(*TimeAcc/1000);
             Dec = (float) (*EndSpeed) / (float)(*TimeStop/1000);

            if(Acc < 14.55)
            {
                Acc = 14.55;
            }

             if(Dec < 14.55)
            {
                Dec = 14.55;
            }

        }
        else
        {
            Acc = Dec = *EndSpeed;

        }
        printf("Acceleration value in Steps/s %.3f\n",Acc);
        printf("Decceleration value in Steps/s %.3f\n",Dec);

        if(*BeginSpeed > 976 || *EndSpeed > 15625)
        {

         printf("Error in Input Parametrs!\n  Begin Speed Motor or End Speeed Motor is more than Limit!\n");
         system("pause");
         exit(0);

        }


        DF.ReadStateDrive(&StateWord,&MotorPosition,&SpeedMotor);
        DecodeStatusWord(&StateWord);
        SYSTEMTIME TimeS;
        
        GetLocalTime(&TimeS);
        char _Name[255];
        memset(_Name,0,255);
        sprintf(_Name,"Speed_Profile_%i_%i_%i_%i_%i_%i_%i.log",TimeS.wYear, TimeS.wMonth, TimeS.wDay, TimeS.wHour, TimeS.wMinute, TimeS.wSecond, TimeS.wMilliseconds);
        FILE* Protocol = fopen(_Name,"w+");
        DF.ReadVersion(Name,HW,SW);
        printf("Name Module = %s\nSW:%i.%i\nHW:%i.%i\n",Name,SW[0],SW[1],HW[0],HW[1]);
        DF.ReadRegistersMotor(&RR);
        
        GetLocalTime(&TimeS);
        memset(_Name,0,255);
        sprintf(_Name,"Initial_Registers_%i_%i_%i_%i_%i_%i_%i.log",TimeS.wYear, TimeS.wMonth, TimeS.wDay, TimeS.wHour, TimeS.wMinute, TimeS.wSecond, TimeS.wMilliseconds);
        SaveRegistersToFile((const char*)_Name,RR);

         fprintf(Protocol,"Control Word:%x\nSpeed Begin:%i\nSpeed End:%i\nStepCount:%i\nStepMode:%x\n",ControlW,*BeginSpeed,*EndSpeed,*StepCount,*StepMode);

         fprintf(Protocol,"- - Intial Registers begin - -\n");
         DF.ReadStateDrive(&StateWord,&MotorPosition,&SpeedMotor);
         fprintf(Protocol,"State Word:%x  Speed:%.2f  Motor Position:%i\n",StateWord,SpeedMotor * g_CoenficinetSpeed,MotorPosition);
         fprintf(Protocol,"- - Intial Registers end- -\n");
         //fflush(Protocol);

         Time.Start();
         
        int Erc = DF.WriteSpeedProfileRun(&ControlW, &StepsBegin,&StepsEnd,TimeAcc,TimeStop,StepCount,StepMode);
        if( Erc == 0)
        {
            DF.ReadSpeedProfileRun(&ControlWordR, &BeginSpeedR,&EndSpeedR,&TimeAccR,&TimeStopR,&StepCountR,&StepModeR);
            if((ControlWordR == ControlW) &&  (BeginSpeedR == StepsBegin)  && (StepsEnd == EndSpeedR)
               && (TimeAccR == *TimeAcc)   && (*TimeStop == TimeStopR  && (*StepCount == StepCountR) &&  *StepMode  == StepModeR))
               {
                    printf("Write Params Sucessufuly\n");

               }
               else
               {
                    printf("Error When Write params\n");

               }

               if(ControlW & 0x40)
               {
                 printf("Control Word  WS Fault Bit  Detected\n");
                 DF.ReadStateDrive(&StateWord,&MotorPosition,&SpeedMotor);
                 fprintf(Protocol,"State Word:%x  Speed:%.2f  Motor Position:%i\n",StateWord,SpeedMotor * g_CoenficinetSpeed,MotorPosition);
                 memset(&RR,0,sizeof(cSPIN_RegsStruct_TypeDef));
                 printf("Control word value before write:0x%x. Control word value aftr write:0x%x\n",*ControlWord,ControlW);
                 printf("Read Regs Moduele\n");
                 printf(" - - - - - -- - - -- - - - - - -- - - - - -\n");
                 DF.ReadRegistersMotor(&RR);
                 TraceInfo_cSPIN_RegsStruct(RR);
                 printf(" - - - - - -- - - -- - - - - - -- - - - - -\n");
                 DecodeStatusWord(&StateWord);
                 DecodeStatusWordInFile(&StateWord);
                 *ControlWord = ControlW;
                 Flag = false;
                 getchar();
                 fclose(Protocol);
                 return Flag;

               }
                    
            printf("Run Procedure");
            int MotorState =  1;
		
            do
            {

            printf("Input INFO\n");

            printf("Acceleration value in Steps/s %.3f\n",Acc);
            printf("Decceleration value in Steps/s %.3f\n",Dec);

            printf("Data from bat\n");
            printf("Control Word:%x\n Speed Begin: %i Steps/s\n Speed End: %i Steps/s\n Time Acc:%i ms\n Time Brake:%i ms\n StepCount:%i Full Steps Step\n StepMode :%x\n",* ControlWord,*BeginSpeed,* EndSpeed,*TimeAcc,* TimeStop,*StepCount,*StepMode);;
            printf("  - - - - - - - - - - - - - ----- - - - - --  - - - - - --  -\n");

            printf("Data in module \n");
            printf("Control Word:%x\n Speed Begin:%i Tiks Driver\n Speed End: %i Tiks Driver\n Time Acc:%i ms\n Time Brake:%i ms\n StepCount:%i Full Steps Step\n StepMode :%x\n",* ControlWord,StepsBegin,StepsEnd,*TimeAcc,* TimeStop,*StepCount,*StepMode);;
            printf("  - - - - - - - - - - - - - ----- - - - - --  - - - - - --  -\n");
          

           
 
            //printf("Read Regs Moduele\n");
            //printf(" - - - - - -- - - -- - - - - - -- - - - - -\n");
           /// DF.ReadRegistersMotor(&RR);
            //TraceInfo_cSPIN_RegsStruct(RR);
            printf(" - - - - - -- - - -- - - - - - -- - - - - -\n");


            DF.ReadStateDrive(&StateWord,&MotorPosition,&SpeedMotor);
            MotorState =  (StateWord >> 5) & 0x3;
            fprintf(Protocol,"State Word:%x  Speed:%.2f  Motor Position:%i\n",StateWord,SpeedMotor * g_CoenficinetSpeed,MotorPosition);
            //fflush(Protocol);
            printf("Motor Speed:%.3f\nMotor Position:%i\n",SpeedMotor * g_CoenficinetSpeed , MotorPosition);
            DecodeStatusWord(&StateWord);
            DecodeControlWord(&ControlW);
           
            printf("  - - - - - - - - - - - - - ----- - - - - --  - - - - - --  -\n");
			if(StateWord & 1 <<19)
			{
			   break;
			}
            //Sleep(5000);

           bool  OCD        =  (StateWord >> 12) & 0x1; 
           bool  TH_SD      =  (StateWord >> 11) & 0x1;
           bool  TH_WRN     =  (StateWord >> 10) & 0x1;
           bool  UVLO       =  (StateWord >> 9) & 0x1;
           bool  WRONG_CMD  =  (StateWord >>8)  & 0x1;
           bool  NOPERF_CMD  =  (StateWord >> 7) & 0x1;
    

         if( !OCD  ||  !TH_SD  || !TH_WRN || !UVLO  || WRONG_CMD || NOPERF_CMD )
         {

            memset(&RR,0,sizeof(cSPIN_RegsStruct_TypeDef));
            DF.ReadRegistersMotor(&RR);
            printf("Fatal Fault detected! \n");
            DecodeStatusWord(&StateWord);
            DecodeControlWord(&ControlW);
            GetLocalTime(&TimeS);
            memset(_Name,0,255);
            sprintf(_Name,"Fault_Registers_%i_%i_%i_%i_%i_%i_%i.log",TimeS.wYear, TimeS.wMonth, TimeS.wDay, TimeS.wHour, TimeS.wMinute, TimeS.wSecond, TimeS.wMilliseconds);
            SaveRegistersToFile((const char*)_Name,RR);
            getchar();
         }
 
            }while(MotorState!=0);
            Time.End();
            DF.ReadRegistersMotor(&RR);
            GetLocalTime(&TimeS);
            memset(_Name,0,255);
            sprintf(_Name,"Registers_After_Stops_%i_%i_%i_%i_%i_%i_%i.log",TimeS.wYear, TimeS.wMonth, TimeS.wDay, TimeS.wHour, TimeS.wMinute, TimeS.wSecond, TimeS.wMilliseconds);
            SaveRegistersToFile((const char*)_Name,RR);
            fprintf(Protocol,"- - Registers after stop begin - -\n");
            DF.ReadStateDrive(&StateWord,&MotorPosition,&SpeedMotor);
            fprintf(Protocol,"State Word:%x  Speed:%.2f  Motor Position:%i\n",StateWord,SpeedMotor * g_CoenficinetSpeed,MotorPosition);
            fprintf(Protocol,"- - Registers after stop end - -\n");
            fflush(Protocol);
            fflush(Protocol);
            fclose(Protocol);
            printf("Time Motion %.3f\n",Time.GetEllapsedSeconds());


        }
        else
        {
          printf("Mesage Error Transaction with code %i\n",Erc);
         // Flag = false;

        ;}


    }
    return Flag;
}





void WriteProfileAndRun(int* ControlWord,int* BeginSpeed,int* EndSpeed,int* TimeAcc,int* TimeStop,int* StepCount,int* StepMode)
{

  if(DF.m_FTDIisConected)
    {
        int StateWord       = 0;
        int MotorPosition   = 0;
        int SpeedMotor      = 0;

        int ControlWordR;
        int BeginSpeedR;
        int EndSpeedR;
        int TimeAccR;
        int TimeStopR;
        int StepCountR;
        int StepModeR;
        int ControlW = *ControlWord;
        float Acc = 0;
        float Dec = 0;
        cSPIN_RegsStruct_TypeDef RR;

        double g_CoenficinetMinSpeed = pow(10.0,6)/(pow(2.0,24) * 0.25);
        double g_CoenficinetSpeed    = pow(10.0,6)/(pow(2.0,28) * 0.25);

        int StepsBegin = *BeginSpeed/g_CoenficinetMinSpeed;
        int StepsEnd   = *EndSpeed/g_CoenficinetSpeed;

        if(TimeAcc > 0 && TimeStop > 0)
        {
             Acc = (float)(*EndSpeed - *BeginSpeed) / (float)(*TimeAcc/1000);
             Dec = (float) (*EndSpeed) / (float)(*TimeStop/1000);

            if(Acc < 14.55)
            {
                Acc = 14.55;
            }

             if(Dec < 14.55)
            {
                Dec = 14.55;
            }

        }
        else
        {
            Acc = Dec = *EndSpeed;

        }
        printf("Acceleration value in Steps/s %.3f\n",Acc);
        printf("Decceleration value in Steps/s %.3f\n",Dec);

        if(*BeginSpeed > 976 || *EndSpeed > 15625)
        {

         printf("Error in Input Parametrs!\n  Begin Speed Motor or End Speeed Motor is more than Limit!\n");
         system("pause");
         exit(0);

        }


        DF.ReadStateDrive(&StateWord,&MotorPosition,&SpeedMotor);
        DecodeStatusWord(&StateWord);
        getchar();

        FILE* Protocol = fopen("Speed_Profile.log","w+");
        DF.ReadVersion(Name,HW,SW);
        printf("Name Module = %s\nSW:%i.%i\nHW:%i.%i\n",Name,SW[0],SW[1],HW[0],HW[1]);
        DF.ReadRegistersMotor(&RR);
        SaveRegistersToFile("Initial_Registers.log",RR);

         fprintf(Protocol,"Control Word:%x\nSpeed Begin:%i\nSpeed End:%i\nStepCount:%i\nStepMode:%x\n",ControlW,*BeginSpeed,*EndSpeed,*StepCount,*StepMode);

         fprintf(Protocol,"- - Intial Registers begin - -\n");
         DF.ReadStateDrive(&StateWord,&MotorPosition,&SpeedMotor);
         fprintf(Protocol,"State Word:%x  Speed:%.2f  Motor Position:%i\n",StateWord,SpeedMotor * g_CoenficinetSpeed,MotorPosition);
         fprintf(Protocol,"- - Intial Registers end- -\n");
         fflush(Protocol);

         Time.Start();
         
        int Erc = DF.WriteSpeedProfileRun(&ControlW, &StepsBegin,&StepsEnd,TimeAcc,TimeStop,StepCount,StepMode);
        if( Erc == 0)
        {
            DF.ReadSpeedProfileRun(&ControlWordR, &BeginSpeedR,&EndSpeedR,&TimeAccR,&TimeStopR,&StepCountR,&StepModeR);
            if((ControlWordR == ControlW) &&  (BeginSpeedR == StepsBegin)  && (StepsEnd == EndSpeedR)
               && (TimeAccR == *TimeAcc)   && (*TimeStop == TimeStopR  && (*StepCount == StepCountR) &&  *StepMode  == StepModeR))
               {
                    printf("Write Params Sucessufuly\n");

               }
               else
               {
                    printf("Error When Write params\n");

               }

               if(ControlW & 0x40)
               {
                 printf("Control Word  WS Fault Bit  Detected\n");
                 DF.ReadStateDrive(&StateWord,&MotorPosition,&SpeedMotor);
                
                 memset(&RR,0,sizeof(cSPIN_RegsStruct_TypeDef));
                 
                 printf("Read Regs Moduele\n");
                 printf(" - - - - - -- - - -- - - - - - -- - - - - -\n");
                 DF.ReadRegistersMotor(&RR);
                 TraceInfo_cSPIN_RegsStruct(RR);
                 printf(" - - - - - -- - - -- - - - - - -- - - - - -\n");
                 DecodeStatusWord(&StateWord);
                
                 system("pause");
                 exit(0);


               }
                    
            printf("Run Procedure");
            int MotorState =  1;
		
            do
            {

            printf("Input INFO\n");

            printf("Acceleration value in Steps/s %.3f\n",Acc);
            printf("Decceleration value in Steps/s %.3f\n",Dec);

            printf("Data from bat\n");
            printf("Control Word:%x\n Speed Begin: %i Steps/s\n Speed End: %i Steps/s\n Time Acc:%i ms\n Time Brake:%i ms\n StepCount:%i Full Steps Step\n StepMode :%x\n",* ControlWord,*BeginSpeed,* EndSpeed,*TimeAcc,* TimeStop,*StepCount,*StepMode);;
            printf("  - - - - - - - - - - - - - ----- - - - - --  - - - - - --  -\n");

            printf("Data in module \n");
            printf("Control Word:%x\n Speed Begin:%i Tiks Driver\n Speed End: %i Tiks Driver\n Time Acc:%i ms\n Time Brake:%i ms\n StepCount:%i Full Steps Step\n StepMode :%x\n",* ControlWord,StepsBegin,StepsEnd,*TimeAcc,* TimeStop,*StepCount,*StepMode);;
            printf("  - - - - - - - - - - - - - ----- - - - - --  - - - - - --  -\n");
          

            memset(&RR,0,sizeof(cSPIN_RegsStruct_TypeDef));
 
            printf("Read Regs Moduele\n");
            printf(" - - - - - -- - - -- - - - - - -- - - - - -\n");
            DF.ReadRegistersMotor(&RR);
            TraceInfo_cSPIN_RegsStruct(RR);
            printf(" - - - - - -- - - -- - - - - - -- - - - - -\n");


            DF.ReadStateDrive(&StateWord,&MotorPosition,&SpeedMotor);
            fprintf(Protocol,"State Word:%x  Speed:%.2f  Motor Position:%i\n",StateWord,SpeedMotor * g_CoenficinetSpeed,MotorPosition);
            fflush(Protocol);
            printf("Motor Speed:%.3f\nMotor Position:%i\n",SpeedMotor * g_CoenficinetSpeed , MotorPosition);
            DecodeStatusWord(&StateWord);
            DecodeControlWord(&ControlW);
            MotorState =  (StateWord >> 5) & 0x3;
            printf("  - - - - - - - - - - - - - ----- - - - - --  - - - - - --  -\n");
			if(StateWord & 1 <<19)
			{
			   break;
			}
            //Sleep(5000);

            }while(MotorState!=0);
            Time.End();
            DF.ReadRegistersMotor(&RR);
            SaveRegistersToFile("Registers_After_Stop.log",RR);
            fprintf(Protocol,"- - Registers after stop begin - -\n");
            DF.ReadStateDrive(&StateWord,&MotorPosition,&SpeedMotor);
            fprintf(Protocol,"State Word:%x  Speed:%.2f  Motor Position:%i\n",StateWord,SpeedMotor * g_CoenficinetSpeed,MotorPosition);
            fprintf(Protocol,"- - Registers after stop end - -\n");
            fflush(Protocol);
            fflush(Protocol);
            fclose(Protocol);
            printf("Time Motion %.3f\n",Time.GetEllapsedSeconds());


        }
        else
        {
          printf("Mesage Error Transaction with code %i\n",Erc);

        ;}


    }

}


void  mainModeTestSeq(int* ControlWord,int* BeginSpeed,int* EndSpeed,int* TimeAcc,int* TimeStop,int* StepCount,int* StepMode,int* NumCycles,int* DellayTime)
{


    for(int i = 0;i<*NumCycles;i++)
    {
    
         printf("C:%o\n");
         WriteProfileAndRun(ControlWord, BeginSpeed,EndSpeed,TimeAcc, TimeStop, StepCount, StepMode);
         printf("C:%o\n");
         Sleep(*DellayTime);
    
    }



}

/*******************************************************************************************************************************************/

void  GoToSemsor(int Dir,int* Sensor,int* BeginSpeed,int* EndSpeed,int* TimeAcc,int* TimeStop,int* StepCount,int* StepMode)

{




}


void WriteRegistersModule(const char* PathToConfig)
{

 if(DF.m_FTDIisConected)
    {
        DF.ReadVersion(Name,HW,SW);
        printf("Name Module = %s\nSW:%i.%i\nHW:%i.%i\n",Name,SW[0],SW[1],HW[0],HW[1]);
        if(Config.Load(PathToConfig))
        {
            cSPIN_RegsStruct_TypeDef RR;
            memset(&RR,0,sizeof(cSPIN_RegsStruct_TypeDef));
            printf("Write Regs Moduele\n");
            printf(" - - - - - -- - - -- - - - - - -- - - - - -\n");
            DF.WriteRegistersMotor(&Config.Registers);
            TraceInfo_cSPIN_RegsStruct(Config.Registers);
            printf(" - - - - - -- - - -- - - - - - -- - - - - -\n");
            Sleep(5000);

            printf("Read Regs Moduele\n");
            printf(" - - - - - -- - - -- - - - - - -- - - - - -\n");
            DF.ReadRegistersMotor(&RR);
            TraceInfo_cSPIN_RegsStruct(RR);
            printf(" - - - - - -- - - -- - - - - - -- - - - - -\n");

        }
        else
        {
          printf("Config not found!\n");

        }



    }




}


void StopMotor(void)
{

    if(DF.m_FTDIisConected)
    {
        DF.ReadVersion(Name,HW,SW);
        printf("Name Module = %s\nSW:%i.%i\nHW:%i.%i\n",Name,SW[0],SW[1],HW[0],HW[1]);
        DF.SoftStopMotor();
        printf("StopProcedureBegin");
    }


}





void StressTestModule(int* DirMove,int* BeginSpeed,int* EndSpeedBegin,int* EndSpeedEnd,int* TimeAcc,int* TimeStop,
                      int* Sensor,int* BeginSpeedS,int* EndSpeedS,int* TimeAccS,int* TimeStopS,int* StepMode,int* StepCountM,int* LoopCounts,int* DellayMs)
{

     SYSTEMTIME Time;
    //Sensor 0 = XP4 Sensor 1  = XP5  DirMove  0  = Forward  DirMove  1  = Backward
    int DirectionControlWord = 0;
    int StateWord           = 0;
    int MotorPosition       = 0;
    int SpeedMotor          = 0; 
    int SensorErrorCount    = 0;
    int SpeedInc = 0 ;
    int ControlMotion = 0;
    int SensorControlWord = 0;
    int PositioningErrorCount = 0;
    int BrokenConfigurationCounts = 0; 

    if(*DirMove == 0)
    {
        ControlMotion = 0;
    }

    if(*DirMove == 1)
    {
        ControlMotion = 1;

    }

    if(*DirMove != 1 || *DirMove != 0)
    {
        ControlMotion = 0;
        *DirMove  = 0;
    }


    if(*EndSpeedEnd != 0 && *EndSpeedEnd!=0 &&  *LoopCounts >1)
    {
        SpeedInc = (*EndSpeedEnd - *EndSpeedBegin)/ (*LoopCounts - 1 );
    }
    int StepCountSensor =  21000;
    int EndSpeedM = *EndSpeedBegin;
    GetLocalTime(&Time);
    char _Name[255];
    int RSErrorCode = 0;
    memset(_Name,0,255);
    sprintf(_Name,"StressTest_%i_%i_%i_%i_%i_%i_%i.log",Time.wYear, Time.wMonth, Time.wDay, Time.wHour, Time.wMinute, Time.wSecond, Time.wMilliseconds);
    FILE* SensorTRace = fopen(_Name,"w+");
    fprintf(SensorTRace," - - Programm options - - \n");
    fprintf(SensorTRace,"Direction move:%i\n",*DirMove);
    fprintf(SensorTRace,"Begin Speed Main motion:%i Steps/s\n",*BeginSpeed);
    fprintf(SensorTRace,"End Speed initial Main motion:%i Steps/s\n",*EndSpeedBegin);
    fprintf(SensorTRace,"End Speed Final Main motion:%i Steps/s\n",*EndSpeedEnd);
    fprintf(SensorTRace,"Time Acc Main motion:%i ms\n ",*TimeAcc);
    fprintf(SensorTRace,"Time Stop Main motion:%i ms\n ",*TimeStop);

    fprintf(SensorTRace,"Binding Sensor:%i  ",*Sensor);
    fprintf(SensorTRace,"Begin Speed Motion to sensor:%i Steps/s\n",*BeginSpeedS);
    fprintf(SensorTRace,"End Speed initial Motion to sensor:%i Steps/s\n",*EndSpeedS);
    fprintf(SensorTRace,"Time Acc Motion to sensor:%i ms\n ",*TimeAccS);
    fprintf(SensorTRace,"Time Stop Motion to sensor:%i ms\n ",*TimeStopS);
    fprintf(SensorTRace,"Step Mode:0x%x\n ",*StepMode);
    fprintf(SensorTRace,"Step count:%i\n ",*StepCountM);
    fprintf(SensorTRace,"Loop count:%i\n ",*LoopCounts);
    fprintf(SensorTRace,"Dellay beetwena end motions:%i ms\n ",*TimeStopS);
    fprintf(SensorTRace,"SpeedInc:%i\n",SpeedInc);
   

    if(ControlMotion == 1 && *Sensor  == 0)
    {
       SensorControlWord = 0x2;
    }

      if( ControlMotion == 0 && *Sensor  == 0)
    {
      SensorControlWord =  0x3;

    }

      if(ControlMotion == 1 && *Sensor  == 1)
    {
         SensorControlWord = 0x4;
    }

      if(ControlMotion == 0 && *Sensor  == 1)
    {
         SensorControlWord = 0x5;
    }

    int Sensor1State = 0;
    int Sensor2State = 0;
    bool MotorAlreadyPositione  = false;
    for(int i = 0;i<*LoopCounts;i++)
    {
        MotorAlreadyPositione = false;
        GetLocalTime(&Time);

        if(!PreaparationToRunModule())
        {
            printf("Load Configuration fall!\n");
            GetLocalTime(&Time);
            RSErrorCode =  DF.ReadStateDrive(&StateWord,&MotorPosition,&SpeedMotor);
            fprintf(SensorTRace,"[%i/%i/%i %i:%i:%i:%03i] Error on writing registers to module StateWord:0x%x,Motor Postion:%i\n ",Time.wYear, Time.wMonth, Time.wDay, Time.wHour, Time.wMinute, Time.wSecond, Time.wMilliseconds,SensorControlWord,StateWord,MotorPosition);
            DecodeStatusWordInFile(&StateWord);
            BrokenConfigurationCounts++;
            getchar(); 
        }


        RSErrorCode =  DF.ReadStateDrive(&StateWord,&MotorPosition,&SpeedMotor);
      
        Sensor1State = false;
        Sensor2State = false;

          if( StateWord &  1 <<16)
          {
 
             Sensor1State = true;
          }
        
 
         if( StateWord &  1 <<17)
         {
            Sensor2State = true;
         }

        if (((SensorControlWord == 0x2 || SensorControlWord == 0x3)  &&  Sensor1State) ||
            ((SensorControlWord == 0x4 || SensorControlWord == 0x5)  &&  Sensor2State))
            {
                GetLocalTime(&Time);
                fprintf(SensorTRace,"[%i/%i/%i %i:%i:%i:%03i] SensorMotion_Skiped,Control Word:0x%x,StateWord:0x%x,Motor Postion:%i,Speed begin:%i,Speed End:%i. Motor already located on optical sensor!\n ",Time.wYear, Time.wMonth, Time.wDay, Time.wHour, Time.wMinute, Time.wSecond, Time.wMilliseconds,SensorControlWord,StateWord,MotorPosition,*BeginSpeedS,*EndSpeedS);
                MotorAlreadyPositione = true;
                DecodeStatusWordInFile(&StateWord);
            }
            else
            {
              GetLocalTime(&Time);
              fprintf(SensorTRace,"[%i/%i/%i %i:%i:%i:%03i] SensorMotion_Start,Control Word:0x%x,StateWord:0x%x,Motor Postion:%i,Speed begin:%i,Speed End:%i\n ",Time.wYear, Time.wMonth, Time.wDay, Time.wHour, Time.wMinute, Time.wSecond, Time.wMilliseconds,SensorControlWord,StateWord,MotorPosition,*BeginSpeedS,*EndSpeedS);
              DecodeStatusWordInFile(&StateWord);

            }
            
            
            if(MotorAlreadyPositione == false)
            {
                if(!WriteSpeedProfileEx(&SensorControlWord, BeginSpeedS, EndSpeedS,TimeAcc, TimeStop, &StepCountSensor, StepMode) )   //�������� �� �������� � ��������������� ����������� ��������
                 {
                     RSErrorCode = DF.ReadStateDrive(&StateWord,&MotorPosition,&SpeedMotor);
                     PositioningErrorCount++;
                     GetLocalTime(&Time);
                     fprintf(SensorTRace,"[%i/%i/%i %i:%i:%i:%03i] WS bit trigger detected! StateWordvalue:0x%x\n",Time.wYear, Time.wMonth, Time.wDay, Time.wHour, Time.wMinute, Time.wSecond, Time.wMilliseconds,StateWord);
                 }

           }
        GetLocalTime(&Time);
        RSErrorCode =   DF.ReadStateDrive(&StateWord,&MotorPosition,&SpeedMotor);
        DecodeStatusWordInFile(&StateWord);
        Sensor1State = (StateWord >> 16) & 0x1;
        Sensor2State = (StateWord >> 17) & 0x1;

        if((SensorControlWord == 0x2 || SensorControlWord == 0x3)   &&  !Sensor1State)
        {
        
            GetLocalTime(&Time);
            DecodeStatusWordInFile(&StateWord);
            fprintf(SensorTRace,"[%i/%i/%i %i:%i:%i:%03i] Motor Stall state detected when move to sensor1(XP4)! Motor not positioned!\n"  , Time.wYear, Time.wMonth, Time.wDay, Time.wHour, Time.wMinute, Time.wSecond, Time.wMilliseconds);

        }
        

        if((SensorControlWord == 0x4 || SensorControlWord == 0x5)  &&  !Sensor2State)
        {
            GetLocalTime(&Time);
            DecodeStatusWordInFile(&StateWord);
            fprintf(SensorTRace,"[%i/%i/%i %i:%i:%i:%03i] Motor Stall state detected when move to sensor2(XP5)! Motor not positioned!\n"  , Time.wYear, Time.wMonth, Time.wDay, Time.wHour, Time.wMinute, Time.wSecond, Time.wMilliseconds);
        }

       if(MotorAlreadyPositione == false)
       {
        GetLocalTime(&Time);
        DecodeStatusWordInFile(&StateWord);
        fprintf(SensorTRace,"[%i/%i/%i %i:%i:%i:%03i] SensorMotion_Stop,Control Word:0x%x,StateWord:0x%x,Motor Postion:%i\n", Time.wYear, Time.wMonth, Time.wDay, Time.wHour, Time.wMinute, Time.wSecond, Time.wMilliseconds,SensorControlWord,StateWord,MotorPosition);
       }
        Sleep(*DellayMs);

        if(i == *LoopCounts - 1)
        {
            EndSpeedM = *EndSpeedEnd;
        }

        GetLocalTime(&Time);
        fprintf(SensorTRace,"[%i/%i/%i %i:%i:%i:%i] MainMotion_Start,Control Word:0x%x,StateWord:0x%x,Motor Postion:%i,Motor Speed Begin %i,Motor Speed End  %i\n",Time.wYear, Time.wMonth, Time.wDay, Time.wHour, Time.wMinute, Time.wSecond, Time.wMilliseconds,ControlMotion,StateWord,MotorPosition,*BeginSpeed,EndSpeedM);

        WriteSpeedProfileEx(&ControlMotion,BeginSpeed, &EndSpeedM,TimeAcc, TimeStop, StepCountM, StepMode);// ��������  ���� �������� ������
        RSErrorCode =  DF.ReadStateDrive(&StateWord,&MotorPosition,&SpeedMotor);
        DecodeStatusWordInFile(&StateWord);
        GetLocalTime(&Time);
        fprintf(SensorTRace,"[%i/%i/%i %i:%i:%i:%i] MainMotion_Stop,Control Word:0x%x,StateWord:0x%x,Motor Postion:%i\n ",Time.wYear, Time.wMonth, Time.wDay, Time.wHour, Time.wMinute, Time.wSecond, Time.wMilliseconds,ControlMotion,StateWord,MotorPosition);
        
        Sleep(*DellayMs);
        EndSpeedM+=SpeedInc;
    
    }
    fprintf(SensorTRace,"WS Bit Error count:%i\n",PositioningErrorCount);
    fprintf(SensorTRace,"L6472 Registers W/R Errorcount:%i\n", BrokenConfigurationCounts);
    fflush(SensorTRace);
    fclose(SensorTRace);

}
/********************************************************************************************************************/





int _tmain(int argc, _TCHAR* argv[])
{

HANDLE PID = GetCurrentProcess();
SetProcessPriorityBoost(PID,true);
int  CID = 0;

    if(argc>= 2)
    {
    
         CID = atoi(argv[1]);

    switch(argc)
{


case 9:
{



 if(CID == 3)
{

    int ControlWord  = atoi(argv[2]);
    int BeginSpeed   = atoi(argv[3]);
    int EndSpeed     = atoi(argv[4]);
    int TimeAcc      = atoi(argv[5]);
    int TimeStop     = atoi(argv[6]);
    int StepCount    = atoi(argv[7]);
    int StepMode     = atoi(argv[8]);
    WriteProfileAndRun(&ControlWord,&BeginSpeed,&EndSpeed,&TimeAcc, &TimeStop,&StepCount,&StepMode);

};






case 17:
{



 if(CID == 4)
{

    int ControlWord    = atoi(argv[2]);
    int BeginSpeed     = atoi(argv[3]);
    int EndSpeedBegin  = atoi(argv[4]);
    int EndSpeedEnd    = atoi(argv[5]);
    int TimeAcc        = atoi(argv[6]);
    int TimeStop       = atoi(argv[7]);
 
    int Sensor        = atoi(argv[8]);;
    int BeginSpeedS   = atoi(argv[9]);
    int EndSpeedS     = atoi(argv[10]);
    int TimeAccS      = atoi(argv[11]);
    int TimeStopS     = atoi(argv[12]);
    int StepMode      = atoi(argv[13]);
    int StepCount     = atoi(argv[14]);
    int LoopCount     = atoi(argv[15]);
    int  Dellty       = atoi(argv[16]);


 StressTestModule(&ControlWord,&BeginSpeed,&EndSpeedBegin,&EndSpeedEnd,&TimeAcc,&TimeStop,
                      &Sensor,&BeginSpeedS,&EndSpeedS, &TimeAccS,&TimeStopS,&StepMode,&StepCount, &LoopCount,&Dellty);



};



break;
}




break;

}


    case 3:
    {  
          if(CID == 1)
        {
            WriteRegistersModule(argv[2]);
        
        }
    break;
    };


    case 2:
    {  
          if(CID == 2)
        {
            StopMotor();
        
        }
    break;
    };






}
}

 
getchar();
return 0;
}



