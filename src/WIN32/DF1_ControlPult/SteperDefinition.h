#include <stdint.h>
#pragma once
#ifndef __SDEF__
#define __SDEF__
#if 0
typedef struct cSPIN_RegsStruct_TypeDef
{
  uint32_t ABS_POS;//4(44);
  uint32_t MARK;//4
  uint32_t SPEED;//4

  uint16_t EL_POS;//4/(40)
  uint16_t RESERVED_3;
  uint16_t ACC;//2(28)
  uint16_t DEC;//2
  uint16_t MAX_SPEED;//2
  uint16_t MIN_SPEED;//2
  uint16_t FS_SPD;//2(21)
  uint16_t CONFIG;//2
  uint16_t STATUS;//2
  uint16_t GATECFG1;//2

  uint8_t  TVAL_HOLD;//1
  uint8_t  TVAL_RUN;//1
  uint8_t  TVAL_ACC;//1
  uint8_t  TVAL_DEC;//1(16)
  uint8_t  T_FAST;//1
  uint8_t  TON_MIN;//1
  uint8_t  TOFF_MIN;//1
  uint8_t  RESERVED_2;//1(11)
  uint8_t  ADC_OUT;//1
  uint8_t  OCD_TH;//1
  uint8_t  RESERVED_1;//0
  uint8_t  STEP_MODE;//1
  uint8_t  ALARM_EN;//1(7)
  uint8_t  GATECFG2;//1
  
}cSPIN_RegsStruct_TypeDef;
#endif
typedef struct
{
  uint32_t ABS_POS;
  uint16_t EL_POS;
  uint32_t MARK;
  uint32_t SPEED;
  uint16_t ACC;
  uint16_t DEC;
  uint16_t MAX_SPEED;
  uint16_t MIN_SPEED;
  uint16_t FS_SPD;
  uint8_t  TVAL_HOLD;
  uint8_t  TVAL_RUN;
  uint8_t  TVAL_ACC;
  uint8_t  TVAL_DEC;
  uint16_t RESERVED_3;
  uint8_t  T_FAST;
  uint8_t  TON_MIN;
  uint8_t  TOFF_MIN;
  uint8_t  RESERVED_2;
  uint8_t  ADC_OUT;
  uint8_t  OCD_TH;
  uint8_t  RESERVED_1;
  uint8_t  STEP_MODE;
  uint8_t  ALARM_EN;
  uint16_t GATECFG1;
  uint8_t  GATECFG2;
  uint16_t CONFIG;
  uint16_t STATUS;
  uint16_t CheckWord;
}cSPIN_RegsStruct_TypeDef;

#endif
