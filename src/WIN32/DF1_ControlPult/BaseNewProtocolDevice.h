#pragma once
#include "MXCOMM.H"

#define  NCOM   128
#define  mID    0x96
class BaseNewProtocolDevice
{
public:
    BaseNewProtocolDevice(void);
    BaseNewProtocolDevice(int _ModuleAdress);
    ~BaseNewProtocolDevice(void);
    void     InitDefaultCRCCheck(void);
    int InitComm(
                                    UINT  _nSpeed    = 115200,
                                    UINT  _nDataBits = 8, 
                                    UINT  _nParity   = 1, 
                                    UINT  _nStopBits = 2,
                                    int   _Timeout   = 120);


    void SetModuleAdress(int _ModuleAdress);
    int       ReadVersion      (BYTE *Name, BYTE *Hard, BYTE *Soft);
    BYTE     MakeCRC   (BYTE *Data, int N);
    void     AddCRC    (BYTE *Data, int N);
    bool     CheckCRC  (BYTE *Data, int N);
    int      CheckErrorWR (BYTE *DataS, int NS, BYTE *DataR, int NR);
    int      WriteCommand (BYTE *DataS, int NS);
    int      ReadCommand  (BYTE *DataS, int NS, BYTE *DataR, int NR);
    
    bool     m_FlgDebugS;
    bool     m_FlgDebugR;
    bool     m_FlgCheckCRCwrite;
    bool     m_FlgCheckCRCread;
    MxComm   m_Comm;
    BYTE     m_DataS [NCOM];
    BYTE     m_DataR [NCOM];
    int      m_ModuleAdress;
    bool     m_FTDIisConected;
    int      LastErrorCode;
};

