﻿========================================================================
    КОНСОЛЬНОЕ ПРИЛОЖЕНИЕ. Обзор проекта DF1_ControlPult
========================================================================

Это приложение DF1_ControlPult создано автоматически с помощью мастера 
приложений.

Здесь приведены краткие сведения о содержимом каждого из файлов, использованных 
при создании приложения DF1_ControlPult.


DF1_ControlPult.vcxproj
    Основной файл проекта VC++, автоматически создаваемый с помощью мастера 
    приложений.
    Он содержит данные о версии языка Visual C++, использованной для создания 
    файла, а также сведения о платформах, настройках и свойствах проекта, 
    выбранных с помощью мастера приложений.

DF1_ControlPult.vcxproj.filters
    Это файл фильтров для проектов VC++, созданный с помощью мастера 
    приложений. 
    Он содержит сведения о сопоставлениях между файлами в вашем проекте и 
    фильтрами. Эти сопоставления используются в среде IDE для группировки 
    файлов с одинаковыми расширениями в одном узле (например файлы ".cpp" 
    сопоставляются с фильтром "Исходные файлы").

DF1_ControlPult.cpp
    Это основной исходный файл приложения.

/////////////////////////////////////////////////////////////////////////////
Другие стандартные файлы:

StdAfx.h, StdAfx.cpp
    Эти файлы используются для построения файла предкомпилированного заголовка 
    (PCH) с именем DF1_ControlPult.pch и файла предкомпилированных типов 
    с именем StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Общие замечания:

С помощью комментариев «TODO:» в мастере приложений обозначаются фрагменты 
исходного кода, которые необходимо дополнить или изменить.

/////////////////////////////////////////////////////////////////////////////
