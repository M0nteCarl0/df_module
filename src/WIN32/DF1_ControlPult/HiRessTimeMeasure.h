#pragma once
#include <Windows.h>
#ifndef _HRTM_
#define _HRTM_
class HiRessTimeMeasure
{
public:
	HiRessTimeMeasure(void);
	~HiRessTimeMeasure(void);
	void Start(void);
	void End(void);
	double GetEllapsedSeconds(void);
	double GetEllapsedMiliseconds(void);
	double GetEllapsedMicroseconds(void);
	unsigned long long GetTotalCountHandles(void);

private:
	LARGE_INTEGER _Begin;
	LARGE_INTEGER _End;
	LARGE_INTEGER _Total;
	LARGE_INTEGER _Freq;
	DOUBLE _Sec;
	DOUBLE _Milisec;
	DOUBLE _Microsec;
	unsigned long long TimerID;
};
#endif
