// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "BaseNewProtocolDevice.h"
#include  <stdio.h>
/**************************************************************************************************************/
BaseNewProtocolDevice::BaseNewProtocolDevice(void)
{
    m_ModuleAdress = 0x0;
    InitDefaultCRCCheck();
}
/**************************************************************************************************************/
BaseNewProtocolDevice::~BaseNewProtocolDevice(void)
{
   
}
/**************************************************************************************************************/
BaseNewProtocolDevice::BaseNewProtocolDevice(int _ModuleAdress)
{

    SetModuleAdress(_ModuleAdress);
    InitDefaultCRCCheck();

}
/**************************************************************************************************************/
 BYTE     BaseNewProtocolDevice::MakeCRC   (BYTE *Data, int N)
 {

   int Summ = 0;
   for (int i = 0; i < N; i++){
      Summ += Data[i];
   }
   int SummH = (Summ >> 7) & 0x7F;

   BYTE mCRC = (BYTE) (SummH + (Summ & 0x7F));

   mCRC &= 0x7F;
   return mCRC;

 }
 /**************************************************************************************************************/
 void     BaseNewProtocolDevice::AddCRC    (BYTE *Data, int N)
 {
   if (N < 5) return;
   Data [4] = MakeCRC (Data, 4);
   if (N > 6){
      Data [N-1] = MakeCRC (&Data[5], N-6);
   }

 }
 /**************************************************************************************************************/
 bool     BaseNewProtocolDevice::CheckCRC  (BYTE *Data, int N)
 {
 
    if (N < 5) return false;

     BYTE CRC = MakeCRC (Data, 4);
     if (CRC != Data[4]) 
        return false;

     if (N > 6){
        CRC = MakeCRC (&Data[5], N-6);
        if (CRC != Data[N-1]) 
           return false;
     }
     return true;

 }
 /**************************************************************************************************************/
 int     BaseNewProtocolDevice:: CheckErrorWR (BYTE *DataS, int NS, BYTE *DataR, int NR)
 {
   if (NR < 5)                return 0x10;
   if (DataR[3] != NR)        return 0x30;
   return 0;

 }
 /**************************************************************************************************************/
 int      BaseNewProtocolDevice::WriteCommand (BYTE *DataS, int NS)
 {

   DataS [0] = mID;
   DataS [1] = m_ModuleAdress;

   DataS [3] = NS;                   // Amount of bytes

   if (m_FlgCheckCRCwrite)
      AddCRC (DataS, NS);

   int NR = NS;
   DWORD NumRec;

   bool Ret = m_Comm.WRcommand (DataS, NS, m_DataR, NR, &NumRec, m_FlgDebugS, m_FlgDebugR);

   if (!Ret)               LastErrorCode = 0x1; return 0x01;
   if (NR != NumRec)       LastErrorCode = 0x2;  return 0x02;
   if (m_FlgCheckCRCread && !CheckCRC (m_DataR, NR)) LastErrorCode = 0x3;  return 0x03;

   int Er = CheckErrorWR (DataS, NS, m_DataR, NR);
   LastErrorCode = Er;
   return Er;


 }
 /**************************************************************************************************************/
 int      BaseNewProtocolDevice::ReadCommand  (BYTE *DataS, int NS, BYTE *DataR, int NR)
 {

   DataS [0] = mID;
   DataS [1] = m_ModuleAdress;

   DataS [3] = NS;                   // Amount of bytes

   if (m_FlgCheckCRCwrite)
      AddCRC (DataS, NS);

   DWORD NumRec;

   bool Ret = m_Comm.WRcommand (DataS, NS, DataR, NR, &NumRec, m_FlgDebugS, m_FlgDebugR);

   if (!Ret)          {LastErrorCode = 0x1; return 0x01;};
   if (NR != NumRec)  {LastErrorCode = 0x2; return 0x02;};
   if (m_FlgCheckCRCread && !CheckCRC (m_DataR, NR)) {LastErrorCode = 0x3;  return 0x03;};

   int Er = CheckErrorWR (DataS, NS, DataR, NR);
   LastErrorCode = Er;
   return Er;


 }
 /**************************************************************************************************************/
 int      BaseNewProtocolDevice:: ReadVersion  (BYTE *Name, BYTE *Hard, BYTE *Soft)
   {

    int NS = 5;                         // Amount of Bytes  SEND
   m_DataS [2] = 0x40;                 // Commanda

   int NR = 14;                         // Amount of Bytes  RECEIVE 5+8+1=14

   int Er = ReadCommand (m_DataS, NS, m_DataR, NR);

   BYTE SB = 0x01;
   Name [2] = Name [3] = 0;
   Name [0] = m_DataR[5];
   if (m_DataR[8] & SB) Name[0] |= 0x80;
   SB = SB << 1;

   Name [1] = m_DataR[6];
   if (m_DataR[8] & SB) Name[1] |= 0x80;
   SB = SB << 1;

   Name[0] = m_DataR[5];
   Name[1] = m_DataR[6];
   Name[2] = m_DataR[7];
   Name[3] = m_DataR[8];

   Hard[0] = m_DataR[9];
   Hard[1] = m_DataR[10];
   Soft[0] = m_DataR[11];
   Soft[1] = m_DataR[12];

   return Er;


   }
 /**************************************************************************************************************/
 void   BaseNewProtocolDevice::InitDefaultCRCCheck(void)
 {
      m_FlgDebugS = false;
      m_FlgDebugR = false;
      m_FlgCheckCRCwrite = true;
      m_FlgCheckCRCread  = true;

 }
  /**************************************************************************************************************/
   
int   BaseNewProtocolDevice::InitComm(
                                    UINT _nSpeed,
                                    UINT _nDataBits, 
                                    UINT  _nParity, 
                                    UINT _nStopBits,
                                    int  _Timeout)



 {
     m_FTDIisConected = false;

    bool Ret = m_Comm.Configure (_nSpeed, 
                                 _nDataBits, 
                                 _nParity, 
                                 _nStopBits);
   if (Ret)
      printf ("Comm Configurate = %d  OK!\n\n", Ret);
     
   else
      printf ("Comm Configurate = %d  Error\n\n", Ret);

   m_Comm.SetTimeout (_Timeout); 
   m_FTDIisConected = Ret;
    return (int)Ret;
 }

 /**************************************************************************************************************/
  void BaseNewProtocolDevice::SetModuleAdress(int _ModuleAdress)
  {

        m_ModuleAdress = _ModuleAdress;

  };
/**************************************************************************************************************/