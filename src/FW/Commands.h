
#include "MyUsart.h"
#include "MyUsartHard.h"
enum CommadID
{
	CommadID_WRITE_SPEED_PROFILE_RUN	  = 0x1,
   CommadID_WRITE_STOP_MOTOR	        = 0x2,
   CommadID_WRITE_DRIVER_SETUP	     = 0x3,
   CommadID_DRIVE_TO_SELECTED_SENSOR  = 0x4,
   
   CommadID_READ_HW_SW				     = 0x40,
   CommadID_READ_SPEED_PROFILE	     = 0x41,
   CommadID_READ_STATE_WORD	        = 0x42,
   CommadID_READ_DRIVER_SETUP		     = 0x43,
   
};

#ifdef __cplusplus
 extern "C" {
 #endif
  
 void Command_ReadVersion(uint8_t *Buff, int N);
 void Command_WriteSetupDriver(uint8_t *Buff, int N);
 void Command_ReadSetupDriver(uint8_t *Buff, int N);
   
 void Command_StopMotor(uint8_t *Buff, int N);
 void Command_SpeedProfileRun(uint8_t *Buff, int N);
 void Command_GotoSelectedSensor(uint8_t *Buff, int N);
 void Command_ReadSpeedProfile(uint8_t *Buff, int N);
 void Command_ReadStateDrive(uint8_t *Buff, int N);
 
 uint16_t   Command_MakeCRC16 (uint8_t *Block, int Len);
 void       Command_ProcessCommands(void);
 void       Command_CommandParser(uint8_t *Buff,uint8_t ComandID,int N);  
 void       Command_IntToByte(int* Source,uint8_t* B1,uint8_t* B2,uint8_t* B3,uint8_t* B4,uint8_t* B5);
 void       Command_ByteToInt(int* Dest,uint8_t* B1,uint8_t* B2,uint8_t* B3,uint8_t* B4,uint8_t* B5);
 #ifdef  __cplusplus  
 }
 #endif

