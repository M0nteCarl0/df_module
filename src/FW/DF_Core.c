#include  "DF_Core.hpp"
#include  "FlashExtended.hpp"
#include  "MySPIN.h"
#include  "Sensors.h"
#include  <math.h> 
#include "Timer.h"
#include "Indep_WD.h"
#include "Timer.h"
int32_t                  g_AddresSPIN =   0x08040000;
FlashIO                  g_FlashEx;
int                      g_PositionBeginStop = 0;
uint32_t                 g_StepCount = 1000;
int32_t                  g_AbsolutePostionMotor = 0;
uint16_t                 g_TimeAcceleration;
uint16_t                 g_TimeBrake = 100;
int32_t                  g_CurrentSpeedMotor = 0;
uint32_t                 g_StateWord;
uint8_t                  g_StepMode = 1;
uint8_t                  g_StepModeR;
uint8_t                  g_ControlWord;
uint16_t                 g_SpeedBegin;
uint32_t                 g_SpeedEnd;
uint32_t                 g_MotorStateWord = 0;
uint32_t                 g_SelectedBaseSensor = 0;
uint32_t                 g_ErrorWS = 0;
uint32_t                 g_Mode = 0;
uint32_t                 g_MotorSpeed;
uint32_t                 g_DriverShutDownEvent = 0;
double                   g_CoenficinetMinSpeed;
double                   g_CoenficinetSpeed;

cSPIN_RegsStruct_TypeDef g_cSPIN;
StateWord                g_AlternState;
ControlWord              g_AltControlWord;
SensorWord  Sense;

float BeginSpeed;
float EndSpeed;   
float ACC;        
float DEC;
cSPIN_Direction_TypeDef Direction;
uint32_t g_RunMinSpeed = 0;
bool TimeLatch;
bool TimeStop;
uint32_t TimeAcc;
uint32_t TimesStop;
uint32_t AccStepCount;
uint32_t BrakeStepCount;
/******************************************************************************/
void  DF_Core_ReadSpeedProflie( 
                               uint8_t* ControlWord, 
                               uint16_t* SpeedBegin,
                               uint32_t* SpeedEnd,
                               uint16_t* TimeAcc,
                               uint16_t* TimeBrake,
                               uint32_t* StepCount,
                               uint8_t* StepMode)
{
  
  *ControlWord   =  g_AltControlWord.Raw;
  *SpeedBegin    =  g_SpeedBegin,
  *SpeedEnd      =  g_SpeedEnd,
  *TimeAcc       =  g_TimeAcceleration;
  *TimeBrake     =  g_TimeBrake;
  *StepCount     =  g_StepCount;
  *StepMode      =  g_StepMode;
}
/******************************************************************************/
void DF_Core_WSErrorCondition(void)
{
      g_ErrorWS                       = 1;
      g_AltControlWord.Bits.WS_Error  = 1;
      g_AlternState.Bits.WS_Error     = 1; 
}

/******************************************************************************/
int DF_Core_CheckSpeedCondtitions(void)
{
  
    int Flag  =  1;
  
    
    BeginSpeed  = 0;
    EndSpeed    = 0;
    
    if(g_SpeedBegin >=4095 )
    {
       g_SpeedBegin = 4095 ;
    }
    
    if(g_SpeedEnd >=1048575)
    {
       g_SpeedEnd = 1048575;
    }
    
    if(g_SpeedEnd == 0 &&  g_SpeedBegin == 0)
    {
      DF_Core_WSErrorCondition();
      
    }
  
    
    if(!g_ErrorWS)
    {
      BeginSpeed                        = (float)(g_SpeedBegin * g_CoenficinetMinSpeed); 
      EndSpeed                          = (float)(g_SpeedEnd *  g_CoenficinetSpeed);
      
      if(BeginSpeed > EndSpeed )
    {
      DF_Core_WSErrorCondition();
    }
    
      
      
    }
  Flag = !g_ErrorWS ;
  return Flag;
  
}
/******************************************************************************/
int DF_Core_CheckAccelerationCondtitions(void)
{
   int Flag  =  1;
   ACC = 0;
   DEC = 0;
   if(g_TimeAcceleration > 0 )
    {
       ACC                            = (EndSpeed - BeginSpeed)/((float)g_TimeAcceleration/1000);
       if(ACC  == 0)
       {
         DF_Core_WSErrorCondition();
       } 
    }
   else
   {
       ACC                            = (EndSpeed - BeginSpeed)/((float)1/1000);
   }
    
    if(g_TimeBrake > 0)
    {
       DEC                            = (EndSpeed - BeginSpeed) /((float)g_TimeBrake/1000);
       if(DEC  == 0)
       {
          DF_Core_WSErrorCondition();
       }
       
    }
    else
   {
       DEC = (EndSpeed - BeginSpeed) /((float)1/1000);;
   }
   
   if( DEC >=  59589)
   {
     DEC = 59589;
   }
   
   if(DEC < 14.55)
   {
     DEC = 14.55; 
   }
    
    if(ACC >=  59589)
   {
      ACC = 59589;
   }
   
   if(ACC < 14.55)
   {
     ACC = 14.55;
   }
   
   Flag = !g_ErrorWS;
   
   return Flag;
  
}
/******************************************************************************/
int DF_Core_CheckStepCountCondtitions(void)
{
  int Flag  =  1;
  if(g_StepCount == 0 ||  g_StepCount >=2097151)
    {
      DF_Core_WSErrorCondition();
    }
  
   Flag = !g_ErrorWS;
   return Flag;  
  
}
/******************************************************************************/
void DF_Core_SetupModeAndPositionBeginStop(void)
{
  
    g_PositionBeginStop               = g_StepCount;
    if(g_AltControlWord.Bits.DIR)
    {
      Direction                        = REV;
      g_PositionBeginStop              *=-1;;
    }
    else
    {
      Direction                        = FWD;
      
    }
    
    switch(g_AltControlWord.Bits.Mode)
     {
        case ControlWordModeBits_StopOnXP4:
        {
          g_AlternState.Bits.Selected_XP = 0;
          break;
        }
          
          case ControlWordModeBits_StopOnXP5:
        {
          g_AlternState.Bits.Selected_XP = 1;
          break;
        }
       
     } 
}
/******************************************************************************/
void DF_CheckCommandExecution(void)
{
      g_MotorStateWord = (uint32_t)cSPIN_Get_Status();
      if( g_MotorStateWord &  cSPIN_STATUS_NOTPERF_CMD == 1 ||
          g_MotorStateWord &  cSPIN_STATUS_WRONG_CMD  == 1)
      {
        DF_Core_WSErrorCondition(); 
      }
}
/******************************************************************************/
void  DF_Core_WriteSpeedProflieRun(
                               uint8_t* ControlWord,    
                               uint16_t* SpeedBegin,
                               uint32_t* SpeedEnd,
                               uint16_t* TimeAcc,
                               uint16_t* TimeBrake,
                               uint32_t* StepCount,
                               uint8_t* StepMode)
{
  
  AccStepCount = 0;
  DF_Core_ReadStateWordDriveAndSensors();
  uint32_t DriveMode = (g_MotorStateWord >> 5) & 0x3;
  if(DriveMode == MOT_STATUSE_Stopped)
  {
     cSPIN_Reset_Pos();
    TimeLatch = 1;
    g_AltControlWord.Raw              =  *ControlWord;
    g_AltControlWord.Bits.WS_Error    =   0;
    g_AlternState.Bits.WS_Error       =   0;
    g_AlternState.Bits.SW_HardStopEvent = 0;
    g_SpeedBegin                      =  *SpeedBegin; 
    g_SpeedEnd                        =  *SpeedEnd;   
    g_TimeAcceleration                =  *TimeAcc;    
    g_TimeBrake                       =  *TimeBrake;  
    g_StepCount                       =  *StepCount;  
    g_StepMode                        =  *StepMode;   
    g_ErrorWS                         =  0;
    uint8_t  StepModeR                =  0;
    
    cSPIN_Hard_HiZ();
    cSPIN_Registers_Set(&g_cSPIN);
    
    uint8_t  StepModeW        = g_StepMode | 0x8 | 0x80;
    StepModeR     =  cSPIN_Get_Param(cSPIN_STEP_MODE) & 0x7;
    cSPIN_Set_Param(cSPIN_STEP_MODE,(uint8_t)(StepModeW));
    DF_CheckCommandExecution();
    
    cSPIN_Set_Param(cSPIN_EL_POS,0); 
    DF_CheckCommandExecution();
    
    DF_Core_CheckSpeedCondtitions();
    DF_Core_CheckAccelerationCondtitions();
    DF_Core_CheckStepCountCondtitions();
    DF_Core_SetupModeAndPositionBeginStop();
    
    CheckSensors();
    Sense                 = GetStatusSensors();
  
    if((g_AltControlWord.Bits.Mode  ==  0x1 &&  g_AlternState.Bits.Selected_XP == 0 && Sense.Bits.DP0) ||
      (g_AltControlWord.Bits.Mode   ==  0x2 &&  g_AlternState.Bits.Selected_XP == 1 && Sense.Bits.DP1))
    {
        DF_Core_WSErrorCondition();
    }
   
     g_MotorStateWord = (uint32_t)cSPIN_Get_Status();
    
    if( g_MotorStateWord & cSPIN_STATUS_NOTPERF_CMD       ||
          g_MotorStateWord & cSPIN_STATUS_UVLO     == 0	 ||	
          g_MotorStateWord & cSPIN_STATUS_UVLO_ADC == 0   ||
          g_MotorStateWord & cSPIN_STATUS_TH_SD	   == 0   ||
          g_MotorStateWord & cSPIN_STATUS_OCD	   == 0   
         
        )
      {
          DF_Core_WSErrorCondition();
      }
      
    if(!cSPIN_CheckConfiguration(&g_cSPIN))
    {
       cSPIN_Registers_Set(&g_cSPIN);
       if(!cSPIN_CheckConfiguration(&g_cSPIN))
       {
          DF_Core_WSErrorCondition(); 
       }
    }
    
    
    if(!g_ErrorWS)
    {
     
      
      StepModeW        = g_StepMode | 0x8 | 0x80;
      StepModeR     =  cSPIN_Get_Param(cSPIN_STEP_MODE) & 0x7;
      cSPIN_Set_Param(cSPIN_STEP_MODE,(uint8_t)(StepModeW));
      StepModeR        = 0;
      uint32_t ABS_PositionR    = 0;
      uint16_t EL_PositionR     = 0;
      
      DF_CheckCommandExecution();
      cSPIN_SetMinSpeed(BeginSpeed+1);
      DF_CheckCommandExecution();
      cSPIN_SetAcc(ACC);
      DF_CheckCommandExecution();
      cSPIN_SetDec(DEC);
      DF_CheckCommandExecution();
      cSPIN_Set_Param(cSPIN_ABS_POS,0);
      DF_CheckCommandExecution();
      
      cSPIN_Reset_Pos();
      DF_CheckCommandExecution();   
      
      StepModeR     =  cSPIN_Get_Param(cSPIN_STEP_MODE) & 0x7;
      ABS_PositionR =  cSPIN_Get_Param(cSPIN_ABS_POS);
      EL_PositionR  =  cSPIN_Get_Param(cSPIN_EL_POS);
      uint32_t EndSpeedW = Speed_Steps_to_Par(EndSpeed);
   
      if(StepModeR == g_StepMode && ABS_PositionR == 0)
      {
        if(!g_ErrorWS)
        {
          Timer_TraceBegin();
          cSPIN_Run(Direction,EndSpeedW+1);
        }
      }
      g_MotorStateWord = (uint32_t)cSPIN_Get_Status();
      
      cSPIN_Direction_TypeDef DirectionR = (cSPIN_Direction_TypeDef)((g_MotorStateWord >> 4) & 0x1);
      if( 
          g_MotorStateWord & cSPIN_STATUS_UVLO     == 0	 ||	
          g_MotorStateWord & cSPIN_STATUS_UVLO_ADC == 0   ||
          g_MotorStateWord & cSPIN_STATUS_TH_SD	   == 0   ||
          g_MotorStateWord & cSPIN_STATUS_OCD	   == 0   ||
          DirectionR != Direction
        )
      {
          DF_Core_WSErrorCondition();
      }
    }
  }
  else
  {
      DF_Core_WSErrorCondition();
  }
    *ControlWord = g_AltControlWord.Raw;
}
/******************************************************************************/
void   DF_Core_ReadDriveState(uint32_t* StateWord,
                              int* ABS_Position,
                              uint32_t* Speed)
{
  
    DF_Core_ReadStateWordDriveAndSensors();
    *StateWord    = g_AlternState.Raw;
    *ABS_Position = g_AbsolutePostionMotor;
    if(g_MotorSpeed >= 0)
    {
        *Speed        = (uint32_t)(float)g_MotorSpeed/g_CoenficinetSpeed;
    }
    else
    {
        *Speed        = 0;
    }
    
}
/******************************************************************************/
void   DF_Core_ReadSetupFomFlash(cSPIN_RegsStruct_TypeDef& cSPIN)
{
   cSPIN_Registers_ReadAll(&cSPIN);  
}
/******************************************************************************/
void   DF_Core_WriteSetupToFlash(cSPIN_RegsStruct_TypeDef& cSPIN)
{
  uint32_t DriveMode = (g_MotorStateWord >> 5) & 0x3;
  if(DriveMode == MOT_STATUSE_Stopped)
  {
    g_AltControlWord.Bits.WS_Error    =   0;
    g_AlternState.Bits.WS_Error       =   0;
    cSPIN_Hard_HiZ();
    cSPIN.CheckWord = 0x92;
    cSPIN_Registers_Set(&cSPIN);
    cSPIN_Registers_ReadAll(&g_cSPIN);
    g_FlashEx.Write(g_AddresSPIN,&cSPIN,1);
  }
  else
  {
     DF_Core_WSErrorCondition();
  }
  
}
/******************************************************************************/
void   DF_Core_LoadSetupDriverFromFlash(void)
{
  
  g_CoenficinetMinSpeed = pow(10.0,6)/(pow(2.0,24) * 0.25);
  g_CoenficinetSpeed    = pow(10.0,6)/(pow(2.0,28) * 0.25);
  g_FlashEx  = FlashIO();
  g_FlashEx.Read(g_AddresSPIN,&g_cSPIN,1);
  cSPIN_Hard_HiZ();
  if(g_cSPIN.CheckWord == 0x92)
  {
      cSPIN_Registers_Set(&g_cSPIN);
      Sensors_InverseInput(g_cSPIN.InverseSensorInput);
  };
  
}
/******************************************************************************/
void   DF_Core_ReadStateWordDriveAndSensors(void)
{
  CheckSensors();
  g_MotorStateWord                   = (uint32_t)cSPIN_Get_Status();
  g_AlternState.Bits.EngineState     = g_MotorStateWord;
  SensorWord  Sense                  = GetStatusSensors();
  uint32_t DriveMode                 = (g_MotorStateWord >> 5) & 0x3;
  
    if(DriveMode != MOT_STATUSE_Stopped)
  {
      g_AbsolutePostionMotor           = cSPIN_Get_ABS_Position();
      g_MotorSpeed                     = cSPIN_Get_CurrentSpeed()+1;
  }
  else
  {
      g_MotorSpeed                     = 0;         
  }
  
  if(DriveMode == MOT_STATUSE_Constant_speed && TimeLatch )
    
  {
    Timer_TraceEnd();
    TimeAcc =  Timer_TraceGetTime();
    AccStepCount =  cSPIN_Get_ABS_Position();
    TimeLatch = false; 
    TimeStop = true;
   }
  
   
    if(DriveMode == MOT_STATUSE_Stopped && TimeStop)
    {
      
      Timer_TraceEnd();
      TimesStop =  Timer_TraceGetTime();
      TimeStop = false;
      
      
    }
   
   
   
  
  g_AlternState.Bits.XP4_Trigered  = Sense.Bits.DP0;
  g_AlternState.Bits.XP5_Trigered  = Sense.Bits.DP1;
  
  if((DriveMode == MOT_STATUSE_Acceleration ||  DriveMode == MOT_STATUSE_Constant_speed))
  {
     if((g_AbsolutePostionMotor >= g_PositionBeginStop && Direction == FWD)||
       ( g_AbsolutePostionMotor <= g_PositionBeginStop && Direction == REV)||
       (g_AltControlWord.Bits.Mode  ==  0x1 &&  g_AlternState.Bits.Selected_XP == 0 && Sense.Bits.DP0) ||
       (g_AltControlWord.Bits.Mode  ==  0x2 &&  g_AlternState.Bits.Selected_XP == 1 && Sense.Bits.DP1))
     {
         if(g_TimeBrake > 0)
         {
            cSPIN_Soft_Stop(); 
         }
         else
         {
           cSPIN_Hard_Stop();
           
         }
         Timer_TraceBegin();
       
         
     } 
  }

}
/****************************************************************************/
void DF_Core_PollingTimerInit(void)
{
   RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
   TIM_TimeBaseInitTypeDef _TimerSetup;;
  _TimerSetup.TIM_ClockDivision = TIM_CKD_DIV1;
  _TimerSetup.TIM_CounterMode  = TIM_CounterMode_Up;
  _TimerSetup.TIM_Prescaler = 4200;
  _TimerSetup.TIM_Period = 5-1;
   TIM_TimeBaseInit(TIM3,&_TimerSetup);
   TIM_Cmd(TIM3,ENABLE);
   TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
   NVIC_InitTypeDef _SetupIRQ;
  _SetupIRQ.NVIC_IRQChannel =TIM3_IRQn;
  _SetupIRQ.NVIC_IRQChannelPreemptionPriority = 0;
  _SetupIRQ.NVIC_IRQChannelSubPriority = 1;
  _SetupIRQ.NVIC_IRQChannelCmd = ENABLE;
   NVIC_Init(&_SetupIRQ);

}
/****************************************************************************/
void  TIM3_IRQHandler(void)
{
    if(TIM_GetITStatus(TIM3,TIM_IT_Update)!=RESET)
  {  
     TIM_ClearITPendingBit(TIM3,TIM_IT_Update);
     DF_Core_ReadStateWordDriveAndSensors();
    
  }
  
}
/****************************************************************************/
void DF_EmeragancyStopMotor(void)
{
  
  cSPIN_Peripherals_GPIO_Init();
  if(iWD_Fault_is_Detected())
  {
      cSPIN_SW_HartStopEngine();
      g_AlternState.Bits.SW_HardStopEvent = 1;
  }
  iWD_Init();
  
}
