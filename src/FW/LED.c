#include "LED.h"
static int i  = 0;
static uint32_t DebugCounter = 0;
/*****************************************************************************/
void InitLED(void)
{
   GPIO_InitTypeDef _GPIO;
   RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);
   RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD,ENABLE); 
  _GPIO.GPIO_Mode = GPIO_Mode_OUT;
  _GPIO.GPIO_OType  = GPIO_OType_PP;
  _GPIO.GPIO_PuPd = GPIO_PuPd_DOWN;
  _GPIO.GPIO_Speed = GPIO_Speed_2MHz;
     
  _GPIO.GPIO_Pin =  GPIO_Pin_11| GPIO_Pin_12;
   GPIO_Init(GPIOC,&_GPIO);
   
   _GPIO.GPIO_Pin =  GPIO_Pin_2;
   GPIO_Init(GPIOD,&_GPIO);
   
   GPIO_ResetBits(GPIOB,GPIO_Pin_8);
   ResetSynchroLed();
}
/*****************************************************************************/
void BlinkyLED(void)
{
    i++;
    if(i%2)
    {
       GPIO_ResetBits(GPIOB,GPIO_Pin_7||GPIO_Pin_9);
    }
    else
    {
       GPIO_SetBits(GPIOB,GPIO_Pin_7|GPIO_Pin_9);
    }
}
/*****************************************************************************/
void SynchroLed(void)
{
   GPIO_SetBits(GPIOB,GPIO_Pin_7);
}
/*****************************************************************************/
void ResetSynchroLed(void)
{
   GPIO_ResetBits(GPIOB,GPIO_Pin_7);
}
/*****************************************************************************/
void EndAcc(void)
{
  GPIO_SetBits(GPIOB,GPIO_Pin_8);
}
/*****************************************************************************/
void BeginAcc(void)
{
    GPIO_ResetBits(GPIOC,GPIO_Pin_6);
}
/*****************************************************************************/
void DeviceOn(void)
{
    GPIO_SetBits(GPIOB,GPIO_Pin_9);
}
/*****************************************************************************/

void OnDP1Led(void)
{
    GPIO_SetBits(GPIOC,LED_Definition_VD4);  
}
/*****************************************************************************/
void OffDP1Led(void)
{
    GPIO_ResetBits(GPIOC,LED_Definition_VD4); 
}
/*****************************************************************************/
void OnDP2Led(void)
{
   GPIO_SetBits(GPIOC,LED_Definition_VD5);  
}
/*****************************************************************************/
void OffDP2Led(void)
{
   GPIO_ResetBits(GPIOC,LED_Definition_VD5); 
}
/*****************************************************************************/
void OnDP3Led(void)
{
   GPIO_SetBits(GPIOC,LED_Definition_VD6);
}
/*****************************************************************************/
void OffDP3Led(void)
{
  GPIO_ResetBits(GPIOC,LED_Definition_VD6); 
}
/*****************************************************************************/
void OnKYEvent(void)
{
    GPIO_SetBits(GPIOB,GPIO_Pin_7); 
}
/*****************************************************************************/
void OffKYEvent(void)
{
    GPIO_ResetBits(GPIOB,GPIO_Pin_7); 
}
/*****************************************************************************/
void DetectorReadyLedOn(void)
{
    GPIO_SetBits(GPIOB,GPIO_Pin_8); 
}
/*****************************************************************************/
void DetectorReadyLedOff(void)
{
    GPIO_ResetBits(GPIOB,GPIO_Pin_8);  
}
/*****************************************************************************/
void OnSVD1(void)
{
   GPIO_SetBits(GPIOD,GPIO_Pin_2); 
}
/*****************************************************************************/
void OnSVD2(void)
{
   GPIO_SetBits(GPIOC,GPIO_Pin_12);
}
/*****************************************************************************/
void OnSVD3(void)
{
   GPIO_SetBits(GPIOC,GPIO_Pin_11);
}
/*****************************************************************************/
void OffSVD1(void)
{
  GPIO_ResetBits(GPIOD,GPIO_Pin_2);
}
/*****************************************************************************/
void OffSVD2(void)
{
  GPIO_ResetBits(GPIOC,GPIO_Pin_12); 
}
/*****************************************************************************/
void OffSVD3(void)
{
  GPIO_ResetBits(GPIOC,GPIO_Pin_11); 
}
/*****************************************************************************/
void LED_ModuleHeartBeat(void)
{
  
    if (((DebugCounter++)/100000) % 2)   OnSVD2();
     else                              OffSVD2();
             
}
/*****************************************************************************/