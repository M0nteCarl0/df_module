//------------------------------------------------------------------------------
//    Novikov
//    10.02.2015
//------------------------------------------------------------------------------
/*****************************************************************************
                          Changelog
20-07-2016: Refactor code Novikov by Molotaliev A.O
*****************************************************************************/


#include "MyUsartHard.h"
#include "Commands.h"
//------------------------------------------------------------------------------
USART_TypeDef * USARTy = USART3;
//-----------------------------------------------------------------------------
  #define USARTy_CLK               RCC_APB1Periph_USART3
  #define USARTy_GPIO_CLK          RCC_AHB1Periph_GPIOB        //RCC_APB2Periph_GPIOB

  #define USARTy_GPIO              GPIOB
  #define USARTy_RxPin             GPIO_Pin_11
  #define USARTy_TxPin             GPIO_Pin_10
  #define USARTy_IRQn              USART3_IRQn
//------------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void Init_UsartHard ()
{
  USART_InitTypeDef USART_InitStructure;
  
  My_Initiate_Usart  ();
  GPIO_Configuration ();
  
  USART_InitStructure.USART_BaudRate    = 19200;     //19200;                                //115200;  
  USART_InitStructure.USART_WordLength  = USART_WordLength_9b;
  USART_InitStructure.USART_StopBits    = USART_StopBits_2;
  USART_InitStructure.USART_Parity      = USART_Parity_Odd;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode        = USART_Mode_Rx | USART_Mode_Tx;

  USART_Init (USARTy, &USART_InitStructure);

  USART_ITConfig (USARTy, USART_IT_RXNE, DISABLE);
  USART_ITConfig (USARTy, USART_IT_TXE,  DISABLE);

  USART_ITConfig (USARTy, USART_IT_CTS,  DISABLE);
  USART_ITConfig (USARTy, USART_IT_LBD,  DISABLE);
  USART_ITConfig (USARTy, USART_IT_TC,   DISABLE);
  USART_ITConfig (USARTy, USART_IT_IDLE, DISABLE);
  USART_ITConfig (USARTy, USART_IT_PE,   DISABLE);
  USART_ITConfig (USARTy, USART_IT_ERR,  DISABLE);
  USART_ITConfig (USARTy, USART_IT_ORE,  DISABLE);
  USART_ITConfig (USARTy, USART_IT_NE,  DISABLE);
  USART_ITConfig (USARTy, USART_IT_FE,  DISABLE);
  USART_ITConfig (USARTy, USART_IT_RXNE, ENABLE);
  USART_Cmd (USARTy, ENABLE);

  NVIC_Configuration ();
};
//-----------------------------------------------------------------------------
void My_Initiate_Usart (void)
{
   RCC_APB1PeriphClockCmd (USARTy_CLK,       ENABLE);  
   RCC_AHB1PeriphClockCmd (USARTy_GPIO_CLK,  ENABLE);   
   GPIO_PinAFConfig (GPIOB, GPIO_PinSource11, GPIO_AF_USART3);
   GPIO_PinAFConfig (GPIOB, GPIO_PinSource10, GPIO_AF_USART3);
};
//------------------------------------------------------------------------------
void GPIO_Configuration(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
                                    /* Configure USARTy Rx as input floating */
  GPIO_InitStructure.GPIO_Pin   = USARTy_RxPin;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;             //GPIO_Mode_IN_FLOATING;
  GPIO_Init (USARTy_GPIO, &GPIO_InitStructure);
  
                      /* Configure USARTy Tx as alternate function push-pull */
  GPIO_InitStructure.GPIO_Pin   = USARTy_TxPin;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;             //GPIO_Mode_AF_PP;
  GPIO_Init (USARTy_GPIO, &GPIO_InitStructure);
  
  
   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
};
//------------------------------------------------------------------------------
void NVIC_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;

  NVIC_PriorityGroupConfig (NVIC_PriorityGroup_1);
  
  NVIC_InitStructure.NVIC_IRQChannel                    = USARTy_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority  = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority         = 1;   //1;
  NVIC_InitStructure.NVIC_IRQChannelCmd                 = ENABLE;
  NVIC_Init (&NVIC_InitStructure);
};
//-----------------------------------------------------------------------------
void  USART3_IRQHandler(void)
{
   while (USART_GetITStatus (USART3, USART_IT_RXNE) != RESET){
      ReceiveData();
   }
  
   if (USART_GetITStatus(USART3, USART_IT_TXE) == SET){   
      SendData();
   }
   
   if (USART_GetITStatus(USART3, USART_IT_TC) != RESET){   
      TransferComplete();
   }
}
/******************************************************************************/
void EnableRS485(void)
 {
    GPIO_WriteBit(GPIOB,GPIO_Pin_12,Bit_SET);
    USART_ITConfig (USART3, USART_IT_TXE,  ENABLE);
 };
 /******************************************************************************/
 void DisableRS485(void)
 {
    GPIO_WriteBit(GPIOB,GPIO_Pin_12,Bit_RESET);  
 };
/******************************************************************************/
 inline void ReceiveData(void)
{ 
      uint8_t DataR;
      DataR = USART_ReceiveData (USART3);
      PutOneCharterToReceiveStack(DataR); 
}
/******************************************************************************/
inline void  SendData(void)
{
      int Ret;
      uint8_t DataT;
      Ret = GetCharterFromTranceiveStack(&DataT);
      if (Ret){
         USART_SendData (USART3, DataT);
      }
      else{
         USART_ITConfig(USART3, USART_IT_TXE,  DISABLE);
         USART_ITConfig (USART3, USART_IT_TC,   ENABLE);    // 10.01.2013
      }
}
/******************************************************************************/
inline void TransferComplete(void)
{
      USART_ClearITPendingBit (USART3, USART_IT_TC);
      USART_ITConfig (USART3, USART_IT_TC,   DISABLE);
      DisableRS485();
}
/******************************************************************************/


