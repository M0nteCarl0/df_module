#include "LED.h"
#include "MySPIN.h"
#include "Sensors.h"
#ifdef __cplusplus
 extern "C" {
#endif

 typedef struct StateWordS
{
 __IO  uint16_t EngineState:16;
 __IO  uint8_t XP4_Trigered:1;
 __IO  uint8_t XP5_Trigered:1;
 __IO  uint8_t Selected_XP:1;
 __IO  uint8_t WS_Error:1;
 __IO  uint8_t SW_HardStopEvent:1;
}StateWordS;

typedef union StateWord
{
  StateWordS Bits;
  uint32_t Raw;
}StateWord;
  
 typedef struct ControlWordS
{
 __IO  uint8_t DIR:1;
 __IO  uint8_t Mode:2;
 __IO  uint8_t Reserved:3;
 __IO  uint8_t WS_Error:1;
 __IO  uint8_t Reserved8:1;
}ControlWordS;

typedef union ControlWord
{
  ControlWordS Bits;
  uint8_t Raw;
}ControlWord;
  
enum StateWordBits
{
  StateWordBits_Hiz,
  StateWordBits_Busy,
  StateWordBits_SW_F,
  StateWordBits_SW_EVN,
  StateWordBits_DIR,
  StateWordBits_NOTPERF_CMD  = 7,
  StateWordBits_WRONG_CMD    = 8,
  StateWordBits_UVLO         = 9,
  StateWordBits_TH_WRN       = 10,
  StateWordBits_TH_SD        = 11,
  StateWordBits_OCD          = 12,
  StateWordBits_SCK_MODE     = 15,
  StateWordBits_XP4_Trigered = 16,
  StateWordBits_XP5_Trigered = 17,
  StateWordBits_Selected_XP  = 18,
  StateWordBits_WS_Error     = 19,
};

enum ControlWordBits
{
  ControlWordBits_DIR,
  ControlWordBits_WS_RO = 7,
};

enum ControlWordModeBits//1:2 Bits
{
  ControlWordModeBits_MainDrive = 0 ,
  ControlWordModeBits_StopOnXP4 = 1,
  ControlWordModeBits_StopOnXP5 = 2,
};


void  DF_Core_ReadSpeedProflie(
                               uint8_t* ControlWord,
                               uint16_t* SpeedBegin,
                               uint32_t* SpeedEnd,
                               uint16_t* TimeAcc,
                               uint16_t* TimeBrake,
                               uint32_t* StepCount,
                               uint8_t* StepMode);

void  DF_Core_WriteSpeedProflieRun(
                               uint8_t* ControlWord,
                               uint16_t* SpeedBegin,
                               uint32_t* SpeedEnd,
                               uint16_t* TimeAcc,
                               uint16_t* TimeBrake,
                               uint32_t* StepCount,
                               uint8_t* StepMode);


void   DF_Core_ReadDriveState(uint32_t* StateWord,
                              int* ABS_Position,
                              uint32_t* Speed);


void   DF_Core_ReadSetupFomFlash(cSPIN_RegsStruct_TypeDef& cSPIN);
void   DF_Core_WriteSetupToFlash(cSPIN_RegsStruct_TypeDef& cSPIN);
void   DF_Core_LoadSetupDriverFromFlash(void);
void   DF_Core_ReadStateWordDriveAndSensors(void);
int DF_Core_CheckSpeedCondtitions(void);
int DF_Core_CheckAccelerationCondtitions(void);
int DF_Core_CheckStepCountCondtitions(void);
void DF_Core_SetupModeAndPositionBeginStop(void);
void DF_Core_WSErrorCondition(void);
void DF_Core_PollingTimerInit(void);
void  TIM3_IRQHandler(void);
void DF_CheckCommandExecution(void);
void DF_EmeragancyStopMotor(void);
#ifdef __cplusplus
 }
#endif 
