#include "Timer.h"
#include "stm32f4xx_dbgmcu.h"
//#include "LED.h"
int Flg = 0;
/***************************************************************************/
 void Timer_InitMilisecond(void)
 {
   DBGMCU_APB1PeriphConfig(DBGMCU_TIM6_STOP,ENABLE);
   
   RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6,ENABLE);
   TIM_DeInit(TIM6);
   TIM_TimeBaseInitTypeDef Timer_Intit;
   
   Timer_Intit.TIM_ClockDivision      = TIM_CKD_DIV1;//36Mhz
   Timer_Intit.TIM_CounterMode        = TIM_CounterMode_Up;
   Timer_Intit.TIM_Period             = 1;
   Timer_Intit.TIM_Prescaler          = 42000;
   Timer_Intit.TIM_RepetitionCounter  = 0;
   TIM_TimeBaseInit(TIM6,&Timer_Intit);
   TIM_ClearITPendingBit(TIM6,TIM_IT_Update);
  
 }
 /***************************************************************************/
void Timer_TraceInit(void)
{
  
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5,ENABLE);
   TIM_DeInit(TIM5);
   TIM_TimeBaseInitTypeDef Timer_Intit;
   
   Timer_Intit.TIM_ClockDivision      = TIM_CKD_DIV1;//36Mhz
   Timer_Intit.TIM_CounterMode        = TIM_CounterMode_Up;
   Timer_Intit.TIM_Period             = 0xFFFFFFFF-1;
   Timer_Intit.TIM_Prescaler          = 42000;///Милисекунды
   Timer_Intit.TIM_RepetitionCounter  = 0;
   TIM_TimeBaseInit(TIM5,&Timer_Intit);
}
 /***************************************************************************/
 void Timer_TraceBase(TraceBase Base)
 {
     TIM5->PSC = Base;
 }
 /***************************************************************************/
  void Timer_TraceBegin(void)
  {
     TIM5->CNT = 0;
     TIM_Cmd(TIM5,ENABLE);   
  }
/***************************************************************************/
  void Timer_TraceEnd(void)
  {
      TIM_Cmd(TIM5,DISABLE);
  }
/***************************************************************************/
  uint32_t Timer_TraceGetTime(void)
  {
    return TIM5->CNT;
  }
/***************************************************************************/
 void Timer_WaitMilisec(uint16_t Time)
 {
 //
   Flg   = 0;
   TIM6->PSC = 42000;
   TIM_SetAutoreload(TIM6,Time);
   TIM_ClearITPendingBit(TIM6,TIM_IT_Update);
   TIM_ITConfig(TIM6,TIM_IT_Update,ENABLE);
   NVIC_EnableIRQ(TIM6_DAC_IRQn);
   TIM_Cmd(TIM6,ENABLE);
   do
   {
   }while(!Flg);
 }
/***************************************************************************/
  void Timer_WaitMicrosec(uint16_t Time)
  {
   
   Flg   = 0;
   TIM6->PSC = 42;
   TIM_SetAutoreload(TIM6,Time);
   TIM_ClearITPendingBit(TIM6,TIM_IT_Update);
   TIM_ITConfig(TIM6,TIM_IT_Update,ENABLE);
   NVIC_EnableIRQ(TIM6_DAC_IRQn);
   TIM_Cmd(TIM6,ENABLE);
   do
   {
   }while(!Flg);   
  }
/***************************************************************************/
 void TIM6_DAC_IRQHandler(void)
 {
   if(TIM_GetITStatus(TIM6,TIM_IT_Update)!= RESET)
   {
    
     TIM_ClearITPendingBit(TIM6,TIM_IT_Update);
     Flg = 1;
     TIM_Cmd(TIM6,DISABLE);
   }
   
 }
/***************************************************************************/
  int  Timer_TraceTimeOut(int Timeout)
  {
    int Flag = 0;
    
    if(Timer_TraceGetTime() >=Timeout)
    {
      Flag = 1;
    }
      return Flag;
  }
 
