#include "Sensors.h"
#include "LED.h"
SensorWord SensorStatus;
Sensor_Assigment DP0;
Sensor_Assigment DP1;
int InverseInpitState;
/******************************************************************************/
void InitSensor(Sensor_Assigment Sensor)
{
  RCC_AHB1PeriphClockCmd(Sensor.RCC_Sensor,ENABLE);  
  GPIO_InitTypeDef _GPIO;
  GPIO_StructInit(&_GPIO);
 _GPIO.GPIO_Mode   = GPIO_Mode_IN;
 _GPIO.GPIO_Pin    = Sensor.InputPin;
 _GPIO.GPIO_PuPd   = GPIO_PuPd_DOWN;
 _GPIO.GPIO_Speed  = GPIO_Speed_2MHz;
  GPIO_Init(Sensor.GPIO_Port,&_GPIO);
  Sensor.State = 0x0;
}
/******************************************************************************/
void InitSensrors(void)
{
  DP0.RCC_Sensor = RCC_AHB1Periph_GPIOC;
  DP0.GPIO_Port = GPIOC;
  DP0.InputPin = GPIO_Pin_0;
  InitSensor(DP0);
  
  DP1.RCC_Sensor = RCC_AHB1Periph_GPIOC;
  DP1.GPIO_Port = GPIOC;
  DP1.InputPin = GPIO_Pin_1;
  InitSensor(DP1);
  SensorStatus.Raw = 0;
}
/******************************************************************************/
uint32_t inline ReadSensor(Sensor_Assigment Sensor)
{
  return GPIO_ReadInputDataBit(Sensor.GPIO_Port,Sensor.InputPin);
}
/******************************************************************************/
bool inline LeftTrigered(void)
{
  bool Flag = false; 
  
  if(ReadSensor(DP0))
   {
     
      if(!InverseInpitState){
        SensorStatus.Bits.DP0 = 1;
        OnSVD3();
        Flag = true;
      }
      else{  
        SensorStatus.Bits.DP0 = 0;
         OffSVD3();
      }
    
   }
   else
   {
      if(!InverseInpitState){ 
     SensorStatus.Bits.DP0 = 0;
      OffSVD3();
      }
      else
      {  
      SensorStatus.Bits.DP0 = 1;
      OnSVD3();
      Flag = true;
      }
      
   };

   return  Flag;
}
/******************************************************************************/
bool inline RightTrigered(void)
{
  bool Flag = false;   
  if(ReadSensor(DP1))
   {
       if(!InverseInpitState){
      SensorStatus.Bits.DP1 = 1;
      OnSVD1();
      Flag = true;
       }
       else{
        SensorStatus.Bits.DP1 = 0;
        OffSVD1(); 
       }
   }
   else
   {
       if(!InverseInpitState){
      SensorStatus.Bits.DP1 = 0; 
      OffSVD1();
       }
       else{ 
      SensorStatus.Bits.DP1 = 1; 
      OnSVD1();
      Flag = true;
       }
   };
   return  Flag;  
}
/******************************************************************************/
void CheckSensors(void)
{
     LeftTrigered();
     RightTrigered();
}
/******************************************************************************/
SensorWord GetStatusSensors(void)
{
  return SensorStatus;
}
/******************************************************************************/
void  Sensors_InverseInput(int State)
{
  InverseInpitState = State;
}

int  Sensors_GetInversInputState(void)
{
  return InverseInpitState;
}