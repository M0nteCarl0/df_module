//------------------------------------------------------------------------------
//    Novikov
//    10.02.2015
//------------------------------------------------------------------------------
#include "MicroStack.h"
#ifndef __MYUSART_H
#define __MYUSART_H


//------------------------------------------------------------------------------
//#define   NST         32
//#define   NCOMD       32

#define   NCOMD       128
#define   COMMAND_ID  0x96
#define   EXPO_ADR    0x09         // Adress Dozimeter Modele 

#ifdef __cplusplus
extern "C" {
#endif
//------------

//------------
typedef struct
{
  uint8_t   Data  [NCOMD];
  int       JC;
  int       Bytes;
  int       Flg;
} MyCommand;
//------------------------------------------------------------------------------
void  InitCommand (MyCommand *Comd);
int   ReceiveCommandL (uint8_t *Buff, int *N, MyStack *Stack, MyCommand *Comd);
int   ReceiveCommandFromInternalStack (uint8_t *Buff, int *N);
int   SendCommandL    (uint8_t *Buff, int N, MyStack *Stack);

int   ReceiveCommand (uint8_t *Buff, int *N);
int   SendCommand    (uint8_t *Buff, int N);

uint8_t MakeCRC (uint8_t *Data, int N);

void InitIntrenalReceiveAndTranceiveStack(void);
void InitInternalReceiveStack(void);
void InitIntrenalTranceiveStack(void);
void InitInternalCommand(void);
void SetCRCCheck(int  Status);  
void BindExternalReceiveStack(MyStack ReceiveStack);
void BindExternalTranceiveStack(MyStack TranceiveStack);
MyStack GetReceiveStack(void);
MyStack GetTransmitStack(void);
int PutOneCharterToReceiveStack(uint8_t Charter);
int GetCharterFromTranceiveStack(uint8_t* Charter);
MyCommand GetComandBuffer(void);
#ifdef __cplusplus
}
#endif
//------------------------------------------------------------------------------

#endif
