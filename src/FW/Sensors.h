
#pragma once
#include "stm32f4xx.h"
#ifndef _SENSORS
#define _SENSORS

typedef struct SensorWordS
{
 __IO  uint8_t DP0:1;
 __IO  uint8_t DP1:1;
 __IO  uint8_t DP2:1;
}SensorWordS;


typedef union SensorWord
{
  SensorWordS Bits;
  uint8_t Raw;
}SensorWord;


typedef struct Sensor_Assigment
{ 
  uint32_t  RCC_Sensor;
  GPIO_TypeDef* GPIO_Port;
  uint32_t  InputPin;
  uint32_t  State;
}Sensor_Assigment;

#ifdef __cplusplus
extern "C" {
#endif 
void InitSensor(Sensor_Assigment Sensor);
uint32_t ReadSensor(Sensor_Assigment Sensor);
void InitSensrors(void);
bool LeftTrigered(void);
bool RightTrigered(void);
bool IsBetweenSensors(void);
void CheckSensors(void);
//bool CheckSensorID(SensorWord Sens,
SensorWord GetStatusSensors(void);
void  Sensors_InverseInput(int State);
int  Sensors_GetInversInputState(void);
#ifdef __cplusplus
}
#endif 

#endif