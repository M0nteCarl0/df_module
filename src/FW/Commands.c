#include "Sensors.h"
#include  "Commands.h"
#include  "MySPIN.h"
#include  "SteperConversion.h"
#include  "DF_Core.hpp"
#include   <math.h>
#include   "LED.h"
#include   "Indep_WD.h"
const unsigned char  Name[]         = {'D','F',0,0};
const unsigned char  VersSW[2]      = {1,3};
const unsigned char  VersHW[2]      = {3,3};
static  uint8_t         Buff [NCOMD];
static int Ret                      = 0;
static int N                        = 0;
static int StepsToWrite             = 0;
static uint8_t StateWR              = 0;
static bool  m_FlgCheckCRCSignature = true;
static int  Position;
/******************************************************************************/
 void Command_StopMotor(uint8_t *Buff, int N)
 {
   
     if (Buff[3] != 5)   return;
    __disable_irq();
     cSPIN_Soft_Stop();
    __enable_irq(); 
    N = Buff[3] = 5;
    int Ret = SendCommand (Buff, N);
    EnableRS485();
   
 }
 /******************************************************************************/
 void Command_SpeedProfileRun(uint8_t *Buff, int N)
 {
    if (Buff[3] != 20)   return;
    __disable_irq();
    
     uint8_t ControlWord = 0;
     uint16_t SpeedBegin = 0;
     uint32_t SpeedEnd   = 0;
     uint16_t TimeAcc    = 0;
     uint16_t TimeBrake  = 0;
     uint32_t StepCount  = 0;
     uint8_t StepMode;
     
     ControlWord			= Buff[5];
     SpeedBegin			        = Buff[6]  |  Buff[7]  << 7; 
     SpeedEnd				= Buff[8]  |  Buff[9]  << 7 |  Buff[10] << 14;
     TimeAcc				= Buff[11] |  Buff[12] << 7;
     TimeBrake				= Buff[13] |  Buff[14] << 7;
     StepCount				= Buff[15] |  Buff[16] << 7 | Buff[17] << 14;
     StepMode				= Buff[18];	   
     DF_Core_WriteSpeedProflieRun(&ControlWord,&SpeedBegin,&SpeedEnd, &TimeAcc,&TimeBrake, &StepCount,&StepMode); 
     Buff[5] =  ControlWord & 0x7f; 
     N = Buff[3] = 20;
    __enable_irq(); 
    
     int Ret = SendCommand (Buff, N);
     EnableRS485();
 }

 /******************************************************************************/
 void Command_ReadSpeedProfile(uint8_t *Buff, int N)
 {
   if (Buff[3] != 5)   return;
    __disable_irq();
     uint8_t ControlWord = 0;
     uint16_t SpeedBegin = 0;
     uint32_t SpeedEnd   = 0;
     uint16_t TimeAcc    = 0;
     uint16_t TimeBrake  = 0;
     uint32_t StepCount  = 0;
     uint8_t StepMode    = 0;
     DF_Core_ReadSpeedProflie(&ControlWord,&SpeedBegin,&SpeedEnd, &TimeAcc,&TimeBrake, &StepCount,&StepMode);
     
     Buff[5]  =  ControlWord & 0x7f; 
     Buff[6]  =  SpeedBegin & 0x7f;	
     Buff[7]  =  (SpeedBegin >> 7) & 0x7f; 
     Buff[8]  =  (SpeedEnd) & 0x7f;
     Buff[9]  =  (SpeedEnd >> 7) & 0x7f;
     Buff[10] =  (SpeedEnd >> 14) & 0x7f;
     Buff[11] =  (TimeAcc) & 0x7f;
     Buff[12] =  (TimeAcc >> 7) & 0x7f;
     Buff[13] =  (TimeBrake) & 0x7f;
     Buff[14] =  (TimeBrake >> 7) & 0x7f;
     Buff[15] =  (StepCount) & 0x7f;
     Buff[16] =  (StepCount >> 7) & 0x7f;
     Buff[17] =  (StepCount >> 14) & 0x7f;
     Buff[18] =  StepMode & 0x7f;
          
    __enable_irq(); 
    N = Buff[3] = 20;
    int Ret = SendCommand (Buff, N);
    EnableRS485();
   
   
 }
 /******************************************************************************/
 void Command_ReadStateDrive(uint8_t *Buff, int N)
 {
   
   if (Buff[3] != 5)   return;
    __disable_irq();
    uint32_t StateWord     = 0;
    int ABS_Position       = 0;
    uint32_t Speed         = 0;
    uint8_t SignByte       = 0;
    uint32_t ABS_Positionu = 0;
    
    DF_Core_ReadDriveState(&StateWord,&ABS_Position,&Speed);
    ABS_Positionu = abs((double)ABS_Position);  
    Buff[5]  =  (StateWord) & 0x7f;
    Buff[6]  =  (StateWord >> 7) & 0x7f;
    Buff[7]  =  (StateWord >> 14) & 0x7f;
    Buff[8]  =  (ABS_Positionu) & 0x7f;
    Buff[9]  =  (ABS_Positionu >> 7) & 0x7f;
    Buff[10] =  (ABS_Positionu >> 14) & 0x7f;
    
    if( ABS_Position & (1 << 31))
    {
      SignByte = 1;
    }
    Buff[11]  =  SignByte;
    Buff[12]  =  (Speed) & 0x7f;
    Buff[13]  =  (Speed >> 7) & 0x7f;
    Buff[14]  =  (Speed >> 14) & 0x7f;
    __enable_irq(); 
    N = Buff[3] = 16;
    int Ret = SendCommand (Buff, N);
    EnableRS485();
   
 }
/******************************************************************************/
 void  Command_ReadVersion(uint8_t *Buff, int N)
 {
   
      if (Buff[3] != 5)   return;
      __disable_irq();
      uint8_t SB = 0x01;
      Buff[7] = Buff[8] = 0;
      Buff[5] = Name[0];
      if (Name[0] & 0x80) Buff[8] |= SB;
      SB = SB << 1;
      Buff[6] = Name[1];
      if (Name[1] & 0x80) Buff[8] |= SB;
      Buff[9]  = VersHW[0];
      Buff[10] = VersHW[1];
      Buff[11] = VersSW[0];
      Buff[12] = VersSW[1];
      N = Buff[3] = 14;
      __enable_irq(); 
      int Ret = SendCommand (Buff, N);
      EnableRS485();
 }
/******************************************************************************/
 void    Command_WriteSetupDriver(uint8_t *Buff, int N)
 {
      if (Buff[3] != 67)   return;
      __disable_irq();
      cSPIN_RegsStruct_TypeDef cSPIN;
      ConertcSPIN_ByteToRegsStruct_TypeDef(&cSPIN,&Buff[5]);
      DF_Core_WriteSetupToFlash(cSPIN);
      cSPIN_Registers_Set(&cSPIN);
      Sensors_InverseInput(cSPIN.InverseSensorInput);
     __enable_irq(); 
 }
/******************************************************************************/
 void    Command_ReadSetupDriver(uint8_t *Buff, int N)
 {
      if (Buff[3] != 5)   return;
      __disable_irq();
      cSPIN_RegsStruct_TypeDef cSPIN;
      DF_Core_ReadSetupFomFlash(cSPIN);
      cSPIN.ABS_POS = abs((double)cSPIN_Get_ABS_Position());
      cSPIN.InverseSensorInput = Sensors_GetInversInputState();
      ConertcSPIN_RegsStruct_TypeDefToByte(&cSPIN,&Buff[5]);
      __enable_irq(); 
      N = Buff[3] = 67;
      int Ret = SendCommand (Buff, N);
      EnableRS485();
 }
/******************************************************************************/
 void  Command_ProcessCommands(void)
 {
 
	 do
     {   
                LED_ModuleHeartBeat();
                iWD_RefreshCountet();
                Ret = ReceiveCommand (Buff, &N);
                  if(Ret)
                        {
                             Command_CommandParser(Buff,Buff[2],N);
                    
                        }
               
                
             
                  
      }while(1);
	
 }
/******************************************************************************/
void  Command_CommandParser(uint8_t *Buff,uint8_t ComandID,int N)
{
   switch(ComandID)
    {
       case  CommadID_WRITE_SPEED_PROFILE_RUN:
       { 
         Command_SpeedProfileRun(Buff,N);
         break;
       } 
       
       case  CommadID_WRITE_STOP_MOTOR:
       {
         Command_StopMotor(Buff, N);
         break;
       }
       
       case  CommadID_WRITE_DRIVER_SETUP:
       {
         Command_WriteSetupDriver(Buff, N);
         break;
       }
       
       case CommadID_READ_HW_SW:
       {
           Command_ReadVersion(Buff, N);
           break;
       }
         
       case CommadID_READ_SPEED_PROFILE:
       {
           Command_ReadSpeedProfile(Buff, N);
           break;
       }
             
       case CommadID_READ_STATE_WORD:
       {
           Command_ReadStateDrive(Buff, N);
           break;
       }
       
       case  CommadID_READ_DRIVER_SETUP:		   
       { 
           Command_ReadSetupDriver(Buff, N);
           break;
       }
      
    } 
}
/******************************************************************************/                            