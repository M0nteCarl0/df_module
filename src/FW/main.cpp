#include "Clock.h"
#include "MyUsartHard.h"
#include "MyUsart.h"
#include "LED.h"
#include "MySPIN.h"
#include "Sensors.h"
#include "Commands.h"
#include "DF_Core.hpp"
#include "Timer.h"
int main()
{
   HSE_BYPASS();
  __disable_irq();
  InitLED();
  InitSensrors();
  Timer_InitMilisecond();
  DF_EmeragancyStopMotor();
  Timer_TraceInit();//SPI Operations Timeout;
  Init_UsartHard();
  InitIntrenalReceiveAndTranceiveStack();
  InitInternalCommand();
  __enable_irq();
  cSPIN_Peripherals_Init();
  cSPIN_PrepareDevice();
  DF_Core_LoadSetupDriverFromFlash();
  DF_Core_PollingTimerInit();
  Command_ProcessCommands();
}
